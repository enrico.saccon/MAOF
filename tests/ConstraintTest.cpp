/**
 * @author Enrico Saccon <enrico.saccon@unitn.it>
 * @date 27/07/22
 */

#ifdef GTEST
#include <gtest/gtest.h>

#include <Defines.hpp>
#include <MAPF/Constraint.hpp>

TEST(Constraint, ConstraintConstructorVoid){
  Constraint cons = Constraint();
  EXPECT_EQ(Node(), cons.getN1());
  EXPECT_EQ(Node(), cons.getN2());
  EXPECT_EQ(0, cons.getTimestamp());
  EXPECT_EQ(0, cons.getAId());
//  EXPECT_EQ(0, cons.getA().getID());
}

TEST(Constraint, ConstraintConstructor1){
  Constraint cons = Constraint(
    Node(1,2),
    1, Agent(3));

  EXPECT_EQ(Node(1,2), cons.getN1());
  EXPECT_EQ(Node(), cons.getN2());
  EXPECT_EQ(1, cons.getTimestamp());
  EXPECT_EQ(3, cons.getAId());
//  EXPECT_EQ(3, cons.getA().getID());
}

TEST(Constraint, ConstraintConstructor2){
  Constraint cons = Constraint(
    Node(1,2),
    Node(2,3),
    1, Agent(3));

  EXPECT_EQ(Node(1,2), cons.getN1());
  EXPECT_EQ(Node(2,3), cons.getN2());
  EXPECT_EQ(1, cons.getTimestamp());
  EXPECT_EQ(3, cons.getAId());
//  EXPECT_EQ(3, cons.getA().getID());
}

TEST(Constraint, ConstraintConstructorCheckConflict1){
  Constraint cons = Constraint(
    Node(1,2),
    Node(2,3),
    1, Agent(3));

  // Correct node wrong time
  EXPECT_EQ(Conflict::NONE, cons.checkConflict(Node(1,2), 2));
  // Correct time wrong node
  EXPECT_EQ(Conflict::NONE, cons.checkConflict(Node(2,2), 1));
  // Correct time but node is N2
  EXPECT_EQ(Conflict::NONE, cons.checkConflict(Node(2,3), 1));
  // Correct time and N1 => Vertex conflict
  EXPECT_EQ(Conflict::VERTEX, cons.checkConflict(Node(1,2), 1));
  // Correct time and N2 => Vertex conflict
  EXPECT_EQ(Conflict::SWAP, cons.checkConflict(Node(1,2), 1, Node(2,3)));
}

TEST(Constraint, ConstraintConstructorCheckConflict2){
  Constraint cons = Constraint(
    Node(1,2),
    Node(2,3),
    1, Agent(3));

  // Create a path that DOES NOT violate the constraint
  std::vector<Node> path1 = {
    Node(1,2),
    Node(2,3),
    Node(3,4)
  };

  // Create a path that DOES violate the constraint with VERTEX conflict
  std::vector<Node> path2 = {
    Node(2,3),
    Node(1,2),
    Node(3,4)
  };

  // Create a path that DOES violate the constraint with SWAP conflict
  std::vector<Node> path3 = {
    Node(3,4),
    Node(1,2),
    Node(2,3)
  };

  EXPECT_EQ(Conflict::NONE, cons.checkConflict(path1));
  EXPECT_EQ(Conflict::VERTEX, cons.checkConflict(path2));
  EXPECT_EQ(Conflict::SWAP, cons.checkConflict(path3));
}

#endif