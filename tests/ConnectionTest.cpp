//
// Created by Enrico Saccon on 27/07/22.
//
#ifdef GTEST
#include <gtest/gtest.h>

#include <Connection.hpp>
#include <MAPF/Constraint.hpp>

typedef GraphNode Node;

TEST(ConnectionTest, ConnectionConstructorVoid)
{
  Connection con = Connection();

  EXPECT_EQ(con.getType(), Connection::TYPE::ZERO);
  EXPECT_EQ(con.size(), 0);
}

TEST(ConnectionTest, ConnectionConstructorTypeONE)
{
  Connection con = Connection(Connection::TYPE::ONE);

  EXPECT_EQ(con.getType(), Connection::TYPE::ONE);
  EXPECT_NE(con.getType(), Connection::TYPE::ZERO);
  EXPECT_NE(con.getType(), Connection::TYPE::LIMIT_ALWAYS);
  EXPECT_NE(con.getType(), Connection::TYPE::LIMIT_ONCE);
}

TEST(ConnectionTest, ConnectionConstructorTypeZERO)
{
  Connection con = Connection(Connection::TYPE::ZERO);

  EXPECT_EQ(con.getType(), Connection::TYPE::ZERO);
  EXPECT_NE(con.getType(), Connection::TYPE::ONE);
  EXPECT_NE(con.getType(), Connection::TYPE::LIMIT_ALWAYS);
  EXPECT_NE(con.getType(), Connection::TYPE::LIMIT_ONCE);
}

TEST(ConnectionTest, ConnectionConstructorTypeLIMITALWAYS)
{
  Connection con = Connection(Connection::TYPE::LIMIT_ALWAYS);

  EXPECT_EQ(con.getType(), Connection::TYPE::LIMIT_ALWAYS);
  EXPECT_NE(con.getType(), Connection::TYPE::ONE);
  EXPECT_NE(con.getType(), Connection::TYPE::ZERO);
  EXPECT_NE(con.getType(), Connection::TYPE::LIMIT_ONCE);
}

TEST(ConnectionTest, ConnectionConstructorTypeLIMITONCE)
{
  Connection con = Connection(Connection::TYPE::LIMIT_ONCE);

  EXPECT_EQ(con.getType(), Connection::TYPE::LIMIT_ONCE);
  EXPECT_NE(con.getType(), Connection::TYPE::ONE);
  EXPECT_NE(con.getType(), Connection::TYPE::ZERO);
  EXPECT_NE(con.getType(), Connection::TYPE::LIMIT_ALWAYS);
}

TEST(ConnectionTest, ConnectionConstructor)
{
  std::vector<TM_ID> times = {std::make_pair(1,0), std::make_pair(2,0)};
  Connection con = Connection(Connection::TYPE::LIMIT_ONCE, 1, times);

  EXPECT_EQ(con.getType(), Connection::TYPE::LIMIT_ONCE);
  EXPECT_EQ(con.size(), 2);
  for (size_t i = 0; i < con.size(); i++) {
    EXPECT_EQ(con[i], times[i]);
  }
}

TEST(ConnectionTest, ConnectionEqual)
{
  Connection a (Connection::TYPE::ZERO);
  Connection b (Connection::TYPE::ONE);
  Connection c (Connection::TYPE::LIMIT_ONCE, 1, {1,2,3});
  Connection d (Connection::TYPE::LIMIT_ALWAYS, 1, {1,2,3});

  EXPECT_EQ(a, Connection(Connection::TYPE::ZERO));
  EXPECT_NE(a, Connection(Connection::TYPE::ONE));
  EXPECT_NE(a, Connection(Connection::TYPE::LIMIT_ONCE, 1, std::vector<size_t>(0)));
  EXPECT_NE(a, Connection(Connection::TYPE::LIMIT_ONCE, 1, {1,2,3,4,5}));
  EXPECT_NE(a, Connection(Connection::TYPE::LIMIT_ALWAYS, 1, std::vector<size_t>(0)));
  EXPECT_NE(a, Connection(Connection::TYPE::LIMIT_ALWAYS, 1, {1,2,3,4,5}));

  EXPECT_EQ(b, Connection(Connection::TYPE::ONE));
  EXPECT_NE(b, Connection(Connection::TYPE::ZERO));
  EXPECT_NE(b, Connection(Connection::TYPE::LIMIT_ONCE, 1, std::vector<size_t>(0)));
  EXPECT_NE(b, Connection(Connection::TYPE::LIMIT_ONCE, 1, {1,2,3,4,5}));
  EXPECT_NE(b, Connection(Connection::TYPE::LIMIT_ALWAYS, 1, std::vector<size_t>(0)));
  EXPECT_NE(b, Connection(Connection::TYPE::LIMIT_ALWAYS, 1, {1,2,3,4,5}));

  EXPECT_EQ(c, Connection(Connection::TYPE::LIMIT_ONCE, 1, {1,2,3}));
  EXPECT_NE(c, Connection(Connection::TYPE::ONE));
  EXPECT_NE(c, Connection(Connection::TYPE::ONE, 1, std::vector<size_t>(0)));
  EXPECT_NE(c, Connection(Connection::TYPE::ONE, 1, {1,2,3,4,5}));
  EXPECT_NE(c, Connection(Connection::TYPE::ZERO));
  EXPECT_NE(c, Connection(Connection::TYPE::ZERO, 1, std::vector<size_t>(0)));
  EXPECT_NE(c, Connection(Connection::TYPE::ZERO, 1, {1,2,3,4,5}));
  EXPECT_NE(c, Connection(Connection::TYPE::LIMIT_ALWAYS, 1, std::vector<size_t>(0)));
  EXPECT_NE(c, Connection(Connection::TYPE::LIMIT_ALWAYS, 1, {1,2,3,4,5}));

  EXPECT_EQ(d, Connection(Connection::TYPE::LIMIT_ALWAYS, 1, {1,2,3}));
  EXPECT_NE(d, Connection(Connection::TYPE::ONE));
  EXPECT_NE(d, Connection(Connection::TYPE::ONE, 1, std::vector<size_t>(0)));
  EXPECT_NE(d, Connection(Connection::TYPE::ONE, 1, {1,2,3,4,5}));
  EXPECT_NE(d, Connection(Connection::TYPE::ZERO));
  EXPECT_NE(d, Connection(Connection::TYPE::ZERO, 1, std::vector<size_t>(0)));
  EXPECT_NE(d, Connection(Connection::TYPE::ZERO, 1, {1,2,3,4,5}));
  EXPECT_NE(d, Connection(Connection::TYPE::LIMIT_ONCE, 1, std::vector<size_t>(0)));
  EXPECT_NE(d, Connection(Connection::TYPE::LIMIT_ONCE, 1, {1,2,3,4,5}));
}

TEST(ConnectionTest, ConnectionLIMIT_ONCECost)
{
  std::vector<TM_ID> times = {{1,2}, {2,4}};
  Connection con = Connection(Connection::TYPE::LIMIT_ONCE, 1, times);

  size_t sum = 0;
  for (size_t i = 0; i < 10; i++) {
    sum += con.cost(i);
  }

  EXPECT_EQ(sum, 10 - times.size());
  bool tmp = equalPairs<size_t, size_t>(con.costId(times[0].first), std::make_pair(0,times[0].second));
  EXPECT_TRUE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[0].first).first, con.costId(times[0].first).second, 0, times[0].second);
  tmp = equalPairs<size_t, size_t>(con.costId(times[1].first), std::make_pair(0,times[1].second));
  EXPECT_TRUE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[1].first).first, con.costId(times[1].first).second, 0, times[1].second);
  tmp = equalPairs<size_t, size_t>(con.costId(times[0].first), std::make_pair(1,0));
  EXPECT_FALSE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[0].first).first, con.costId(times[0].first).second, 1, 0);
  tmp = equalPairs<size_t, size_t>(con.costId(times[1].first), std::make_pair(1,0));
  EXPECT_FALSE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[1].first).first, con.costId(times[1].first).second, 1, 0);
}

TEST(ConnectionTest, ConnectionLIMIT_ALWAYSCost)
{
  std::vector<TM_ID> times = {{1, 2},{2,4}};
  Connection con = Connection(Connection::TYPE::LIMIT_ALWAYS, 1, times);

  size_t sum = 0;
  for (size_t i = 0; i < 10; i++) {
    sum += con.cost(i);
  }

  EXPECT_EQ(sum, times.size());
  bool tmp = equalPairs<size_t, size_t>(con.costId(times[0].first), std::make_pair(0,times[0].second));
  EXPECT_FALSE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[0].first).first, con.costId(times[0].first).second, 0, times[0].second);
  tmp = equalPairs<size_t, size_t>(con.costId(times[1].first), std::make_pair(0,times[1].second));
  EXPECT_FALSE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[1].first).first, con.costId(times[1].first).second, 0, times[1].second);
  tmp = equalPairs<size_t, size_t>(con.costId(times[0].first), std::make_pair(1,times[0].second));
  EXPECT_TRUE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[0].first).first, con.costId(times[0].first).second, 1, 0);
  tmp = equalPairs<size_t, size_t>(con.costId(times[1].first), std::make_pair(1,times[1].second));
  EXPECT_TRUE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[1].first).first, con.costId(times[1].first).second, 1, 0);
}

TEST(ConnectionTest, ConnectionONECost)
{
  std::vector<TM_ID> times = {{1, 2},{2,4}};

  Connection con = Connection(Connection::TYPE::ONE, 1, times);

  size_t sum = 0;
  for (size_t i = 0; i < 10; i++) {
    sum += con.cost(i);
  }

  EXPECT_EQ(sum, 10);
  bool tmp = equalPairs<size_t, size_t>(con.costId(times[0].first), std::make_pair(1, 0));
  EXPECT_TRUE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[0].first).first, con.costId(times[0].first).second, 1, 0);
  tmp = equalPairs<size_t, size_t>(con.costId(times[1].first), std::make_pair(1, 0));
  EXPECT_TRUE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[1].first).first, con.costId(times[1].first).second, 1, 0);
  tmp = equalPairs<size_t, size_t>(con.costId(times[0].first), std::make_pair(0,0));
  EXPECT_FALSE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[0].first).first, con.costId(times[0].first).second, 0, 0);
  tmp = equalPairs<size_t, size_t>(con.costId(times[1].first), std::make_pair(0,0));
  EXPECT_FALSE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[1].first).first, con.costId(times[1].first).second, 0, 0);
}

TEST(ConnectionTest, ConnectionZEROCost)
{
  std::vector<TM_ID> times = {{1, 2},{2,4}};
  Connection con = Connection(Connection::TYPE::ZERO, 1, times);

  size_t sum = 0;
  for (size_t i = 0; i < 10; i++) {
    sum += con.cost(i);
  }

  EXPECT_EQ(sum, 0);
  bool tmp = equalPairs<size_t, size_t>(con.costId(times[0].first), std::make_pair(0,0));
  EXPECT_TRUE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[0].first).first, con.costId(times[0].first).second, 0, 0);
  tmp = equalPairs<size_t, size_t>(con.costId(times[1].first), std::make_pair(0,0));
  EXPECT_TRUE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[1].first).first, con.costId(times[1].first).second, 0, 0);
  tmp = equalPairs<size_t, size_t>(con.costId(times[0].first), std::make_pair(1,0));
  EXPECT_FALSE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[0].first).first, con.costId(times[0].first).second, 1, 0);
  tmp = equalPairs<size_t, size_t>(con.costId(times[1].first), std::make_pair(1,0));
  EXPECT_FALSE(tmp) << tprintf("(@,@)!=(@,@)", con.costId(times[1].first).first, con.costId(times[1].first).second, 1, 0);
}

TEST(ConnectionTest, ConnectionConvert)
{
  std::vector<std::vector<int>> connect = {
    {1, 0, 1, 1, 0, 1, 1}, {0, 1, 1, 1, 1, 0, 0}, {1, 1, 1, 0, 1, 0, 0}, {0, 0, 0, 1, 1, 0, 0},
    {0, 0, 0, 0, 1, 0, 0}, {1, 0, 1, 0, 1, 0, 1}, {0, 1, 0, 1, 0, 1, 0}};

  std::vector<std::vector<Connection>> convertCon = convertConnect(connect);

  EXPECT_EQ(connect.size(), convertCon.size());

  size_t x=0, y=0;
  bool quit = false;
  for(; x<connect.size() && !quit; x++){
    EXPECT_EQ(connect[x].size(), convertCon[x].size());
    for(; y<connect[x].size(); y++){
      if(connect[x][y] != convertCon[x][y].cost()){
        quit = true;
        break;
      }
    }
  }

  EXPECT_FALSE(quit) << "Function did not convert correctly position " << x << "," << y;
}



#endif