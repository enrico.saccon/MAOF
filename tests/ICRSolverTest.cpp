/** 
 * @author Enrico Saccon <enrico.saccon@unitn.it>.
 * @file ICRTest.cpp
 * @brief Test file for ICR
 */

#ifdef GTEST
#include <gtest/gtest.h>

// Library includes
#include <Defines.hpp>
#include "MAPF/iMAPF.hpp"
#include "SAPF/iSAPF.hpp"
#include <Utils.hpp>
#include <Timeperf.hpp>
#include <ConMatrix.hpp>
#include <Test.hpp>

// System includes
#include <stdlib.h>
#include <algorithm>
#include <iostream>
#include <random>

TEST(ICRSolver, ICRSolverConstructor)
{
  size_t n_agents = 3;
  std::vector<Node> nodes = {Node(1), Node(2), Node(3), Node(4),
                             Node(5), Node(6), Node(7), Node(8)};
  std::vector<Node> initPos = {Node(1), Node(2), Node(3)};
  std::vector<Node> endPos = {Node(4), Node(5), Node(6)};
  std::vector<std::vector<Node>> goals = {
    {Node(3)}, {Node(1), Node(3)}, {Node(1), Node(2), Node(7)}};

  std::vector<std::vector<int>> connect = {{0, 1, 0, 0, 0, 0, 0, 0}, {1, 0, 1, 1, 0, 0, 0, 0},
                                           {0, 1, 0, 0, 1, 0, 0, 0}, {0, 1, 0, 0, 0, 0, 1, 0},
                                           {0, 0, 1, 0, 0, 0, 0, 1}, {0, 0, 0, 0, 0, 0, 1, 0},
                                           {0, 0, 0, 1, 0, 1, 0, 1}, {0, 0, 0, 0, 1, 0, 1, 0}};

  std::shared_ptr<Graph> graph = std::make_shared<ConMatrix>(ConMatrix(nodes, connect));

  TDSPSolver* sapfSolver = new TDSPSolver(graph);

  std::vector<Agent> agents;
  for (size_t i = 0; i < n_agents; i++) {
    Agent a(i, i, initPos[i], endPos[i], goals[i], std::make_unique<SAPFSolver*>(sapfSolver), "");
    agents.push_back(a);
  }
  ICRSolver solver = ICRSolver(agents, MAPF_TYPE::ICTS, COST_FUNCTION::SIC);

  EXPECT_EQ(solver.getAgents().size(), n_agents);
  for (size_t a_id = 0; a_id < solver.getAgents().size(); a_id++) {
    Agent a = solver.getAgents()[a_id];
    EXPECT_EQ(a.getSolver()->getGraph()->getNodes(), nodes);
    EXPECT_EQ(a.getInitPos(), initPos[a_id]);
    EXPECT_EQ(a.getEndPos(), endPos[a_id]);
    EXPECT_EQ(a.getGoals(), goals[a_id]);
    EXPECT_TRUE(a.getSolver()->getGraph()->equal(graph.get()[0]));
  }

  n_agents = 2;
  nodes = {Node(1), Node(2), Node(3), Node(4), Node(5), Node(6), Node(7), Node(8), Node(9)};
  initPos = {Node(2), Node(3)};
  endPos = {Node(7), Node(6)};
  goals = {{Node(1), Node(3)}, {Node(1), Node(2), Node(4)}};

  connect = {{0, 1, 0, 0, 0, 0, 0, 0}, {1, 0, 1, 1, 0, 0, 0, 1}, {0, 1, 0, 0, 1, 0, 0, 0},
             {0, 1, 0, 0, 0, 0, 1, 0}, {0, 0, 1, 0, 0, 0, 0, 1}, {0, 0, 0, 0, 0, 0, 1, 0},
             {0, 0, 0, 1, 0, 1, 0, 1}, {0, 0, 0, 0, 1, 0, 1, 0}};

  graph = std::make_shared<ConMatrix>(ConMatrix(nodes, connect));

  EXPECT_NE(solver.getAgents().size(), n_agents);
  for (size_t a_id = 0; a_id < n_agents; a_id++) {
    Agent a = solver.getAgents()[a_id];
    EXPECT_NE(a.getSolver()->getGraph()->getNodes(), nodes);
    EXPECT_NE(a.getInitPos(), initPos[a_id]);
    EXPECT_NE(a.getEndPos(), endPos[a_id]);
    EXPECT_NE(a.getGoals(), goals[a_id]);
    EXPECT_FALSE(a.getSolver()->getGraph()->equal(graph.get()[0]));
  }
}

TEST(ICRSolver, ICRSolverConstructorRandom)
{
  std::vector<size_t> agents = {2, 5, 10, 100, 200};
  std::vector<size_t> n_nodes = {10, 500}; //, 1000, 100000, 1000000};

  int counter = 0;
  for(auto a : agents){
    for(auto n : n_nodes) {
      counter++;
      if (n < a) {
        continue;
      }
      std::cout << std::endl << tprintf("@% agents @ nodes @", (counter * 100.0 / (agents.size() * n_nodes.size())), a, n);

      std::vector<Node> nodes = {};
      std::vector<Node> initPos = {};
      std::vector<Node> endPos = {};
      std::vector<std::vector<Node>> goals = {};

      std::vector<std::vector<int>> connect = {};

      for (size_t i = 1; i <= n; i++) {
        nodes.push_back(Node(i));
      }
      std::sample(
        nodes.begin(), nodes.end(), std::back_inserter(initPos), a,
        std::mt19937{std::random_device{}()});
      std::sample(
        nodes.begin(), nodes.end(), std::back_inserter(endPos), a,
        std::mt19937{std::random_device{}()});
      for (size_t i = 0; i < a; i++) {
        std::vector<Node> tmp = {};
        std::sample(
          nodes.begin(), nodes.end(), std::back_inserter(tmp), rand() % (int)(n / 2) + 1,
          std::mt19937{std::random_device{}()});
        goals.push_back(tmp);
      }

      connect.resize(n);
      for (size_t i = 0; i < n; i++) {
        connect[i].resize(n);
        for (size_t j = 0; j < n; j++) {
          connect[i][j] = rand() % 2;
        }
      }

      TimePerf tp;
      tp.start();
      std::shared_ptr<Graph> graph = std::make_shared<ConMatrix>(ConMatrix(nodes, connect));
      TDSPSolver* sapfSolver = new TDSPSolver(graph);
      std::cout << "Took " << tp.getTime() << "ms to create graph" << std::endl;
      tp.start();

      std::vector<Agent> agents;
      for (size_t i = 0; i < a; i++) {
        Agent a(i, i, initPos[i], endPos[i], goals[i], std::make_unique<SAPFSolver*>(sapfSolver), "");
        agents.push_back(a);
      }
      std::cout << "Took " << tp.getTime() << "ms to create agents" << std::endl;
      tp.start();

      ICRSolver solver = ICRSolver(agents, MAPF_TYPE::ICTS, COST_FUNCTION::SIC);
      std::cout << "Took " << tp.getTime() << "ms to create solver" << std::endl;
      tp.start();

      double newt = 0.0;
      TimePerf tp1;
      EXPECT_EQ(solver.getAgents().size(), a);
      for (size_t a_id = 0; a_id < solver.getAgents().size(); a_id++) {
        Agent a = solver.getAgents()[a_id];
        EXPECT_EQ(a.getSolver()->getGraph()->getNodes(), nodes);
        EXPECT_EQ(a.getInitPos(), initPos[a_id]);
        EXPECT_EQ(a.getEndPos(), endPos[a_id]);
        EXPECT_EQ(a.getGoals(), goals[a_id]);
        tp1.start();
        EXPECT_TRUE(a.getSolver()->getGraph()->equal(graph.get()[0]));
        newt+=tp1.getTime();
      }
      std::cout << "Took " << tp.getTime() << "ms to check the rest of which " << newt << "ms to check the graph" << std::endl;
    }
  }
}

TEST(ICRSolver, ICRSolverVertexConflict){
  EXPECT_NO_THROW(runTest("tests/vertexConflict.json", "ICR", "SIC", "TDSP", "", "ICTS"));
}

TEST(ICRSolver, ICRSolverSwapConflict){
  EXPECT_NO_THROW(runTest("tests/swapConflict.json", "ICR", "SIC", "TDSP", "", "ICTS"));
}

TEST(ICRSolver, ICRSolverEasy){
  EXPECT_NO_THROW(runTest("tests/easy.json", "ICR", "SIC", "TDSP", "", "ICTS"));
}

#endif  // GTEST