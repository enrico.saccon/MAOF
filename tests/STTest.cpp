//
// Created by Enrico Saccon on 27/07/22.
// DEPRECATED
//

#ifdef GTEST

// System includes
#include <gtest/gtest.h>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <vector>
namespace fs = std::filesystem;

// Library includes
#include "SAPF/ST/ST.hpp"
#include <Test.hpp>
#include <nlohmann/json.hpp>

typedef GraphNode Node;

TEST(SpanningTree, SpanningTree)
{
  // 1-2-3
  //   | |
  //   4 5
  //   | |
  // 6-7-8

  std::vector<Node> nodes = {Node(1), Node(2), Node(3), Node(4),
                             Node(5), Node(6), Node(7), Node(8)};

  std::vector<std::vector<int>> connect = {{0, 1, 0, 0, 0, 0, 0, 0}, {1, 0, 1, 1, 0, 0, 0, 0},
                                           {0, 1, 0, 0, 1, 0, 0, 0}, {0, 1, 0, 0, 0, 0, 1, 0},
                                           {0, 0, 1, 0, 0, 0, 0, 1}, {0, 0, 0, 0, 0, 0, 1, 0},
                                           {0, 0, 0, 1, 0, 1, 0, 1}, {0, 0, 0, 0, 1, 0, 1, 0}};

  std::vector<Node> goals = {};

  std::shared_ptr<Graph> graph = std::make_shared<ConMatrix>(ConMatrix(nodes, connect));

  STSolver solver(graph);
  std::vector<Node> path = solver.solve(Node(1), Node(6), goals);

  std::vector<Node> sol = {Node(1), Node(2), Node(4), Node(7), Node(6)};
//    {Node(1), Node(2), Node(3), Node(5), Node(8), Node(7), Node(6)},

  EXPECT_TRUE(equalPaths(path, sol)) << "Path computed is not the solution " << pathToString(path);
}

TEST(SpanningTree, SpanningTreeWithGoals)
{
  // 1-2-3
  //   | |
  //   4 5
  //   | |
  // 6-7-8

  std::vector<Node> nodes = {Node(1), Node(2), Node(3), Node(4),
                             Node(5), Node(6), Node(7), Node(8)};

  std::vector<std::vector<int>> connect = {{0, 1, 0, 0, 0, 0, 0, 0}, {1, 0, 1, 1, 0, 0, 0, 0},
                                           {0, 1, 0, 0, 1, 0, 0, 0}, {0, 1, 0, 0, 0, 0, 1, 0},
                                           {0, 0, 1, 0, 0, 0, 0, 1}, {0, 0, 0, 0, 0, 0, 1, 0},
                                           {0, 0, 0, 1, 0, 1, 0, 1}, {0, 0, 0, 0, 1, 0, 1, 0}};

  std::vector<Node> goals = {Node(3), Node(4)};

  std::shared_ptr<Graph> graph = std::make_shared<ConMatrix>(ConMatrix(nodes, connect));

  STSolver solver(graph);
  std::vector<Node> path = solver.solve(Node(1), Node(6), goals);

  std::vector<Node> sol = {Node(1),Node(2),Node(3),Node(2),Node(4),Node(7),Node(6)};
//    {Node(1),Node(2),Node(3),Node(2),Node(4),Node(2),Node(3),Node(5),Node(8),Node(7),Node(6)},
//    {Node(1),Node(2),Node(3),Node(5),Node(8),Node(7),Node(4),Node(7),Node(6)},
//    {Node(1),Node(2),Node(3),Node(5),Node(8),Node(7),Node(4),Node(2),Node(3),Node(5),Node(8),Node(7),Node(6)},
//    {Node(1),Node(2),Node(4),Node(7),Node(8),Node(5),Node(3),Node(2),Node(4),Node(7),Node(6)},
//    {Node(1),Node(2),Node(4),Node(7),Node(8),Node(5),Node(3),Node(2),Node(4),Node(2),Node(3),Node(5),Node(8),Node(7),Node(6)},
//    {Node(1),Node(2),Node(4),Node(7),Node(8),Node(5),Node(3),Node(5),Node(8),Node(7),Node(4),Node(7),Node(6)},
//    {Node(1),Node(2),Node(4),Node(7),Node(8),Node(5),Node(3),Node(5),Node(8),Node(7),Node(4),Node(2),Node(3),Node(5),Node(8),Node(7),Node(6)}

  EXPECT_TRUE(equalPaths(path, sol)) << "Path computed is not the solution " << pathToString(path);

}


TEST(SpanningTree, ShortestPathTest)
{
  for (const auto & filename : fs::directory_iterator("SPTests")) {
    if (std::string(filename.path()).find("json") != std::string::npos) {
      std::cout << "Running test " << filename.path() << std::endl;

      Node source, target;
      std::vector<Node> goals = {};
      std::vector<Node> path = {};
      std::vector<Node> sol = {};

      std::shared_ptr<Graph> graph = readSAPFJSON(sol, source, target, goals, filename.path());
      if (graph.get()->getNNodes() > 20) { continue; }
      std::cout << graph.get()[0] << std::endl;
      STSolver solver(graph);

//      try {
//        ASSERT_DURATION_LE(10, path = solver.solve(source, target, goals))
//        path = solver.solve(source, target, goals);
//      } catch (std::exception & E) {
//        std::cout << E.what() << std::endl;
//        path = {};
//      }
//
//      EXPECT_EQ(path.size(), sol.size()) << "path and sol do not have the same length "
//                                         << path.size() << " " << sol.size() << std::endl
//                                         << solver.getGraph()->toString();
//
//      EXPECT_TRUE((equalPaths(path, sol)) || bothValid(path, sol, graph.get()->getMatrix()))
//        << "Path is not the correct solution\n"
//        << pathToString(path) << std::endl
//        << pathToString(sol);
    }
  }
}


#endif  // GTEST
