/**
* @author Enrico Saccon <enrico.saccon@unitn.it>
* @file CBSNodeTest.cpp
*/

#ifdef GTEST
#include <gtest/gtest.h>
#include <stdlib.h>

#include <Defines.hpp>
#include "MAPF/CBS/CBSNode.hpp"
#include <Test.hpp>
#include <Utils.hpp>
#include <iostream>

TEST(CBSNode, CBSNodeConstructorVoid)
{
  CBSNode node = CBSNode();

  EXPECT_EQ(nullptr, node.getParent());
  EXPECT_EQ(0, node.getConstraints().size());
  EXPECT_EQ(0, node.getSolution().size());
  EXPECT_EQ(-1, node.getCost());
}

TEST(CBSNode, CBSNodeConstructor1)
{
  CBSNode parent = CBSNode();
  std::vector<Constraint> constraints = {Constraint(Node(1), 2, Agent(3))};
  std::vector<std::vector<Node>> solution = {{Node(1), Node(2)}, {Node(2), Node(3)}};
  std::shared_ptr<CBSNode> _parent = std::make_shared<CBSNode>(parent);
  CBSNode node = CBSNode(_parent, constraints, solution, 2);

  EXPECT_EQ(parent, node.getParent().get()[0]);
  EXPECT_EQ(constraints.size(), node.getConstraints().size());
  for (size_t i = 0; i < constraints.size(); i++) {
    //    EXPECT_EQ(constraints[i].getA(), node.getConstraints()[i].getA());
    EXPECT_EQ(constraints[i].getAId(), node.getConstraints()[i].getAId());
    EXPECT_EQ(constraints[i].getN1(), node.getConstraints()[i].getN1());
    EXPECT_TRUE(constraints[i].getN2() == node.getConstraints()[i].getN2());
    EXPECT_EQ(constraints[i].getTimestamp(), node.getConstraints()[i].getTimestamp());
  }
  EXPECT_EQ(solution.size(), node.getSolution().size());
  for (size_t i = 0; i < solution.size(); i++) {
    EXPECT_TRUE(equalPaths(solution[i], node.getSolution()[i]));
  }
  EXPECT_EQ(2, node.getCost());
}

TEST(CBSNode, CBSNodeConstructor2)
{
  CBSNode parent = CBSNode();
  std::vector<Constraint> constraints = {};
  for (size_t i = 0; i < 100; i++) {
    constraints.push_back(Constraint(Node(rand() % 100+1), rand() % 100+1, Agent(rand() % 10)));
  };
  std::vector<std::vector<Node>> solution = {};
  size_t n_agents = rand() % 50 + 10;
  for (size_t i = 0; i < n_agents; i++) {
    solution.push_back({});
    size_t n_nodes = rand() % 100 + 10;
    for (size_t j = 0; j < n_nodes; j++) {
      solution.back().push_back(Node(rand() % 10000 + 1));
      if (solution.back().back().getV() != 1) {
      }
    }
  }
  std::shared_ptr<CBSNode> _parent = std::make_shared<CBSNode>(parent);
  CBSNode node = CBSNode(_parent, constraints, solution, 2);

  EXPECT_EQ(parent, node.getParent().get()[0]);
  EXPECT_EQ(constraints.size(), node.getConstraints().size());
  for (size_t i = 0; i < constraints.size(); i++) {
//    EXPECT_EQ(constraints[i].getA(), node.getConstraints()[i].getA());
    EXPECT_EQ(constraints[i].getAId(), node.getConstraints()[i].getAId());
    EXPECT_EQ(constraints[i].getN1(), node.getConstraints()[i].getN1());
    EXPECT_EQ(constraints[i].getN2(), node.getConstraints()[i].getN2());
    EXPECT_EQ(constraints[i].getTimestamp(), node.getConstraints()[i].getTimestamp());
  }
  EXPECT_EQ(solution.size(), node.getSolution().size());
  for (size_t i = 0; i < solution.size(); i++) {
    EXPECT_TRUE(equalPaths(solution[i], node.getSolution()[i]));
  }
  EXPECT_EQ(2, node.getCost());
}

#endif  // GTEST