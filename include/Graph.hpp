/**
* @author Enrico Saccon <enrico.saccon@unitn.it>
* @file Graph.hpp
* @brief Models a graph.
*/

#ifndef MAOF_GRAPHI_HPP
#define MAOF_GRAPHI_HPP

// Library includes
#include <GraphNode.hpp>
#include <Defines.hpp>
#include <Connection.hpp>

// System includes
#include <math.h>
#include <iostream>
#include <iomanip>

class Graph
{
public:
  enum class TYPE { INVALID, CON_MATRIX };

protected:
  std::vector<GraphNode> nodes;
  Graph::TYPE type;
  size_t initialNodes;

public:
  Graph(std::vector<GraphNode> _nodes = {}, Graph::TYPE _type = Graph::TYPE::INVALID)
  : nodes(_nodes), type(_type), initialNodes(_nodes.size())
  {
  }

/**************************************************************************************************/
/*                                        GETTERS                                                 */
/**************************************************************************************************/

  /**
   * @brief Get the type of the graph.
   * @return The type of the graph as GRAPH::TYPE.
   */
  [[nodiscard]] Graph::TYPE getType() const { return this->type; }
  /**
   * @brief Get the number of nodes in the graph.
   * @return The number of nodes in the graph.
   */
  [[nodiscard]] size_t getNNodes() const { return this->nodes.size(); }
  /**
   * @brief Get the number of nodes in the graph at the creation of the graph.
   * @details Some algorithms may change the nodes in the graph while running.
   * @return The number of nodes in the graph at the creation of the graph.
   */
  [[nodiscard]] size_t getInitialNNodes() const { return this->initialNodes; }
  /**
   * @brief Get the nodes in the graph.
   * @return A `std::vector` of `GraphNode`.
   */
  [[nodiscard]] std::vector<GraphNode> getNodes() const { return this->nodes; }
  /**
   * @brief Get a node in the graph.
   * @param id The id of the node to get.
   * @return A reference to the node with the given id.
   */
  [[nodiscard]] GraphNode & getNode(size_t id) { return this->nodes.at(id); }
  /**
   * @brief Return the underlying adjacency matrix.
   * @return A std::vector of std::vector of Connection.
   */
  [[nodiscard]] virtual std::vector<std::vector<Connection>> getMatrix() const = 0;
  /**
   * @brief Get the connection between two nodes.
   * @param a The id of the first node.
   * @param b The id of the second node.
   * @return A reference to the connection between the two nodes.
   */
  [[nodiscard]] virtual Connection& getConnection(size_t a, size_t b) = 0;
  /**
   * @brief Get the connection between two nodes.
   * @param a The first `GraphNode`.
   * @param b The second `GraphNode`.
   * @return A reference to the connection between the two nodes.
   */
  [[nodiscard]] Connection& getConnection(GraphNode a, GraphNode b) {
    return this->getConnection(a.getId(), b.getId());
  }
  /**
   * @brief Get the cost of the connection between two nodes.
   * @details If the `Connection` is not available at the given time, then the cost will be 0.
   * @param a The id of the first node.
   * @param b The id of the second node.
   * @param t The time at which the usage of the connection is requested, default INF.
   * @return The cost of the connection between the two nodes.
   */
  [[nodiscard]] virtual int getCost(size_t a, size_t b, size_t t = INF<size_t>()) const = 0;
  [[nodiscard]] int getCost(GraphNode a, GraphNode b, size_t t = INF<size_t>()) const {
    return this->getCost(a.getId(), b.getId(), t);
  }
  /**
   * @brief Check if the connection at a given time is available for a given agent.
   * @details If the `Connection` is not available, then the cost will be 0.
   * @param a The id of the first node.
   * @param b The id of the second node.
   * @param t The time at which the usage of the connection is requested.
   * @param a_id The id of the node that is requesting the connection.
   * @return The cost of the connection between the two nodes.
   */
  [[nodiscard]] virtual int getCost(size_t a, size_t b, size_t t, size_t a_id) const = 0;
  /**
   * @brief Check if the connection at a given time is available for a given agent.
   * @details If the `Connection` is not available, then the cost will be 0.
   * @param a The first `GraphNode`.
   * @param b The second `GraphNode`.
   * @param t The time at which the usage of the connection is requested.
   * @param a_id The id of the node that is requesting the connection.
   * @return The cost of the connection between the two nodes.
   */

  [[nodiscard]] int getCost(GraphNode a, GraphNode b, size_t t, size_t a_id) const {
    return this->getCost(a.getId(), b.getId(), t, a_id);
  }
  /**
   * @brief Get the neighbors of a node.
   * @param n_id The id of the node.
   * @param t The time at which the neighbors are requested, default INF.
   * @return A `std::vector` of `GraphNode`.
   */
  [[nodiscard]] virtual std::vector<GraphNode> getNeighbors(size_t n_id, size_t t = INF<size_t>()) const = 0;
  /**
   * @brief Get the neighbors of a node.
   * @param n_id The `GraphNode`.
   * @param t The time at which the neighbors are requested, default INF.
   * @return A `std::vector` of `GraphNode`.
   */
   [[nodiscard]] std::vector<GraphNode> getNeighbors(GraphNode n, size_t t = INF<size_t>()) const {
    return this->getNeighbors(n.getId(), t);
  }
  /**
   * @brief Get the ids of the neighbors of a node.
   * @param n_id The id of the node.
   * @param t The time at which the neighbors are requested, default INF.
   * @return A `std::vector` of `size_t`.
   */
  [[nodiscard]] virtual std::vector<size_t> getNeighborsIds(size_t n_id, size_t t = INF<size_t>()) const = 0;
  /**
   * @brief Get the ids of the neighbors of a node.
   * @param n_id The `GraphNode`.
   * @param t The time at which the neighbors are requested, default INF.
   * @return A `std::vector` of `size_t`.
   */
   [[nodiscard]] std::vector<size_t> getNeighborsIds(GraphNode n, size_t t = INF<size_t>()) const {
    return this->getNeighborsIds(n.getId(), t);
  }
  /**
   * @brief Get the number of neighbors of a node.
   * @param n_id The id of the node.
   * @param t The time at which the neighbors are requested, default INF.
   * @return The number of neighbors.
   */
  [[nodiscard]] virtual size_t getNNeighbors(size_t n_id, size_t t = INF<size_t>()) const = 0;
  /**
   * @brief Get the number of neighbors of a node.
   * @param n_id The `GraphNode`.
   * @param t The time at which the neighbors are requested, default INF.
   * @return The number of neighbors.
   */
  [[nodiscard]] size_t getNNeighbors(GraphNode n, size_t t = INF<size_t>()) const {
    return this->getNNeighbors(n.getId(), t);
  }
  /**
   * @brief Get the number of nodes with at least the given number of neighbors.
   * @param nNeigh The number of neighbors.
   * @return The number of nodes with the given number of neighbors.
   */
  [[nodiscard]] inline size_t getNodesWithDegree(size_t nNeigh = 3){
    size_t count = 0;
    for (const auto & node : this->nodes){
      if (getNNeighbors(node) >= nNeigh){
        count ++;
      }
    }
    return count;
  }

/**************************************************************************************************/
/*                                        METHODS                                                 */
/**************************************************************************************************/

  /**
   * @brief Add a time to a connection of the graph.
   * @details Check the documentation of `Connection::addTime(size_t, int)` for more information.
   * @param a The id of the first node of the connection.
   * @param b The id of the second node of the connection.
   * @param t The time to be added to the vector.
   */
  virtual void addTime(size_t a, size_t b, size_t t) = 0;
  /**
   * @brief Add a time to a connection of the graph.
   * @details Check the documentation of `Connection::addTime(size_t, int)` for more information.
   * @param a The first `GraphNode` of the connection.
   * @param b The second `GraphNode` of the connection.
   * @param t The time to be added to the vector.
   */
  virtual void addTime(GraphNode a, GraphNode b, size_t t) {
    this->addTime(a.getId(), b.getId(), t);
  };
  /**
   * @brief Add a time to a connection of the graph.
   * @details Check the documentation of `Connection::addTime(TM_ID)` for more information.
   * @param a The id of the first node of the connection.
   * @param b The id of the second node of the connection.
   * @param t The time to be added to the vector.
   */
  virtual void addTime(size_t a, size_t b, size_t t, size_t a_id) = 0;
  /**
   * @brief Add a time to a connection of the graph.
   * @details Check the documentation of `Connection::addTime(TM_ID)` for more information.
   * @param a The first `GraphNode` of the connection.
   * @param b The second `GraphNode` of the connection.
   * @param t The time to be added to the vector.
   */
  virtual void addTime(GraphNode a, GraphNode b, size_t t, size_t a_id) {
    this->addTime(a.getId(), b.getId(), t, a_id);
  };
  /**
   * @brief Add a constraint to the graph.
   * @param constraints The constraint to be added.
   * @param placeholders A pointer to a vector of `GraphNode` to be filled with the placeholders if
   * any were created due to the constraint.
   */
  virtual void addConstraints(
    std::vector<Constraint> constraints,
    std::vector<GraphNode> * placeholders = nullptr) = 0;

/**************************************************************************************************/
/*                                         OTHERS                                                 */
/**************************************************************************************************/

  [[nodiscard]] virtual bool equal(const Graph & g) const = 0;
  [[nodiscard]] virtual std::string toString(bool verbose = false) const = 0;

/**************************************************************************************************/
/*                                       OVERLOADS                                                */
/**************************************************************************************************/

  bool operator==(const Graph & g) const { return this->equal(g); }
  virtual explicit operator std::vector<std::vector<int>>() const = 0;

/**************************************************************************************************/
/*                                         FRIENDS                                                */
/**************************************************************************************************/

  friend std::ostream & operator<<(std::ostream & stream, const Graph & g)
  {
    stream << g.toString();
    return stream;
  }
};

#endif  //MAOF_GRAPHI_HPP
