/** 
 * @author Enrico Saccon <enrico.saccon@unitn.it>.
 * @file ArgParser.hpp
 * @brief File that contain the parser for the argument to the program.
 */

#ifndef MAOF_ARGPARSER_HPP
#define MAOF_ARGPARSER_HPP

#include <Utils.hpp>

#include <iostream>
#include <string>
#include <map>

class ArgParser {
private:
  std::string description;
  std::map<std::string, std::string> argvType;
  std::map<std::string, void *> argvPointer;

  std::string removeDash (std::string str){
    for (size_t i=0; i<str.size(); i++){
      if (str[i] == '-'){
        str.erase(i, 1);
        i--;
      }
    }
    return str;
  }

  bool getArg(const std::string & aName, const std::string & argVal) {
    std::string argName = this->removeDash(aName);
    if (argvType.find(argName) == argvType.end()) {
      ERROR("Argument @ not found", argName.c_str());
    }
    else if (argName == "h" || argName == "help") {
      std::cout << this->description << std::endl;
      exit(1);
    }
    else if (argvType[argName] == "bool") {
      bool * ptr = (bool *) argvPointer[argName];
      *ptr = true;
      return false;
    }
    else {
      if (argvPointer[argName] == nullptr) {
        ERROR("Argument @ does not point a correct area of memory", argName);
      }
      if (argvType[argName] == "int") {
        int * ptr = (int *)argvPointer[argName];
        *ptr = std::stoi(argVal);
      } else if (argvType[argName] == "size_t") {
        std::size_t * ptr = (std::size_t *)argvPointer[argName];
        *ptr = std::stoull(argVal);
      } else if (argvType[argName] == "float") {
        float * ptr = (float *)argvPointer[argName];
        *ptr = std::stoi(argVal);
      } else if (argvType[argName] == "double") {
        double * ptr = (double *)argvPointer[argName];
        *ptr = std::stod(argVal);
      } else if (argvType[argName] == "string") {
        std::string * ptr = (std::string *)argvPointer[argName];
        *ptr = argVal;
      }
      return true;
    }

  }

public:
  ArgParser() : description("") {
    argvType["h"] = "bool";
    argvType["help"] = "bool";
  };

  ArgParser(std::string _description) : description(_description) {
    argvType["h"] = "bool";
    argvType["help"] = "bool";
  };

  template <typename T>
  T addArg(std::string shortName, std::string type, void * pointer, T defaultValue = T(), std::string longName = "") {
    std::string sName = this->removeDash(shortName);
    if (argvType.find(sName) != argvType.end()) {
      ERROR("Argument @ already exists", sName.c_str());
    }
    argvType[sName] = type;
    argvPointer[sName] = pointer;
    if (longName != "") {
      std::string lName = this->removeDash(longName);
      if (argvType.find(lName) != argvType.end()) {
        ERROR("Argument @ already exists", lName.c_str());
      }
      argvType[lName] = type;
      argvPointer[lName] = pointer;
    }
    return defaultValue;
  }

  void parseArgs(int argc, char ** argv, bool skipFirst = true) {
    for (int i = (skipFirst ? 1 : 0); i < argc; i++) {
      if (i < argc-1){
        if (this->getArg(argv[i], argv[i+1])){
          i++;
        }
      }
      else {
        getArg(argv[i], "");
      }
    }
  }
};

#endif  //MAOF_ARGPARSER_HPP
