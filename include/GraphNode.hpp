/**
 * @author Enrico Saccon (enrico.saccon@studenti.unitn.it)
 * @file GraphNode.hpp
 * @brief Models the node of the environment graph.
 */

#ifndef MAOF_GRAPHNODE_HPP
#define MAOF_GRAPHNODE_HPP

// Library includes
//#include <ankerl/unordered_dense.h>

// System includes
#include <iostream>
#include <string>

class GraphNode
{
private:
  size_t id;
  int value;
  int x, y;

public:
  GraphNode() : id(0), value(0), x(0), y(0) {};
  GraphNode(int _id, int _value, int _x=0, int _y=0) : id(_id), value(_value), x(_x), y(_y) {};
  explicit GraphNode(int _value)
  {
    if (_value>0){
      this->id = _value-1;
      this->value = _value;
    }
    else {
      throw std::runtime_error("Cannot use one argument constructors with non positives values "+std::to_string(_value));
    }
  }

  [[nodiscard]] size_t getId() const { return this->id; }
  [[nodiscard]] int getV() const { return this->value; }
  int getX() const { return this->x; }
  int getY() const { return this->y; }

  void setId(size_t _id) { this->id = _id; }
  void setV(int _value) { this->value = _value; }
  void setX(int _x) { this->x = _x; }
  void setY(int _y) { this->y = _y; }

  [[nodiscard]] bool empty() const { return this->getId()==0 && this->getV()==0; }

  bool operator== (GraphNode node) const {
    return (this->empty() && node.empty()) || (!this->empty() && !node.empty() && this->getId() == node.getId());
  }
  bool operator!= (GraphNode node) const { return !(*this == node); }
  bool operator<(GraphNode node) const { return this->getId() < node.getId(); }
  bool operator>(GraphNode node) const { return this->getId() > node.getId(); }

  std::string toString(size_t verbose = 0) const {
    std::string ret = "";
    switch (verbose) {
      case 1:
        ret = std::to_string(this->value);
        break;
      case 2:
        ret = std::to_string(this->id);
        break;
      default:
        ret = "("+std::to_string(this->value)+", "+std::to_string(this->id)+")";
    }
    return ret;
  }

  friend std::ostream & operator<<(std::ostream & stream, const GraphNode & node)
  {
    stream << node.toString(0);
    return stream;
  }
//  friend ankerl::unordered_dense::hash<GraphNode>;
};

struct GraphNodeHasher
{
  size_t operator()(const GraphNode& n) const
  {
    return n.getId();
  }
};

//template <>
//struct ankerl::unordered_dense::hash<GraphNode>
//{
//  using is_avalanching = void;
//
//  [[nodiscard]] auto operator()(GraphNode const & n) const noexcept -> uint64_t
//  {
//    return detail::wyhash::hash(n.id);
//  }
//};

#endif  // MAOF_GRAPHNODE_HPP
