/**
 * @author Enrico Saccon (enrico.saccon@unitn.it)
 * @file Utils.hpp
 * @brief Contains a number of utility functions, from debugging to string formatting.
 */

#ifndef MAOF_UTILS_HPP
#define MAOF_UTILS_HPP

// Define print macros if MAOF_DEBUG is not defined
#ifndef INFO
#define INFO(...)
#endif
#ifndef INFO_COL
#define INFO_COL(str, col)
#endif
#ifndef WARNING
#define WARNING(...)
#endif
#ifndef ERROR
#define ERROR(...) throw std::runtime_error("<<"+tprintf(__VA_ARGS__)+tprintf(">> in file @ at line @", __FILE__, __LINE__));
#endif
#ifndef FUNCTION_NAME
#define FUNCTION_NAME(name, var)
#endif
#ifndef PARSER
#define PARSER(...)
#endif

// If MAOF_DEBUG is defined, no matter the verbosity, define the following macros
#ifdef MAOF_DEBUG
#undef FUNCTION_NAME
#define FUNCTION_NAME(name, var)    \
  if (!var) {                       \
    std::cout << name << std::endl; \
    var = true;                     \
  }
#endif // MAOF_DEBUG

// If MAOF_DEBUG is defined as verbose, then add the following macros
#if MAOF_DEBUG > 1
#undef INFO
#define INFO(...) std::cout << tprintf(__VA_ARGS__) << std::endl; std::flush(std::cout);
#define INFO_COL(str, color) std::cout << Color(color).to_string() << str << "\033[0m" << std::endl; std::flush(std::cout);
#undef WARNING
#define WARNING(...) std::cerr << "\033[1;31m" << tprintf(__VA_ARGS__) << "\033[0m" << std::endl; std::flush(std::cerr);
#endif //VERBOSE

// If MAOF_PARSER is defined, then define the following macros
#ifdef MAOF_PARSER
#undef PARSER
#define PARSER(...) std::cout << tprintf(__VA_ARGS__) << std::endl; std::flush(std::cout);
#endif // MAOF_PARSER

// Library includes
#include <GraphNode.hpp>
#include <Timeperf.hpp>

// System includes
#include <sstream>
#include <string>
#include <vector>
#include <future>
#include <map>
#include <algorithm>
#include <cmath>
#include <iomanip>
#include <limits>

/**
 * @brief Struct that defines the possible colors for the text.
 */
struct Color {
  enum CODE {
    RED = 31,
    GREEN = 32,
    YELLOW = 33,
    BLUE = 34,
    PURPLE = 35,
    CYAN = 36,
    WHITE = 37,
    DEFAULT = 39
  };
  CODE code;
  Color(CODE code) { this->code = code; }
  std::string to_string() const { return "\033[" + std::to_string(code) + "m"; }
};

/**
 * @brief Struct that returns the infinity value for a given type.
 * @tparam T The type of the value to be returned.
 */
template<class T>
struct INF{
  static constexpr T val = std::numeric_limits<T>::max();

  constexpr operator T() const { return val; }
};

/**
 * @brief Function that returns a string with the paths in the solution
 * @param sol A std::vector<std::vector<GraphNode>> containing the solution with the different paths
 * @param separator A string containing the separator to be used between nodes
 * @return A formatted string
 */
std::string multiPathToString(
  const std::vector<std::vector<GraphNode>> & sol, std::string separator = " ");

/**
 * @brief Function that returns a string with the nodes in the path
 * @param path A std::vector containing the path
 * @param separator A string containing the separator to be used between nodes
 * @return A formatted string
 */
std::string pathToString(const std::vector<GraphNode> & path, std::string separator = " ");

/**************************************************************************************************/

/**
 * @brief Overload of operator << for std::vector
 * @tparam T The type of the elements of the vector
 * @param stream The stream to which the vector will be printed
 * @param v The vector to be printed
 * @return The stream
 */
template<typename T>
std::ostream & operator<<(std::ostream& stream, const std::vector<T>& v)
{
  stream << "[";
  for (size_t i=0; i<v.size(); i++){
    stream << v.at(i) << " ";
  }
  stream << "]";
  return stream;
}

/**************************************************************************************************/

/**
 * @brief Overload of operator << for std::map
 * @tparam T1 The type of the keys of the map
 * @tparam T2 The type of the values of the map
 * @param stream The stream to which the map will be printed
 * @param m The map to be printed
 * @return The stream
 */
template<typename T1, typename T2>
std::ostream & operator<<(std::ostream& stream, const std::map<T1, T2>& m)
{
  for (const auto& e : m) {
    stream << "[" << e.first << "] -> [" << e.second << "]" << std::endl;
  }
  return stream;
}

/**************************************************************************************************/

/**
 * @brief Base case of template recursion for tprintf
 * @tparam T The type of the value to be printed
 * @param value The value to be printed
 * @return A string containing the value
 */
template<typename T>
inline std::string tprintf(T value)
{
  std::stringstream sstr;
  sstr << value;
  return sstr.str();
}

/**
 * @brief TMP function that prints the string with the values
 * @details The `fmt` string must contain `@` as placeholders for the values. For example
 * `tprintf("Hello @", "world")` will return `Hello world`.
 * @tparam T The type of the first value to be printed
 * @tparam Targs Parameter pack
 * @param fmt The string to be printed with `@` as placeholders for the values
 * @param next The next value to be added inside the `fmt` string
 * @param Fargs The rest of the values to be added inside the `fmt` string
 * @return A string containing the formatted string
 */
template<typename T, typename... Targs>
std::string tprintf(const char* fmt, T next, Targs... Fargs)
{
  std::stringstream sstr;
  for (; *fmt != '\0'; fmt++)
  {
    if (*fmt == '@')
    {
      sstr << next << tprintf(fmt + 1, Fargs...);
      return sstr.str();
    }
    sstr << *fmt;
  }

  return "";
}

/**************************************************************************************************/

/**
 * @brief Function that pretty-prints a table with the paths
 * @param paths A vector containing the paths
 * @return A string containing the table
 */
inline std::string printTablePaths(const std::vector<std::vector<GraphNode>> & paths){
  std::vector<std::string> printings = {};
  for (const auto& v : paths){
    for (const GraphNode& n : v) {
      printings.push_back(n.toString(1));
    }
  }
  size_t maxStr = std::max_element(printings.begin(), printings.end(), [](const std::string& s1, const std::string& s2){ return s1.size() < s2.size(); })->size();
  size_t maxPath = std::max_element(paths.begin(), paths.end(), [](const auto& p1, const auto& p2){ return p1.size() < p2.size(); })->size();

  maxStr = std::ceil(log10(maxPath)) > maxStr ? std::ceil(log10(maxPath)) : maxStr;

  std::stringstream stream;
  // Print blank spaces at the beginning of first row
  for(size_t i = 0; i <= log10(paths.size()); i++){ stream << " ";  }
  // Print first row of timestamps
  for(size_t i = 0; i < maxPath; i++) { stream << std::setw(maxStr+1) << i; }
  stream << std::endl;
  // Print the other rows
  size_t counter = 0;
  for(size_t i = 0; i < paths.size(); i++){
    stream << std::setw(log10(paths.size())) << i+1;
    for(size_t j = 0; j < paths[i].size(); j++){
      stream << std::setw(maxStr+1) << printings[counter];
      counter++;
    }
    stream << std::endl;
  }
  stream << std::endl;

  return stream.str();
}

/**************************************************************************************************/

/**
 * @brief Function that checks if two pairs with the same times are equal.
 * @tparam T1 The first type of the pairs
 * @tparam T2 The second type of the pairs
 * @param p1 The first pair
 * @param p2 The second pair
 * @return True if the pairs are equal, false otherwise
 */
template<class T1=size_t, class T2=size_t>
bool equalPairs(const std::pair<T1,T2>& p1, const std::pair<T1,T2> p2)
{
  return p1.first==p2.first && p1.second==p2.second;
}

/**************************************************************************************************/

/**
 * @brief Function that converts a string to uppercase
 * @details I thought that C++ would have had a nice function for this, and instead... https://stackoverflow.com/questions/735204/convert-a-string-in-c-to-upper-case
 * @param str The string to be converted to uppercase
 */
inline void s_toupper (std::string & str) {
  std::transform(str.begin(), str.end(), str.begin(), ::toupper);
}

/**************************************************************************************************/

#ifdef MAOF_DEBUG
#if MAOF_DEBUG == 1
#ifndef ASSERT_DURATION_LE
#define ASSERT_DURATION_LE(secs, stmt) { \
  std::promise<bool> completed; \
  auto stmt_future = completed.get_future(); \
  std::thread([&](std::promise<bool>& completed) { \
    stmt; \
    completed.set_value(true); \
  }, std::ref(completed)).detach(); \
  if(stmt_future.wait_for(std::chrono::seconds(secs)) == std::future_status::timeout) \
    GTEST_FATAL_FAILURE_("timed SPTests (> " #secs \
    " seconds)."); \
}
#endif // ASSERT_DURATION_LE
#endif // MAOF_DEBUG
#endif // MAOF_DEBUG

#endif  //MAOF_UTILS_HPP
