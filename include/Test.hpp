/**
 * @author Enrico Saccon (enrico.saccon@unitn.it)
 * @file Test.hpp
 * @brief Contains functions that can be used to test the code against examples through JSON.
 */

#ifndef MAOF_TEST_HPP
#define MAOF_TEST_HPP

// System defines
#include <filesystem>
#include <fstream>
#include <iostream>
#include <vector>
namespace fs = std::filesystem;

// Library defines
#include <ConMatrix.hpp>
#include <Defines.hpp>
#include <MAPF/iMAPF.hpp>
#include <SAPF/iSAPF.hpp>
#include <Timeperf.hpp>
#include <nlohmann/json.hpp>

MAPFSolver * readJSON(
  const std::string& fileName, std::string mapfSolver = "", std::string costFunction = "" , std::string sapfSolver = "",
  std::string heuristic = "none", std::string subSolver = "none");
void runTest(
  std::string json_file, std::string mapfSolver = "", std::string costFunction = "", std::string sapfSolver = "",
  std::string heuristic = "none", std::string subSolver = "none");
void runTests(
  std::string directory, std::string mapfSolver = "", std::string costFunction = "", std::string sapfSolver = "",
  std::string heuristic = "none", std::string subSolver = "none");

std::shared_ptr<Graph> readSAPFJSON(std::vector<GraphNode>& sol, GraphNode& initPos, GraphNode& endPos, std::vector<GraphNode>& goals, const std::string& fileName);

/**
 * @brief Function that checks whether two paths are the same or not
 * @param path1 The first path to be compared
 * @param path2 The second path to be compared
 * @return `true` if the two paths are the same
 * @return `false` if the two paths are different
 */
bool equalPaths(const std::vector<GraphNode> & path1, const std::vector<GraphNode> & path2);

bool bothValid(const std::vector<GraphNode> & path1, const std::vector<GraphNode> & path2, const std::vector<std::vector<int>>& connect);
bool bothValid(const std::vector<GraphNode> & path1, const std::vector<GraphNode> & path2, const std::vector<std::vector<Connection>>& connect);

bool equalMultiPaths(const std::vector<std::vector<GraphNode>> & paths1, const std::vector<std::vector<GraphNode>> & paths2);
bool validMultiPaths(const std::vector<std::vector<GraphNode>> & paths1, const std::vector<std::vector<GraphNode>> & paths2, std::vector<Agent> agents = {});


#endif  //MAOF_TEST_HPP
