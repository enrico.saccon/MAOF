/**
 * @author Enrico Saccon
 * @file Constraint.hpp
 * @brief Contains the definition of the class that models a constraint.
 */

#ifndef MAOF_CONSTRAINT_HPP
#define MAOF_CONSTRAINT_HPP

class Constraint; // Forward declaration

// Library includes
#include <Agent.hpp>
#include <Conflict.hpp>

// System includes
#include <vector>

/**
 * @brief Class used to store information regarding a constraint. It stores both vertex and swap
 * conflicts, in the first case n2 is empty, in the second one it has a value. It also stores the
 * time step at which the constraint should be enforced and the agent which should respect the
 * constraint.
 */
class Constraint
{
private:
  GraphNode n1;      ///The node on which the agent should not be at the time step specified
  GraphNode n2;      ///The node on which the agent should not be at the time step specified+1. Defined only if it is a swap conflict
  size_t timestamp;  ///The time step at which the constraint should be enforced
  int a_id;          /// The agent which should respect the constraint. If it's -1, then any agent is restricted

public:
  Constraint() : n1(GraphNode()), n2(GraphNode()), timestamp(0), a_id(0){};

  Constraint(GraphNode _n1, size_t _timestamp, size_t _a)
  : n1(_n1), n2(GraphNode()), timestamp(_timestamp), a_id(_a){};

  Constraint(GraphNode _n1, GraphNode _n2, size_t _timestamp, size_t _a)
  : n1(_n1), n2(_n2), timestamp(_timestamp), a_id(_a){};

  Constraint(GraphNode _n1, size_t _timestamp, Agent _a)
  : n1(_n1), n2(GraphNode()), timestamp(_timestamp), a_id(_a.getID()){};

  Constraint(GraphNode _n1, GraphNode _n2, size_t _timestamp, Agent _a)
  : n1(_n1), n2(_n2), timestamp(_timestamp), a_id(_a.getID()){};

  [[nodiscard]] GraphNode getN1() const { return this->n1; }
  [[nodiscard]] GraphNode getN2() const { return this->n2; }
  [[nodiscard]] size_t getTimestamp() const { return this->timestamp; }
  [[nodiscard]] size_t getAId() const { return this->a_id; }

  void setN1(GraphNode n) { this->n1 = n; }
  void setN2(GraphNode n) { this->n2 = n; }
  void setTimestamp(size_t ts) { this->timestamp = ts; }
  void setAId(size_t _a) { this->a_id = _a; }

  bool operator==(Constraint cons) const
  {
    return this->getN1() == cons.getN1() && this->getN2() == cons.getN2() &&
           this->getAId() == cons.getAId() && this->getTimestamp() == cons.getTimestamp();
  }

  /**
   * @brief Check if a constraint is violated given two nodes and a timestamp.
   * @param n1 The first node which may cause the violation of the constraint.
   * @param t The timestamp considered.
   * @param n2 The second node which is set only when a swap conflict is considered.
   * @return CONFLICT::NONE when the constraint is valid given the nodes and the timestamp.
   * @return CONFLICT::VERTEX when the constraint is violated by a vertex conflict.
   * @return CONFLICT::SWAP when the constraint is violated by a swap conflict.
   */
  Conflict::TYPE checkConflict(GraphNode n1, size_t t, GraphNode n2 = GraphNode());
  /**
   * @brief Check if a constraint is violated in a path.
   * @param path a vector containing the node occupied by the agent at each timestamp.
   * @return CONFLICT::NONE when the constraint is valid given the path.
   * @return CONFLICT::VERTEX when the constraint is violated by a vertex conflict in the path.
   * @return CONFLICT::SWAP when the constraint is violated by a swap conflict in the path.
   */
  Conflict::TYPE checkConflict(std::vector<GraphNode> path);

  friend std::ostream & operator<<(std::ostream & stream, const Constraint & c)
  {
    stream << "<a: " << c.getAId() << " n1: " << c.getN1() << " n2: " << c.getN2()
           << " t: " << c.getTimestamp() << ">";
    return stream;
  }
};

std::vector<Constraint> getConstraintsFromConflicts(Conflict conflict);


#endif  //MAOF_CONSTRAINT_HPP
