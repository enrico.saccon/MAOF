# Multi-Agent Path Finding Solver

This folder contains different MAPF solvers that can be used. 

A brief list of the solvers implemented is the following. For more information, see the corresponding README inside each
folder.

- CBS [1]: uses a constraint tree to manage the conflicts between different agents after having initially solved each
  SAPF problem.
- ICTS [2]: initially solves the SAPF problems for each agent and then manages the conflicts by iteratively increasing
  of one step the agents' paths.
- CP [3]: this algorithm using constraint programming takes from the work of Barták et al. (2019). The constraints have
  been revised and implemented using CPLEX (implementation with o-tools from Google coming soon).
- ICR: novel solver which first solves the SAPF problems and then for each conflict it extracts a sub-graph around the
  conflict and considers only the agents involved with the sub-graph and calls another MAPF solver to solve the 
  sub-problem.
- PP: each agent is associated with a priority and the algorithm solves the SAPF problem in order of priority.

| Solver | Optimality  | Complete |
|--------|-------------|----------|
| CBS    | Optimal     | Yes      |
| ICTS   | Optimal     | Yes      |
| CP     | Optimal     | Yes      |
| ICR    | Sub-optimal | Yes      |
| PP     | Sub-optimal | No       |


Here is a table showing which are implemented and which still need to be worked on:

| Solver | Implemented         |
|--------|---------------------|
| CBS    | Not finished yet    |
| ICTS   | Finished            |
| CP     | Finished            |
| ICR    | Finished            |
| PP     | Not implemented yet |

## Code structure

### MAPFSolver

In the file `MAPFSolver.hpp`, the abstract class for `MAPFSolver` from which all the other solvers inherits is defined.
Moreover, the typedef `MAPF_TYPE` is provided to describe the different types of MAPF solvers available. 
See the corresponding Doxygen documentation for more information.

In the file `iMAPF.hpp`, there are two functions that can be used to choose the correct `MAPFSolver`. They will return a 
pointer to a `MAPFSolver` which can then be simply cast to the chosen solver.

### Cost functions 

File `CostFunction.hpp` contains the functions that can be used as cost functions. Notice that not all MAPF solvers
accept every cost function. The two that are currently implemented are:

- Makespan: returns the maximum length between the agents' paths.
- Sum of costs: returns the sum of the lengths of the agents' paths.

A typedef `COST_FUNCTION` defines the different cost functions that are available. For a complete list refer to the file
documentation:

| Function     | `COST_FUNCTION` |
|--------------|-----------------|
| Makespan     | MKS             |
| Sum of costs | SIC             |


The file provides the function `costFunction` which takes the type of the cost function and the paths, and returns the
value of the required cost function.  For example, the call:
```c++
costFunction(COST_FUNCTION::MKS, {{1,2,3},{2,3,4,5}});
```
will return 4 as the second path `{2,3,4,5}` is the longest one and has length 4.

### Constraints

While it was first created for CBS, it can be used in a number of different solvers. Refer to the file documentation 
for more information. 

## Contributing

You are more than welcome to contribute to this repository. If you add a new MAPF solver, please remember the following
points in order to maintain the structure of the repository:

1. It should have its own repository with a name (or acronym) that identifies it.
2. It must (for obvious reasons) implement all the virtual functions that are provided by the `MAPFSolver` class. In
  particular, remember to correctly implement the functions that allow other solvers to work, for example the
  `solveICR()` function.
3. Unless absolutely necessary, avoid using `std::cout` and co. directly, but please use the functions provided in 
  `Utils.hpp` to print out pieces of information (`INFO()`), warnings (`WARNING()`) and errors (`ERROR()`).
4. In the directory `test`, you should create a new test file for the new solver taking from the already provided test
  files. You are free to then add more tests, but the baseline has to be met. 
5. In the directory `testingScripts`, a tester is provided which also parses the output of the executable called. 
  Consider contributing to the `parse.yaml` (and/or `parse.json`) file with regrex that you think may be usefull when 
  running your solver. Refer to the `testingScripts/test` directory for more information.

[1] Sharon, G., Stern, R., Felner, A., & Sturtevant, N. R. (2015). Conflict-based search for optimal multi-agent pathfinding. Artificial Intelligence, 219, 40–66. [https://doi.org/10.1016/j.artint.2014.11.006](https://doi.org/10.1016/j.artint.2014.11.006)
[2] Sharon, G., Stern, R., Goldenberg, M., & Felner, A. (2013). The increasing cost tree search for optimal multi-agent pathfinding. Artificial Intelligence, 195, 470–495. [https://doi.org/10.1016/j.artint.2012.11.006](https://doi.org/10.1016/j.artint.2012.11.006)
[3] Barták, R., & vancara, J. (2019). On SAT-Based Approaches for Multi-Agent Path Finding with the Sum-of-Costs Objective. SOCS.