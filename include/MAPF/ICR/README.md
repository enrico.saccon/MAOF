# Increasing Conflict Resolution (ICR)

This takes from the concept of LNS (Large Neighborhood Search). The initial step is to solve each SAPF problem. Then
each time a conflict is found, a subgraph considering only the agents that are interested is extracted. The conflict is
resolved using another MAPF solver and then merged with the previous solution. 

This allows to reduce the computational complexity of calling MAPF solvers on large maps. 

The subgraph is extracted by considering the conflict node and then extending the subgraph to adjacent nodes until an
intersection node is included. If a solution has been found inside the subgraph, then it's merged, otherwise the 
subgraph is extended to other neighboring nodes until another intersection node is added. The algorithm continues
until a feasible solution is found, or until the maximum number of intersections has been included. If a solution is not 
found within the considered number of intersections, then the whole map is considered.

If the considered subgraph becomes the whole map, then the whole map is solved using the chosen MAPF sub-solver.

At the time of writing, no known bound on the optimality of the solution can be placed.

## Structure

The solver inherits from the `MAPFSolver` class. It adds some private variables:

- `subSolverType`: a `MAPF_TYPE` which states the sub-solver to be used to solve the local conflicts.
- `cf`: a `COST_FUNCTION` that represent the cost function that should be minimized. Since the first step of ICR is to 
  solve the SAPF problems and then to call a MAPF sub-solver, the cost function is actually going to be minimized by the
  sub-solver. 
- `n_hops`: a positive integer value that stats the maximum number of hops to consider. 

For a detailed description of the methods and functions available, please look at the file documentation. 
