/**
 * @author Enrico Saccon (enrico.saccon@unitn.it)
 * @file MAPFSolver.hpp
 * @brief Header file containing the abstract class inherited by the MAPF solvers.
 */

#ifndef MAOF_MAPFSOLVER_HPP
#define MAOF_MAPFSOLVER_HPP

// Library includes
#include <Defines.hpp>
#include <MAPF/CostFunction.hpp>
#include <Agent.hpp>

// System includes
#include <vector>
#include <memory>

class MAPFSolver
{
protected:
  std::vector<Agent> agents;

public:
  MAPFSolver() : agents({})
  {
  }

  MAPFSolver(std::vector<Agent>& _agents)
  : agents(_agents)
  {
  }

  virtual ~MAPFSolver() = 0;

  [[nodiscard]] std::vector<Agent> getAgents() const { return this->agents; }
  [[nodiscard]] Agent* getAgent(size_t aId) {
    if (aId < this->agents.size()) {
      return &(this->agents[aId]);
    }
    else {
      throw std::runtime_error(tprintf("Agent @ is not in vector of size @", aId, this->agents.size()));
    }
  }
  void setAgents(std::vector<Agent>& _agents) { this->agents = _agents; }

  void setAgent(size_t aId, Agent _agent) {
    if (aId < this->agents.size()) {
      this->agents[aId] = _agent;
    }
    else {
      throw std::runtime_error(tprintf("Agent @ is not in vector of size @", aId, this->agents.size()));
    }
  }

  void addAgent(const Agent& a)
  {
    if((size_t)a.getID() != this->agents.size()) { throw  std::runtime_error("Cannot add agent to solver because the id is out of order"); }
    this->agents.push_back(a);
  }

  void removeAgent(size_t id)
  {
    auto el = std::find_if(this->agents.begin(), this->agents.end(), [id](Agent& a){ return a.getID() == id; });
    if (el != this->agents.end()){
      this->agents.erase(el);
    }
  }

  /**
   * Check that the matrix contains only edges of cost 1. If it doesn't, then it adds nodes to reduce the cost.
   */
  void connectToUnit();
  virtual void postProcess(
    std::vector<std::vector<GraphNode>> &) = 0;  //TODO check if this function is actually needed

  [[nodiscard]] virtual std::string out() const = 0;

  /**
   * @attention The derived solvers should take care of assigning the paths inside the solution to the correct agents.
   * @return
   */
  virtual std::vector<std::vector<GraphNode>> solve() = 0;
  virtual std::vector<std::vector<GraphNode>> solveICR(const std::vector<size_t>& times) = 0;
};

static const std::vector<std::string> MAPF_NAMES {
  "NONE", "CBS", "ICTS", "CP", "ICR", "PP"
};
enum class MAPF_TYPE {
  NONE, CBS, ICTS, CP, ICR, PP
};

inline std::string mapfName(MAPF_TYPE value)
{
  return MAPF_NAMES[static_cast<size_t>(value)];
}

inline MAPF_TYPE mapfType(std::string value)
{
  auto it = std::find(MAPF_NAMES.begin(), MAPF_NAMES.end(), value);
  if (it == MAPF_NAMES.end()) { return MAPF_TYPE::NONE; }
  else {
    size_t index = std::distance(MAPF_NAMES.begin(), it);
    return static_cast<MAPF_TYPE>(index);
  }
}

#endif  //MAOF_MAPFSOLVER_HPP
