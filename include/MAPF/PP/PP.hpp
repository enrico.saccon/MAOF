/*!
 * @author Enrico Saccon (enrico.saccon@studenti.unitn.it)
 * @file PP.hpp
 * @brief Prioritized planning, with fixed priorities
 */

#ifndef MAOF_PP_HPP
#define MAOF_PP_HPP

// Library includes
#include <MAPF/MAPFSolver.hpp>
#include <MAPF/Constraint.hpp>

class PPSolver : public MAPFSolver {
public:
  PPSolver() : MAPFSolver() {}
  explicit PPSolver(std::vector<Agent>& _agents) : MAPFSolver(_agents)
  {
    this->sortAgents();
  }

//  PPSolver(std::vector<int> _priorities, size_t NAgents, const std::vector<Node> & _nodes,
//           const std::vector<Node> & _initPos, const std::vector<Node> & _endPos,
//           const std::vector<std::vector<Node>> & _goalPos,
//           const std::vector<std::vector<Connection>> & _connect, SAPFSolver* _sapfSolver)
//    : MAPFSolver(NAgents, _nodes, _initPos, _endPos, _goalPos, _connect, _sapfSolver)
//  {
//    for(size_t i = 0; i < NAgents; i++){
//      this->agents[i].setPriority(_priorities[i]);
//    }
//    this->sortAgents();
//  }
//  PPSolver(std::vector<int> _priorities, size_t NAgents, const std::vector<Node> & _nodes,
//           const std::vector<Node> & _initPos, const std::vector<Node> & _endPos,
//           const std::vector<std::vector<Node>> & _goalPos,
//           const std::vector<std::vector<int>> & _connect, SAPFSolver* _sapfSolver)
//  : MAPFSolver(NAgents, _nodes, _initPos, _endPos, _goalPos, _connect, _sapfSolver)
//  {
//    for(size_t i = 0; i < NAgents; i++) {
//      this->agents[i].setPriority(_priorities[i]);
//    }
//    this->sortAgents();
//  }

  void sortAgents(bool (*func)(const Agent& a1, const Agent a2) = nullptr);
  std::vector<std::vector<GraphNode>> solve() override;
  std::vector<std::vector<GraphNode>> solveICR(const std::vector<size_t> & times) override { return this->solve(); }

  void postProcess(std::vector<std::vector<Node>> &) override {}

  [[nodiscard]] std::string out() const override
  {
    std::stringstream stream;
    stream << "Using PP" << std::endl;
    stream << "Number of agents: " << this->getAgents().size() << std::endl;
    stream << "Nodes: [";
    SAPFSolver * sapfSolver = this->getAgents()[0].getSolver();
    for (auto n : sapfSolver->getGraph()->getNodes()) {
      stream << n << " ";
    }
    stream << "]\n";

    stream << "Agents:" << std::endl;
    for (const Agent & a : this->getAgents()) {
      stream << tprintf("- @ @->@ goals: [", a, a.getInitPos(), a.getEndPos());
      for (auto n : a.getGoals()) {
        stream << n << " ";
      }
      stream << "]\n";
    }
    stream << "Graph: \n";
    stream << *(this->getAgents()[0].getSolver()->getGraph());

    return stream.str();
  }

  friend std::ostream & operator<<(std::ostream & stream, const PPSolver & sol)
  {
    stream << sol.out();
    return stream;
  }

};

#endif  //MAOF_PP_HPP
