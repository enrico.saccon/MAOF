/**
 * @author Enrico Saccon <enrico.saccon@unitn.it>.
 * @file CP.hpp
 * @brief 
 */

#ifndef MAOF_CP_HPP
#define MAOF_CP_HPP

// System includes
#include <vector>
#include <numeric>

// Library includes
#include <MAPF/iMAPF.hpp>
#include <MAPF/Constraint.hpp>
#include <Utils.hpp>

#ifdef CPLEX_FOUND  // I cannot find a better way of removing this header file from the include files of cmake.
#include <ilcplex/ilocplex.h>
#include <ilcp/cp.h>

#ifndef FOREACH
#define FOREACH(x, nodes) \
  for (IloInt x = nodes.getFirst(); x < nodes.getLast(); x = nodes.getNext(x))
#endif  // FOREACH

#ifdef DISPLAY_RESULTS
#undef DISPLAY_RESULTS
#endif  // DISPLAY_RESULTS

#if MAOF_DEBUG > 1
#define DISPLAY_RESULTS()                                                          \
  std::cout << " --------------------------------------------------" << std::endl; \
  std::cout << "Solution status: " << cplex.getStatus() << std::endl;              \
  std::cout << std::endl << "Solution found:" << std::endl;                        \
  std::cout << " Objective value = " << cplex.getObjValue() << std::endl;          \
  std::cout << "--------------------------------------------------" << std::endl;  \
  std::cout << "Movement of the agents on the nodes:" << std::endl;                \
  FOREACH(s, stepsR)                                                                \
  {                                                                                \
    std::cout << "Step " << s << std::endl;                                        \
    FOREACH(n, nodesR)                                                              \
    {                                                                              \
      int p = 0;                                                                   \
      FOREACH(a, agentsR)                                                           \
      {                                                                            \
        if ((bool)(cplex.getValue(x[s][n][a]))) {                                  \
          p = a + 1;                                                               \
          break;                                                                   \
        }                                                                          \
      }                                                                            \
      std::cout << p << " ";                                                       \
    }                                                                              \
    std::cout << std::endl;                                                        \
  }                                                                                \
  std::cout << "--------------------------------------------------" << std::endl;  \
  std::cout << "The nodes that each agent traverses" << std::endl;                 \
  FOREACH(a, agentsR)                                                               \
  {                                                                                \
    std::cout << "agent " << a << " : " << std::endl;                              \
    FOREACH(s, stepsR)                                                              \
    {                                                                              \
      FOREACH(n, nodesR)                                                            \
      {                                                                            \
        if ((bool)(cplex.getValue(x[s][n][a]))) {                                  \
          std::cout << this->agents[0].getSolver()->getGraph()->getNode((int)n) << " ";                                               \
          break;                                                                   \
        }                                                                          \
      }                                                                            \
    }                                                                              \
    std::cout << std::endl;                                                        \
  }                                                                                \
  std::cout << "--------------------------------------------------" << std::endl;  \
  std::cout << "The edge occupation matrix" << std::endl;                          \
  std::vector<std::string> nodesString;                                            \
  for (const auto& node : this->agents[0].getSolver()->getGraph()->getNodes()) {   \
    nodesString.push_back(node.toString(1));                                       \
  }                                                                                \
  size_t largestNode = std::ceil(log10(std::max_element(nodesString.begin(),       \
    nodesString.end(), [](auto s1, auto s2){ return s1.size()< s2.size(); })->size())) + 1; \
  FOREACH(s, IloIntRange(env, 0, this->nSteps - 1))                                \
  {                                                                                \
    std::cout << "Step: " << s << std::endl;                                       \
    std::cout << std::setw(largestNode) << "";                                     \
    for (std::string str : nodesString) { std::cout << std::setw(largestNode) << str << " "; }   \
    std::cout << std::endl;                                                        \
    FOREACH(n1, nodesR)                                                             \
    {                                                                              \
      std::cout << std::setw(largestNode) << nodesString[n1];                                                                             \
      FOREACH(n2, nodesR)                                                           \
      {                                                                            \
        std::cout << std::setw(largestNode) << (bool)cplex.getValue(edges[n1][n2][s]) << " ";                \
      }                                                                            \
      std::cout << std::endl;                                                      \
    }                                                                              \
    std::cout << std::endl;                                                        \
  }                                                                                \
  std::cout << "--------------------------------------------------" << std::endl;  \
  std::cout << "Movement cost " << this->nSteps << std::endl;                                       \
  FOREACH(a, agentsR)                                                               \
  {                                                                                \
    std::cout << "a" << a << ": ";                                                 \
    FOREACH(s, IloIntRange(env, 0, this->nSteps-1))                                       \
    {                                                                              \
      std::cout << cplex.getValue(movement_cost[a][s]) << " ";                     \
    }                                                                              \
    std::cout << std::endl;                                                        \
  }                                                                                \
  std::cout << "--------------------------------------------------" << std::endl;  \
  std::cout << "Goal points" << std::endl;                                         \
  FOREACH(a, agentsR)                                                               \
  {                                                                                \
    std::cout << "Agent: " << a << std::endl;                                      \
    FOREACH(g, IloIntRange(env, 0, this->agents[a].getGoals().size()))                                    \
    {                                                                              \
      std::cout << tprintf("@ ", cplex.getValue(goal_points[a][g]));               \
    }                                                                              \
    std::cout << std::endl;                                                        \
  }                                                                                \
  std::cout << "--------------------------------------------------" << std::endl;
//  std::cout << out.str() << std::endl;

#else
#define DISPLAY_RESULTS()
#endif  // MAOF_DEBUG

struct TreeNode {
  size_t val;
  std::unique_ptr<TreeNode> left;
  std::unique_ptr<TreeNode> right;
  TreeNode(size_t x) : val(x), left(nullptr), right(nullptr) {}

  inline std::unique_ptr<TreeNode>& next (bool dec) {
    return dec ? this->left : this->right;
  }
};
void prettyPrintTree(const std::unique_ptr<TreeNode> & root, size_t depth = 0);
std::unique_ptr<TreeNode> constructBST(std::vector<size_t>& nums);

class CPSolver : public MAPFSolver
{
private:
  COST_FUNCTION cf;
  size_t nSteps;
  size_t nIterations;
  bool checkComputability;
  std::vector<size_t> steps;

  void _solve(std::vector<std::vector<GraphNode>>& paths);

public:
  CPSolver() : MAPFSolver() {}

  CPSolver(std::vector<Agent> agents, COST_FUNCTION _cf, size_t _nSteps = 0, size_t _nIterations = 10, bool _checkComputability = false, std::vector<size_t> _steps = {})
  : MAPFSolver(agents), cf(_cf), nSteps(_nSteps), nIterations(_nIterations), checkComputability(_checkComputability)
  {
    if (!_steps.empty()) {
      this->steps = _steps;
    }
    else if (this->nSteps>0 && this->nIterations>0){
      this->steps.resize(this->nIterations);
      std::iota(this->steps.begin(), this->steps.end(), this->nSteps);
    }
  }

  ~CPSolver() override {}

  std::vector<std::vector<GraphNode>> solve() override;
  std::vector<std::vector<GraphNode>> solveICR(const std::vector<size_t>& times) override;

  void addAgent(const Agent& a)
  {
    if((size_t)a.getID() != this->agents.size()) { throw  std::runtime_error("Cannot add agent to solver because the id is out of order"); }
    this->agents.push_back(a);
  }

  void removeAgent(size_t id)
  {
    auto el = std::find_if(this->agents.begin(), this->agents.end(), [id](Agent& a){ return a.getID() == id; });
    if (el != this->agents.end()){
      this->agents.erase(el);
      size_t index = std::distance(this->agents.begin(), el);
    }
  }

  void setCostFunction(COST_FUNCTION _cf) { this->cf = _cf; }
  [[nodiscard]] COST_FUNCTION getCostFunction() { return this->cf; }

  void setNIterations(size_t _nIterations) { this->nIterations = _nIterations; }
  [[nodiscard]] size_t getNIterations() { return this->nIterations; }

  void setNSteps(size_t _nSteps) { this->nSteps = _nSteps; }
  [[nodiscard]] size_t getNSteps() { return this->nSteps; }

  void setCheckComputability(bool _checkComputability) { this->checkComputability = _checkComputability; }
  [[nodiscard]] bool getCheckComputability() { return this->checkComputability; }

  void setSteps(std::vector<size_t> _steps) { this->steps = _steps; }
  [[nodiscard]] std::vector<size_t> getSteps() { return this->steps; }

  /** TODO this is an old description
   * @brief Function to solve the MAPF problem with constraint programming.
   * @param n_nodes The number of nodes in the problem
   * @param n_agents The number of agents in the problem
   * @param n_steps The number of steps the solution should take
   * @param _init_pos A vector containing the ids of the nodes from which the agents start
   * @param _end_pos A vector containing the ids of the nodes that the agents should reach
   * @param _goal_pos A vector of vectors containing the ids of the nodes of the goals for each agent
   * @param _connect An integer connectivity matrix.
   * @param costF The name of the cost function to be minimized. Options are "SIC" or "MKS".
   * @return A vector of vectors containing the ids of the nodes that make the paths that the agents
   * should follow to solve the problem
   */
  std::vector<std::vector<size_t>> solveCP();

  void postProcess(std::vector<std::vector<GraphNode>> & n) override
  {
    std::cout << "postProcess " << n.size() << std::endl;
  }

  [[nodiscard]] std::string out() const override
  {
    std::stringstream stream;
    stream << "Using CP" << std::endl;
    stream << "Number of agents: " << this->getAgents().size() << std::endl;
    stream << "Number of nodes: " << this->getAgents()[0].getSolver()->getGraph()->getNNodes() << std::endl;
    stream << tprintf("Nodes: @\n", this->getAgents()[0].getSolver()->getGraph()->getNodes());

    stream << "Agents:" << std::endl;
    for (const Agent & a : this->getAgents()) {
      stream << tprintf("- @ @->@ goals: @\n", a, a.getInitPos(), a.getEndPos(), a.getGoals());
    }

    stream << tprintf(
      "Number of steps: @\n"\
      "Iterations: @\n"\
      "Check computability: @\n"
      "Steps: @\n",
      this->nSteps, this->nIterations, this->checkComputability, this->steps);

//    stream << "Graph: \n";
//    stream << *(this->getAgents()[0].getSolver()->getGraph());
    return stream.str();
  }

  friend std::ostream & operator<<(std::ostream & stream, const CPSolver & sol)
  {
    stream << sol.out();
    return stream;
  }
};

#else  //CPLEX_FOUND

#ifndef CPLEX_NOT_FOUND_WARNING
#warning "CPLEX not found, do not call CPSolver"
#define CPLEX_NOT_FOUND_WARNING
#endif // CPLEX_NOT_FOUND_WARNING

class CPSolver : MAPFSolver
{
  CPSolver() { throw std::runtime_error("CP is not defined, please recompile code"); }
  CPSolver(std::vector<Agent> agents, COST_FUNCTION _cf) : MAPFSolver(agents)
  {
    throw std::runtime_error("CP is not defined, please recompile code");
  }
  ~CPSolver() override {}
  std::vector<std::vector<GraphNode>> solve() override;
  void postProcess(std::vector<std::vector<GraphNode>> & n) override;
  [[nodiscard]] std::string out() const override;
};

#endif  //CPLEX_FOUND
#endif  //MAOF_CP_HPP
