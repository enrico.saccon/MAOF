/**
 * @file CostFunction.hpp
 * @author Enrico Saccon (enrico.saccon@studenti.unitn.it)
 * @brief File that contains some cost functions that can be used for the MAPF problem. 
 */

#ifndef MAOF_COSTFUNCTION_HPP
#define MAOF_COSTFUNCTION_HPP

// Library includes
#include <Defines.hpp>
#include <Utils.hpp>

// System includes
#include <algorithm>
#include <vector>

enum class COST_FUNCTION { NONE, SIC, MKS };

/**
 * @brief Function to take in input a string and return the corresponding type of the cost function
 * @details The string is first converted to upper case
 * @param s The string containing the name for the cost function
 * @return The type of the cost function as `COST_FUNCTION`
 */
inline COST_FUNCTION chooseCFType(std::string s)
{
  s_toupper(s);
  if (s == "SIC"){
    return COST_FUNCTION::SIC;
  }
  else if (s == "MKS"){
    return COST_FUNCTION::MKS;
  }
  else{
    return COST_FUNCTION::NONE;
  }
}

/*!
 * SIC = Sum of Individual Costs
 * @param solution the proposed solution containing the paths
 * @return the sum of the paths length
 */
size_t CF_SIC(const std::vector<std::vector<Node>> & solution);

/*!
 * Function to compute the makespan of the solution
 * @param solution the proposed solution containing the paths
 * @return the length of the longest path
 */
size_t CF_MKS(const std::vector<std::vector<Node>> & solution);

[[nodiscard]] inline size_t costFunction(COST_FUNCTION type, const std::vector<std::vector<Node>> & solution)
{
  switch (type)
  {
    case COST_FUNCTION::SIC:
    {
      return CF_SIC(solution);
    }
    case COST_FUNCTION::MKS:
    {
      return CF_MKS(solution);
    }
    case COST_FUNCTION::NONE:
    default:
      return (size_t)-1;
  }
}
#endif  //MAOF_COSTFUNCTION_HPP
