/**
* @author Giovanni Lorenzini and Diego Planchenstainer
* @file NodeAgentLabel.hpp
 */
#ifndef MAOF_NODEAGENTLABEL_HPP
#define MAOF_NODEAGENTLABEL_HPP

#include <Hash.hpp>
#include <GraphNode.hpp>

class NodeAgentLabel
{
private:
  GraphNode node;
  size_t agent;
  size_t label;

public:
  NodeAgentLabel() {}

  NodeAgentLabel(GraphNode node, size_t agent, size_t label) : node(node), agent(agent), label(label)
  {
  }

  bool operator==(const NodeAgentLabel & other) const
  {
    return node == other.node && agent == other.agent && label == other.label;
  }

  friend std::hash<NodeAgentLabel>;
};

namespace std
{
template <>
struct hash<NodeAgentLabel>
{
  std::size_t operator()(const NodeAgentLabel & nal) const noexcept
  {
    size_t seed = 0;

    hash_combine(seed, nal.node.getId());
    hash_combine(seed, nal.agent);
    hash_combine(seed, nal.label);

    return seed;
  }
};
}  // namespace std

#endif  //MAOF_NODEAGENTLABEL_HPP
