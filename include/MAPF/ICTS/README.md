# ICTS

Implementation of the algorithm Increasing Cost Tree Search proposed by Sharon et al. [1].

The solver uses also Independent Decomposition (ID), which was taken from [2].

## Thanks 

Thanks to [Giovanni Lorenzini](mailto:giovanni.lorenzini@studenti.unitn.it) and 
[Diego Planchestainer](mailto:d.planchenstainer@studenti.unitn.it) for their contribution in writing this solver. 

## Briefly

The algorithm solves the MAPF problem by managing an Increase Cost Tree (ICT), whose nodes contain the lengths of the
paths of the different agents. A high-level search manages the tree, while a low-level search finds the paths of the 
agents with the corresponding length inside the ICT node considered. 

The low-level uses Multi-value Decision Diagrams (MDD) to find the possible paths for an agent.

The high-level checks the MDDs computed by the low-level search and if no conflicts are found, then the solution is 
returned. Otherwise, it will create $k$ new nodes each $i$-th node increasing the $i$-th agent's path length of 1.

The search continues until a solution without conflicts is found. 

## Usage

The advice is to use wither the `readJSON` function or the `runTest` one available in `Test.hpp`. The first will return
a pointer to a `MAPFSolver` (which can then be dynamically cast to a `ICTS`) while the second will run the whole solver
on its own.

If you want to use the solver in a different way, you can create the solver as follows:

```cpp
#include <MAPF/CBS/ICTS.hpp>
ICTSSolver solver (agents);
```

where `agents` is a vector of `Agent`. Then, you can use the `solve` function.


## References

[1] Sharon, G., Stern, R., Goldenberg, M., & Felner, A. (2013). The increasing cost tree search for optimal multi-agent pathfinding. Artificial intelligence, 195, 470-495, [https://doi.org/10.1016/j.artint.2012.11.006](https://doi.org/10.1016/j.artint.2012.11.006).

[2] Standley, T. (2010, July). Finding optimal solutions to cooperative pathfinding problems. In Proceedings of the AAAI Conference on Artificial Intelligence (Vol. 24, No. 1, pp. 173-178).