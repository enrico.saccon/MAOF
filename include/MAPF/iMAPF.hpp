/**
 * @author Enrico Saccon <enrico.saccon@unitn.it>
 * @file iMAPF.hpp
 * @brief Interface to include and choose the MAPF solver
 */

#ifndef MAOF_IMAPF_HPP
#define MAOF_IMAPF_HPP

#include <MAPF/MAPFSolver.hpp>
#include <MAPF/CBS/CBS.hpp>
#include <MAPF/CP/CP.hpp>
#include <MAPF/ICR/ICR.hpp>
#include <MAPF/ICTS/ICTS.hpp>
#include <MAPF/PP/PP.hpp>

/**
 * @brief Function to take a `MAPF_TYPE` in input and return a pointer to the corresponding MAPF solver
 * @param mapfSolver The `MAPF_TYPE` of the MAPF solver to use
 * @param agents A vector with the agents for the MAPF solver
 * @param costF The cost function to minimize
 * @param subSolver The `MAPF_TYPE` of the sub solver to be used in some cases, e.g. with ICR, default is NONE.
 * @return A pointer to the corresponding `MAPFSolver`
 */
MAPFSolver * chooseMAPFSolver (MAPF_TYPE mapfSolver, std::vector<Agent> & agents, COST_FUNCTION costF, MAPF_TYPE subSolver = MAPF_TYPE::NONE);

/**
 * @brief Function to take a string in input and return a pointer to the corresponding MAPF solver
 * @details The string is first converted to upper case
 * @param mapfSolver The string with the MAPF solver to use
 * @param agents A vector with the agents for the MAPF solver
 * @param costF The cost function to minimize
 * @param subSolver A string containing the sub solver to be used in some cases, e.g. with ICR, default is NONE.
 * @return A pointer to the corresponding `MAPFSolver`
 */
inline MAPFSolver * chooseMAPFSolver (std::string mapfSolver, std::vector<Agent> & agents, COST_FUNCTION costF, std::string subSolver = "NONE") {
  return chooseMAPFSolver(mapfType(mapfSolver), agents, costF, mapfType(subSolver));
}



#endif  //MAOF_IMAPF_HPP
