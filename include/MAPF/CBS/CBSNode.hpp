/**
 * @author Enrico Saccon <enrico.saccon@unitn.it>
 * @file CBSNode.hpp
 * @brief File that contains the model for a node of the constraint tree used in CBS.
 */
#ifndef CBS_CBSNODE_HH
#define CBS_CBSNODE_HH

// Library includes
#include <MAPF/Constraint.hpp>
#include <Agent.hpp>

// System includes
#include <limits>

/**
 * @brief Class to model a node of the constraint tree used by CBS
 */
class CBSNode
{
private:
  std::shared_ptr<CBSNode> parent;                 /// A `std::shared_ptr` to the parent of the node
  std::vector<Constraint> constraints;             /// The list of constraints associated with the node
  std::vector<std::vector<GraphNode> > solution;   /// The joint plan
  int cost;                                        /// The cost of the node
  std::vector<std::shared_ptr<CBSNode>> children;  /// The list of children of the node

public:
  /**
   * @brief Default constructor
   */
  CBSNode()
  : parent(nullptr),
    constraints({}),
    solution({}),
    cost(-1){};

  /**
   * @brief Constructor that takes as input the required information to create a node
   * @param _parent A `std::shared_ptr` to the parent of the node
   * @param _constraints The list of constraints associated with the node
   * @param _solution The joint plan
   * @param _cost The cost of the node
   */
  CBSNode(
    std::shared_ptr<CBSNode>& _parent, const std::vector<Constraint> & _constraints,
    std::vector<std::vector<GraphNode> > _solution, int _cost = std::numeric_limits<int>::max())
  : parent(_parent), constraints(_constraints), solution(_solution), cost(_cost)
  {}

/**************************************************************************************************/
/*                                          Getters                                               */
/**************************************************************************************************/

  /**
   * @brief Get the parent of the node
   * @return A `std::shared_ptr` to the parent of the node
   */
  std::shared_ptr<CBSNode> getParent() const { return this->parent; }
  /**
   * @brief Get the list of constraints associated with the node
   * @return A `std::vector` of `Constraint` objects
   */
  std::vector<Constraint> getConstraints() const { return this->constraints; }
  /**
   * @brief Get the joint plan associated with the node
   * @return A `std::vector` of `Path`s
   */
  std::vector<std::vector<GraphNode> > getSolution() const { return this->solution; }
  /**
   * @brief Get the cost of the node returned by the cost function applied to the joint plan
   * @return The cost of the node
   */
  int getCost() const { return this->cost; }
  /**
   * @brief Get the list of children of the node
   * @return A `std::vector` of `std::shared_ptr` to the children of the node
   */
  std::vector<std::shared_ptr<CBSNode>> getChildren() const { return this->children; }

/**************************************************************************************************/
/*                                          Setters                                               */
/**************************************************************************************************/

  /**
   * @brief Set the parent of the node
   * @param _parent A `std::shared_ptr` to the parent of the node
   */
  void setParent(std::shared_ptr<CBSNode>& _parent) { this->parent = std::move(_parent); }
  /**
   * @brief Set the list of constraints associated with the node
   * @param _constraints A `std::vector` of `Constraint` objects
   */
  void setConstraints(std::vector<Constraint> _constraints) { this->constraints = _constraints; }
  /**
   * @brief Set the joint plan associated with the node
   * @param _solution A `std::vector` of `Path`s
   */
  void setSolution(std::vector<std::vector<GraphNode> > _solution) { this->solution = _solution; }
  /**
   * @brief Set a single path inside the joint plan
   * @param _solution The new single plan to be set inside the joint plan
   * @param pos The position in which it should be inserted
   */
  void setSolution(std::vector<GraphNode> _solution, size_t pos) { this->solution[pos] = _solution; }
  /**
   * @brief Set the cost of the node returned by the cost function applied to the joint plan
   * @param _cost The cost of the node
   */
  void setCost(int _cost) { this->cost = _cost; }

/**************************************************************************************************/
/*                                           Others                                               */
/**************************************************************************************************/
  /**
   * @brief Add a child to the node
   * @param _child A `std::shared_ptr` to the child to be added
   */
  void addChild(std::shared_ptr<CBSNode> _child) { this->children.push_back(_child); }
  /**
   * @brief Add a constraint to the list of constraints associated with the node
   * @param _constraint A `Constraint` object
   */
  void addConstraint(Constraint _constraint) { this->constraints.push_back(_constraint); }

/**************************************************************************************************/
/*                                 Operator overloading                                           */
/**************************************************************************************************/

  /**
   * @brief Overload of the `==` operator
   * @param node The node to be compared with
   * @return `true` if the nodes are equal, `false` otherwise
   */
  bool operator== (const CBSNode& node) const {
    return  this->getParent() == node.getParent() && this->getSolution() == node.getSolution() &&
            this->getCost() == node.getCost() && this->getConstraints() == node.getConstraints();
  }

  /**
   * @brief Overload of the `<` operator
   * @param node The node to be compared with
   * @return `true` if the cost of the node is less than the cost of the node passed as argument, `
   * false` otherwise
   */
  bool operator< (const CBSNode& node) const {
    return this->getCost() < node.getCost();
  }

  /**
   * @brief Overload of the `<<` operator
   * @param stream The stream to which the node should be printed
   * @param node The node to be printed
   * @return The stream to which the node has been printed
   */
  friend std::ostream & operator<<(std::ostream & stream, const CBSNode & node)
  {
    stream << tprintf("Cost: @ #solutions: @\n", node.getCost(), node.getSolution().size());
    stream << node.getSolution() << std::endl;
    stream << tprintf("constraints @ @", node.getConstraints().size(), node.getConstraints());
    return stream;
  }
};

/**
 * @brief Function to correctly order the nodes in the priority queue
 * @param lhs The shared pointer to the left hand side `CBSNode` of the comparison
 * @param rhs The shared pointer to the right hand side `CBSNode` of the comparison
 * @return True if the cost of the `CBSNode` pointed by `lhs` is less than the cost of the `CBSNode` pointed by `rhs`
 */
inline bool operator< (const std::shared_ptr<CBSNode>& lhs, const std::shared_ptr<CBSNode>& rhs){
  return lhs->getCost() < rhs->getCost();
}

#endif  //CBS_CBSNODE_HH
