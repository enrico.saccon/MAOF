/**
 * @author Enrico Saccon
 * @file CBS.hpp
 * @brief Header file containing the class for CBS.
 */

#ifndef MAOF_CBS_HPP
#define MAOF_CBS_HPP

// Library includes
#include <SAPF/DistanceFunction.hpp>
#include <MAPF/CBS/CBSNode.hpp>
#include <MAPF/iMAPF.hpp>

// System includes
#include <iomanip>

/**
 * @brief This class implements the CBS algorithm.
 */
class CBSSolver : public MAPFSolver
{
private:
  COST_FUNCTION cf;  //< Cost function used by the solver

public:
  /**
   * @brief Default constructor.
   */
  CBSSolver() : MAPFSolver(), cf(COST_FUNCTION::NONE) {}

  /**
   * @brief Constructor.
   * @param agents Vector of agents to be solved.
   * @param _cf Cost function to be used.
   */
  CBSSolver(std::vector<Agent> agents, COST_FUNCTION _cf) : MAPFSolver(agents), cf(_cf) {}

/**************************************************************************************************/
/*                                          Solvers                                               */
/**************************************************************************************************/

  /**
   * This function is the equivalent to the high level search of CBS
   * @return A `std::vector` of `Path` containing the solution, if any were found, otherwise an empty one.
   */
  std::vector<std::vector<GraphNode>> solve() override;
  /**
   * This function is the equivalent to the high level search of CBS, but modified to be used in the ICR algorithm.
   * @details It adds as many initial goals (each equal to the initial position) as the time at which the agent starts.
   * @param times The time at which the agents start.
   * @return A `std::vector` of `Path` containing the solution, if any were found, otherwise an empty one.
   */
  std::vector<std::vector<GraphNode>> solveICR(const std::vector<size_t> & times) override;

  /**
   * @brief Function that computes the first solution for all the agents.
   * @details This function solves each individual SAPF problem.
   * @param node The node for which the first solution should be computed.
   * @return A `std::vector` of `Path` containing the solution, if any were found, otherwise an empty one.
   */
  std::vector<std::vector<GraphNode>> lowLevelAgents(std::shared_ptr<CBSNode> & node);
  /**
   * @brief Function that updates a solution for an agent in the conflict depending on the added
   * constraint.
   * @param node The node for which the solution should be updated.
   * @param c The constraint that was added.
   * @return A `std::vector` of `Path` containing the solution, if any were found, otherwise an empty one.
   */
  std::vector<std::vector<GraphNode>> lowLevelAgentsSingle(
    std::shared_ptr<CBSNode> & node, const Constraint & c);

/**************************************************************************************************/
/*                                    Utility functions                                           */
/**************************************************************************************************/

  /**
   * @brief Function to be called to post-process the solution.
   * @details This function checks if for any reason, too many nodes were added, and removes them.
   * @param paths The solution to be processed.
   */
  void postProcess(std::vector<std::vector<GraphNode>> & paths) override;

  /**
   * @brief Get the cost function used by the solver.
   * @return The cost function used by the solver.
   */
  COST_FUNCTION getCostFunction() const { return this->cf; }
  /**
   * @brief Set the cost function used by the solver.
   * @param _cf The cost function to be used.
   */
  void setCostFunction(COST_FUNCTION _cf) { this->cf = _cf; }
  /**
   * @brief Function that calls the cost function on the solution.
   * @param sol The solution for which the cost should be computed
   * @return The cost of the solution.
   */
  inline size_t cost(const std::vector<std::vector<GraphNode>> & sol) const
  {
    return costFunction(this->cf, sol);
  }

  /**
   * @brief Function that returns a string representation of the solver.
   * @return A string representation of the solver.
   */
  [[nodiscard]] std::string out() const override
  {
    std::stringstream stream;
    stream << "Using CBS" << std::endl;
    stream << "Number of agents: " << this->getAgents().size() << std::endl;
    stream << "Number of nodes: " << this->getAgents()[0].getSolver()->getGraph()->getNNodes() << std::endl;
    stream << "Nodes: [";
    SAPFSolver * sapfSolver = this->getAgents()[0].getSolver();
    for (auto n : sapfSolver->getGraph()->getNodes()) {
      stream << n << " ";
    }
    stream << "]\n";

    stream << "Agents:" << std::endl;
    for (const Agent & a : this->getAgents()) {
      stream << tprintf("- @ @->@ goals: [", a, a.getInitPos(), a.getEndPos());
      for (auto n : a.getGoals()) {
        stream << n << " ";
      }
      stream << "]\n";
    }
//    stream << "Graph: \n";
//    stream << *(this->getAgents()[0].getSolver()->getGraph());

    return stream.str();
  }

  /**
   * @brief Overload of the << operator for the class.
   * @param stream The stream to which the solver should be printed.
   * @param sol The solver to be printed.
   * @return The stream to which the solver was printed.
   */
  friend std::ostream & operator<<(std::ostream & stream, const CBSSolver & sol)
  {
    stream << sol.out();
    return stream;
  }
};

#endif  //MAOF_CBS_HPP
