/** 
 * @author Enrico Saccon <enrico.saccon@unitn.it>.
 * @file ConMatrix.hpp
 * @brief 
 */

#ifndef MAOF_CONMATRIX_HPP
#define MAOF_CONMATRIX_HPP

// Library includes
#include <Graph.hpp>
#include <MAPF/Constraint.hpp>

//System includes

/**
 * @brief Class that models the graph using a connectivity matrix
 */
class ConMatrix : public Graph
{
private:
  std::vector<std::vector<Connection>> matrix;
  /**
   * @brief Function to convert a non-unitary matrix to a matrix with edges of cost 1.
   * @param [in,out] m The matrix to be changed
   * @return The matrix with unitary costs
   */
  std::vector<std::vector<int>>& toUnit(std::vector<std::vector<int>>& m)
  {
    size_t max_val = std::max_element(this->nodes.begin(), this->nodes.end(),
                                      [](const GraphNode& n1, const GraphNode& n2){
                                        return n2.getV() > n1.getV(); })->getV();
    for(size_t i = 0; i<m.size(); i++){
      for(size_t j = 0; j<m[i].size(); j++){
        if (m[i][j]>1) {
          // Add new node
          GraphNode newNode = GraphNode(this->nodes.size(), ++max_val);
          this->nodes.push_back(newNode);

          // Add row
          m.push_back(std::vector<int>(m[i].size(), 0));
          m.back()[j] = m[i][j] - 1;
          // Add column
          for (size_t h = 0; h<m.size(); h++){
            m[h].push_back( h==i ? 1 : 0);
          }
          // Reset value
          m[i][j] = 0;
        }
      }
    }
    return m;
  }

public:
  ConMatrix(std::vector<GraphNode> _nodes, std::vector<std::vector<Connection>> _matrix, Graph::TYPE _type = Graph::TYPE::CON_MATRIX)
  : Graph(_nodes, _type), matrix(std::move(_matrix))
  {
  }

  ConMatrix(std::vector<GraphNode> _nodes, std::vector<std::vector<int>> & _matrix, Graph::TYPE _type = Graph::TYPE::CON_MATRIX)
  :
    Graph(_nodes, _type)
  {
//    this->matrix = convertConnect(this->toUnit(_matrix));
    this->matrix = convertConnect(_matrix);
  }

//************************************************************************************************//
//                                          GETTERS                                               //
//************************************************************************************************//

  /**
   * @brief Function that returns the matrix used below as a value.
   * @return A vector of vector of Connection.
   */
  [[nodiscard]] std::vector<std::vector<Connection>> getMatrix() const override { return this->matrix; }

  /**
   * @brief Function to return the reference to a `Connection` object so that it can be directly
   * accessed.
   * @details This is a REFERENCE, hence if it is modified, also the value inside the matrix will be
   * modified. Notice that it cannot be assigned to a variable:
   * `Connection c = con.getConnection(0,1); c.setType();`
   * won't affect the value inside the matrix, but:
   * `con.getConnection(0,1).setType()`
   * will.
   * @param a The starting node of the connection.
   * @param b The ending node of the connection.
   * @return A reference to the `Connection` object.
   */
  [[nodiscard]] Connection& getConnection(size_t a, size_t b) {
    return this->matrix[a][b];
  }

  /**
   * @brief It returns the cost of the connection between nodes `a`->`b` at time `t` and for the agent with
   * id `a_id`.
   * @param a The id of the starting node.
   * @param b The id of the ending node.
   * @param t The time at which the connection should be used.
   * @param a_id The id of the agent that wants to use the connection.
   * @return An integer value representing the cost of using the connection. If it is 0, then the
   * connection cannot be used.
   */
  [[nodiscard]] int getCost(size_t a, size_t b, size_t t, size_t a_id) const override
  {
    std::pair<size_t, size_t> p = this->matrix[a][b].costId(t);
    if (p.second == a_id){
      return 0;
    }
    else { // (p.second == 0){
      return p.first;
    }
  }

  /**
   * @brief It returns the cost of the connection between nodes `a`->`b` at time `t` independently
   * from the agent requiring the usage.
   * @param a The id of the starting node.
   * @param b The id of the ending node.
   * @param t The time at which the connection should be used.
   * @return An integer value representing the cost of using the connection. If it is 0, then the
   * connection cannot be used.
   */
  [[nodiscard]] int getCost(size_t a, size_t b, size_t t = INF<size_t>()) const override
  {
    return this->matrix[a][b].cost(t);
  }

  /**
   * @brief Function to return the neighbors of a node which are reachable at a given time.
   * @param n_id The id of the node for which the neighbors should be retrieved.
   * @param t The time at which the connections to the neighbors should be checked.
   * @return A vector containing the nodes (as objects) that are neighbors of the argument node.
   */
  [[nodiscard]] std::vector<GraphNode> getNeighbors(size_t n_id, size_t t = INF<size_t>()) const override
  {
    std::vector<GraphNode> neighs = {};
    for (GraphNode n : this->getNodes()){
      if (matrix[n_id][n.getId()].cost(t) > 0){
        neighs.push_back(n);
      }
    }
    return neighs;
  }

  /**
   * @brief Function to return the neighbors of a node which are reachable at a given time.
   * @param n_id The id of the node for which the neighbors should be retrieved.
   * @param t The time at which the connections to the neighbors should be checked.
   * @return A vector containing the ids of the nodes that are neighbors of the argument node.
   */
  [[nodiscard]] std::vector<size_t> getNeighborsIds(size_t n_id, size_t t = INF<size_t>()) const override
  {
    std::vector<size_t> ids = {};
    for (size_t i = 0; i < matrix[n_id].size(); ++i) {
      if (matrix[n_id][i].cost(t) > 0) {
        ids.push_back(i);
      }
    }

    return ids;
  }

  /**
   * @brief Function that returns the number of reachable neighbors of a given node at a given
   * moment.
   * @param a The id of the node for which the number of available neighbors should me retrieved.
   * @param t The time at which the connections should be available.
   * @return An unsigned integer stating the number of neighbors.
   */
  [[nodiscard]] size_t getNNeighbors(size_t a, size_t t = INF<size_t>()) const override
  {
    size_t counter = 0;
    for (size_t i = 0; i < matrix[a].size(); ++i) {
      if (matrix[a][i].cost(t) > 0 && a != i) {
        counter ++;
      }
    }

    return counter;
  }

//************************************************************************************************//
//                                          SETTERS                                               //
//************************************************************************************************//

  /**
   * @brief Add a time to the connection. Refer to Connection.addTime(t, id) for more details.
   * @param a The node from which the connection goes.
   * @param b The node to which the connection goes.
   * @param t The time to be added in which the connection is either not available or available.
   */
  void addTime(size_t a, size_t b, size_t t) override
  {
    this->matrix[a][b].addTime(t);
  }

  /**
   * @brief Add a time to the connection. Refer to Connection.addTime(t, id) for more details.
   * @param a The node from which the connection goes.
   * @param b The node to which the connection goes.
   * @param t The time to be added in which the connection is either not available or available.
   * @param a_id The id of the agent that can or cannot use the connection.
   */
  void addTime(size_t a, size_t b, size_t t, size_t a_id)
  {
    this->matrix[a][b].addTime(t, a_id);
  }

//************************************************************************************************//
//                                         FUNCTIONS                                              //
//************************************************************************************************//

  // Comments in ConMatrix.cpp
  void addConstraints(
    std::vector<Constraint> constraints,
    std::vector<GraphNode> * placeholders = nullptr) override;

  /**
   * @brief Function that checks if the two matrixes underneath are the same
   * @details Notice that the argument is a reference to a `Graph`, so any derived class of `Graph`,
   * which can be casted to `ConMatrix` can be used.
   * @param g The graph to compare to.
   * @return True if the two matrixes are the same, false otherwise.
   */
  [[nodiscard]] bool equal(const Graph& g) const override
  {
    return this->getMatrix() == dynamic_cast<const ConMatrix*>(&g)->getMatrix();
  }

  /**
   * @brief Function that stores information regarding the graph in a string.
   * @param verbose Its value is passed to the `Connection`, if true also times and ids will be
   * printed, otherwise only if the connection is available or not.
   * @return A string containing the matrix.
   */
  [[nodiscard]] std::string toString(bool verbose = false) const override
  {
    std::stringstream stream;
    int max_val = 0;
    for (auto n : this->nodes) { max_val = max_val > n.getV() ? max_val : n.getV(); }
    size_t largest = (size_t)(log10(std::abs(max_val)))+1;

    // Print spaces for padding first row of indexes:
    for (size_t i=0; i<largest+1; i++){
      stream << " ";
    }

    // Print indexes for columns:
    for (size_t i = 0; i < this->getMatrix().size(); i++) {
      stream << std::setw(largest) << this->nodes[i].getV() << " ";
    }
    stream << std::endl;

    // Print the various rows adding the index at the beginning
    for (size_t i=0; i<this->matrix.size(); i++){
      stream << std::setw(largest) << this->nodes[i].getV() << " ";
      for (auto cell : this->matrix[i]){
        stream << std::setw(largest) << cell.toString(verbose) << " ";
      }
      stream << std::endl;
    }

    return stream.str();
  }

  /**
   * @brief Function to cast the `ConMatix` to a matrix of integers.
   * @details It sets 1 if the connection is of type `ONE` or `LIMIT_ONCE`, 0 otherwise.
   * @return A matrix of integers representing the matrix.
   */
  explicit operator std::vector<std::vector<int>>() const override
  {
    std::vector<std::vector<int>> ret (this->matrix.size());

    for (size_t i=0; i<this->matrix.size(); i++)
    {
      ret[i].resize(this->matrix[i].size());
      for (size_t j=0; j<this->matrix[i].size(); j++)
      {
        switch(this->matrix[i][j].getType())
        {
          case Connection::TYPE::ONE:
          case Connection::TYPE::LIMIT_ONCE:
          {
            ret[i][j] = 1;
            break;
          }
          case Connection::TYPE::ZERO:
          case Connection::TYPE::LIMIT_ALWAYS:
          default:
          {
            ret[i][j] = 0;
          }
        }
      }
    }
    return ret;
  }

};

#endif  //MAOF_CONMATRIX_HPP
