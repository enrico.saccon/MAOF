/**
 * @author Enrico Saccon (enrico.saccon@studenti.unitn.it)
 * @file Defines.hpp
 * @brief File that can be used to define types.
 */
#ifndef MAOF_DEFINES_HH
#define MAOF_DEFINES_HH

#include <GraphNode.hpp>

typedef GraphNode Node;
typedef std::pair<GraphNode, GraphNode*> Placeholder;

#endif  //MAOF_DEFINES_HH
