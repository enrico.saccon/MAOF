/**
* @author Diego Planchenstainer, Giovanni Lorenzini and Enrico Saccon
* @file AStar.hpp
* @brief File that contains functions to solve the SAPF problem using A*.
*/

#ifndef MAOF_ASTAR_HPP
#define MAOF_ASTAR_HPP

// Library includes
#include <Connection.hpp>
#include <GraphNode.hpp>
#include <MAPF/Constraint.hpp>
#include <SAPF/SAPFSolver.hpp>
#include <Utils.hpp>
#include <SAPF/DistanceFunction.hpp>
#include <PriorityQueue.hpp>

// System includes
#include <unordered_map>
#include <algorithm>
#include <iostream>
#include <limits>
#include <vector>
#include <set>

class AStarSolver : public SAPFSolver
{
private:
 size_t depTime;
 std::vector<GraphNode> reconstructPath(std::vector<size_t> cameFrom, GraphNode current);

public:
 AStarSolver(
   std::shared_ptr<Graph>& _connectivity,
   double (*_heuristic)(GraphNode, GraphNode) = defaultHeur,
   size_t _depTime = 0)
 : SAPFSolver(_connectivity, _heuristic), depTime(_depTime)
 {
 }

 SAPFSolver * copy() override { return new AStarSolver(*this); }
 std::unique_ptr<SAPFSolver *> deepCopy() override {
   return std::make_unique<SAPFSolver*>(new AStarSolver(this->connect, this->heuristic, this->depTime));
 }

 std::vector<GraphNode> solve(
   GraphNode initPos, GraphNode endPos, std::vector<GraphNode> goals, size_t depTime = 0, int a_id = -1) override;

 std::vector<GraphNode> solveCBS(
   GraphNode initPos, GraphNode endPos, std::vector<GraphNode> goals,
   std::vector<Constraint> & constraints, size_t depTime = 0, int a_id = -1,
   std::vector<std::vector<GraphNode>> paths = {}) override;
};

#endif  //MAOF_ASTAR_HPP
