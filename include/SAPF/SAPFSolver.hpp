/**
 * @author Enrico Saccon (enrico.saccon@unitn.it)
 * @file SAPFSolver.hpp
 * @brief Contains the abstract class SAPFSolver which should be inherited to create new solvers for
 * the SAPF problem.
 */

#ifndef MAOF_SAPFSOLVER_HPP
#define MAOF_SAPFSOLVER_HPP

// Library includes
#include <Graph.hpp>
#include <Connection.hpp>
#include <SAPF/DistanceFunction.hpp> // TODO remove

// System includes
#include <memory>

class Constraint;  // Forward declaration

/*!
 * @brief SAPFSolver class used to call one of the SAPF solvers available.
 * @details Notice that the connectivity matrix is stored as a Connection matrix. A constructor
 * using an integer connectivity matrix is available, but *does not* handle constraints. If you need
 * to create a connectivity matrix with constraints, first create the object using the correct
 * `convertConnect`, see Connection.hpp, then pass the objet to this constructor.
 */
class SAPFSolver
{
protected:
  std::shared_ptr<Graph> connect;
  double (*heuristic)(GraphNode s, GraphNode f);

public:
  SAPFSolver(std::shared_ptr<Graph> _connect, double (*_heuristic)(GraphNode, GraphNode))
  : connect(_connect), heuristic(_heuristic)
  {
  }

  ~SAPFSolver();

  [[nodiscard]] Graph* getGraph() { return this->connect.get(); }
  [[nodiscard]] std::shared_ptr<Graph> getShrGraph() { return this->connect; }
  void setShrGraph(std::shared_ptr<Graph> _graph) { this->connect.swap(_graph); }
  void setGraph(Graph* _graph) { this->connect = std::shared_ptr<Graph>(_graph); }

  template<class T>
  void setConnect(const std::vector<std::vector<Connection>> & _connect) { this->connect = std::make_shared<Graph*>(new T(_connect)); }
  template<class T>
  void setConnect(const std::vector<std::vector<int>> & _connect) { this->connect = std::make_shared<Graph*>(new T(_connect)); }
  template<class T>
  void setConnect(T* _connect) { this->connect = std::make_shared<Graph*>(_connect); }

  // TODO should add a copy constructor to all derived classes
  SAPFSolver(const SAPFSolver& S) : connect(S.connect.get()), heuristic(S.heuristic) {}

  virtual SAPFSolver * copy() = 0;
  virtual std::unique_ptr<SAPFSolver *> deepCopy() = 0;

  virtual std::vector<GraphNode> solve(
    GraphNode initPos, GraphNode endPos, std::vector<GraphNode> goals, size_t depTime = 0, int a_id = -1) = 0;
  virtual std::vector<GraphNode> solveCBS(
    GraphNode initPos, GraphNode endPos, std::vector<GraphNode> goals,
    std::vector<Constraint> & constraints, size_t depTime = 0, int a_id = -1, std::vector<std::vector<GraphNode>> paths = {}) = 0;
};

#endif  //MAOF_SAPFSOLVER_HPP
