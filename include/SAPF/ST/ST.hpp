/**
 * @author Enrico Saccon (enrico.saccon@studenti.unitn.it)
 * @file ST.hpp
 * @brief Contains the solver that uses spanning trees.
 */

#ifndef MAOF_ST_HPP
#define MAOF_ST_HPP

// Library includes
#include <GraphNode.hpp>
#include <MAPF/Constraint.hpp>
#include <Utils.hpp>

// System includes
#include <algorithm>
#include <iostream>

std::vector<std::vector<GraphNode>> spanningTree(
  std::vector<GraphNode> & nodes, GraphNode initPos, GraphNode endPos,
  const Graph * connect, size_t initDist = 0);

std::vector<std::vector<GraphNode>> spanningTree(
  std::vector<GraphNode> & nodes, GraphNode initPos, GraphNode endPos,
  std::vector<GraphNode> & goalPos, const Graph * connect,
  size_t initDist = 0);

std::vector<GraphNode> spanningTree(
  std::vector<GraphNode> nodes, GraphNode initPos, GraphNode endPos,
  std::vector<GraphNode> & goalPos, const Graph * connect,
  std::vector<Constraint> & constraints);

class STSolver : public SAPFSolver
{
private:
  size_t depTime;

public:
  STSolver(std::shared_ptr<Graph> & _connectivity,
           double (*_heuristic)(GraphNode, GraphNode) = defaultHeur, size_t _depTime = 0)
  : SAPFSolver(_connectivity, _heuristic), depTime(_depTime)
  {
  }

  // TODO should add a copy constructor

  SAPFSolver * copy() override { return new STSolver(*this); }
  std::unique_ptr<SAPFSolver *> deepCopy() override {
    SAPFSolver * ret = new STSolver(this->connect, this->heuristic, this->depTime);
    return std::make_unique<SAPFSolver*>(ret);
  }

  std::vector<GraphNode> solve (
    GraphNode initPos, GraphNode endPos, std::vector<GraphNode> goals, size_t depTime = 0, int a_id = -1) override
  {
    std::vector<GraphNode> nodes = this->getGraph()->getNodes();
    std::vector<std::vector<GraphNode>> sols =
      spanningTree(nodes, initPos, endPos, goals, this->connect.get(), this->depTime);

    if (!sols.empty()) {
      return *std::min_element(sols.begin(), sols.end(), [](const auto & a, const auto & b) {
        return a.size() < b.size();
      });
    }
    else { return {}; }
  };

  std::vector<GraphNode> solveCBS (
    GraphNode initPos, GraphNode endPos, std::vector<GraphNode> goals,
    std::vector<Constraint> & constraints, size_t depTime = 0, int a_id = -1,
    std::vector<std::vector<GraphNode>> paths = {}) override
  {
    std::vector<GraphNode> nodes = this->getGraph()->getNodes();
    return spanningTree(nodes, initPos, endPos, goals, this->connect.get(), constraints);
  };
};

#endif  //MAOF_ST_HPP
