/** 
 * @author Enrico Saccon <enrico.saccon@unitn.it>.
 * @file Tree.hpp
 * @brief It's the year 2022 and C++ has no tree structure, not even in STL
 */

#ifndef MAOF_TREE_HPP
#define MAOF_TREE_HPP

#include <iostream>
#include <vector>
#include <algorithm>

class TreeNode {
private:
  std::shared_ptr<TreeNode> parent;
  std::vector<std::shared_ptr<TreeNode>> children;
  void * data;

public:
  TreeNode() : parent(nullptr), children({}) {}

  TreeNode(TreeNode * _parent, std::vector<std::shared_ptr<TreeNode>> _children = {})
  : parent(std::make_shared<TreeNode>(_parent[0])), children(_children) {}

  TreeNode(std::shared_ptr<TreeNode> _parent, std::vector<std::shared_ptr<TreeNode>> _children = {})
  : parent(_parent), children(_children) {}

  bool equal(const TreeNode& node)
  {
    return this->parent == node.parent && this->children == node.getChildren();
  }

  bool operator== (const TreeNode& node) { return this->equal(node); }
  bool operator!= (const TreeNode& node) { return !this->equal(node); }

  [[nodiscard]] std::shared_ptr<TreeNode> getParent () const { return this->parent; }
  [[nodiscard]] std::vector<std::shared_ptr<TreeNode>> getChildren () const { return this->children; }

  void setParent(TreeNode * _parent) { this->parent = std::make_shared<TreeNode>(_parent[0]); }
  void setParent(std::shared_ptr<TreeNode> _parent) { this->parent = _parent; }
  void setChildren(std::vector<std::shared_ptr<TreeNode>>& _children)
  {
    this->children = std::move(_children);
  }

  bool addChild (std::shared_ptr<TreeNode> _child)
  {
    if (std::find_if(
          this->getChildren().begin(),
          this->getChildren().end(),
          [_child](const std::shared_ptr<TreeNode>& node)->bool {
            return _child.get() == node.get(); }
       ) != this->getChildren().end())
    {
      this->children.push_back(_child);
      return true;
    }
    return false;
  }

  bool removeChild (std::shared_ptr<TreeNode> _child)
  {
    auto it = std::find_if(
          this->getChildren().begin(),
          this->getChildren().end(),
          [_child](const std::shared_ptr<TreeNode>& node)->bool {
            return _child.get() == node.get(); });
    if (it != this->getChildren().end())
    {
      this->children.erase(it);
      return true;
    }
    return false;
  }

  bool removeChild (size_t _childPos)
  {
    if (_childPos < this->getChildren().size())
    {
      auto it = this->children.begin() + _childPos;
      this->children.erase(it);
      return true;
    }
    return false;
  }


};

#endif  //MAOF_TREE_HPP
