/**
 * @author Enrico Saccon (enrico.saccon@studenti.unitn.it)
 * @file TDSP.hpp
 * @brief Contains the solver that uses time dependant shortest paths.
 */

#ifndef MAOF_TDSP_HPP
#define MAOF_TDSP_HPP

// Library includes
#include <Connection.hpp>
#include <GraphNode.hpp>
#include <MAPF/Constraint.hpp>
#include <SAPF/SAPFSolver.hpp>
#include <Utils.hpp>

// System includes
#include <algorithm>
#include <iostream>
#include <limits>
#include <vector>

class TDSPSolver : public SAPFSolver
{
private:
  size_t depTime;

public:
  TDSPSolver(
    std::shared_ptr<Graph>& _connectivity,
    double (*_heuristic)(GraphNode, GraphNode) = defaultHeur,
    size_t _depTime = 0)
  : SAPFSolver(_connectivity, _heuristic), depTime(_depTime)
  {
  }

  SAPFSolver * copy() override { return new TDSPSolver(*this); }
  std::unique_ptr<SAPFSolver *> deepCopy() override {
    SAPFSolver * ret = new TDSPSolver(this->connect, this->heuristic, this->depTime);
    return std::make_unique<SAPFSolver*>(ret);
  }

  std::vector<GraphNode> solve(
    GraphNode initPos, GraphNode endPos, std::vector<GraphNode> goals, size_t depTime = 0, int a_id = -1) override;
  std::vector<GraphNode> solveCBS(
    GraphNode initPos, GraphNode endPos, std::vector<GraphNode> goals,
    std::vector<Constraint> & constraints, size_t depTime = 0, int a_id = -1, std::vector<std::vector<GraphNode>> paths = {}) override;
};

#endif  //MAOF_TDSP_HPP
