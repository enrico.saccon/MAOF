/** 
 * @author Enrico Saccon <enrico.saccon@unitn.it>.
 * @file A_TDPS.hpp
 * @brief 
 */

#ifndef MAOF_A_TDSP_HPP
#define MAOF_A_TDSP_HPP

#include <SAPF/SAPFSolver.hpp>

class A_TDSPSolver : public SAPFSolver {
public:
  A_TDSPSolver(std::shared_ptr<Graph> _connect, double (*_heuristic)(GraphNode, GraphNode))
  : SAPFSolver(_connect, _heuristic) {}

  SAPFSolver * copy() override { return new A_TDSPSolver(*this); }
  std::unique_ptr<SAPFSolver *> deepCopy() override {
    SAPFSolver * ret = new A_TDSPSolver(this->connect, this->heuristic);
    return std::make_unique<SAPFSolver*>(ret);
  }

  std::vector<GraphNode> solve(
    GraphNode initPos, GraphNode endPos, std::vector<GraphNode> goals, size_t depTime = 0, int aId = -1) override;
  std::vector<GraphNode> solveCBS(
    GraphNode initPos, GraphNode endPos, std::vector<GraphNode> goals,
    std::vector<Constraint> & constraints, size_t depTime = 0, int aId = -1, std::vector<std::vector<GraphNode>> paths = {}) override;
};

#endif  //MAOF_A_TDSP_HPP
