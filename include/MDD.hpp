/**
* @author Giovanni Lorenzini and Diego Planchenstainer
* @file MDD.hpp
* @brief File that contains the model for a node of the Multi-valued Decision Diagram.
*/
#ifndef MAOF_MDD_HPP
#define MAOF_MDD_HPP

#include <iostream>
#include <vector>
#include <map>
#include <set>

#include <fstream>
#include <string>

#include <Utils.hpp>
#include <GraphNode.hpp>

typedef GraphNode T;

class MDDNode
{
private:
 size_t level;
 T node;
 size_t label;

public:
 MDDNode(size_t level, T node, size_t label) : level(level), node(node), label(label) {}

 T getNode() const { return node; }
 size_t getLevel() const { return level; }
 size_t getLabel() const { return label; }

 void incrementLabel() { label++; }

 bool operator==(const MDDNode & other) const;
 bool operator<(const MDDNode & other) const;
};

/**
* @brief Class that models a Multi-valued Decision Diagram, with value, level and children of the current node.
*/
class MDD
{
private:
 // Using a set so that we have unique nodes
 typedef std::set<MDDNode> Parents;
 typedef std::set<MDDNode> Children;

 std::map<MDDNode, std::pair<Parents, Children>> mdd;

public:
  bool empty() { return mdd.empty(); }

 /**
  * @brief Add a node to the MDD.
  * @param mddNode MDD node to be added.
  * @returns true if the node was added, false if the node was already present.
  */
 bool addNode(MDDNode mddNode);

 /**
  * @brief Add a link between two nodes.
  * @param mddNode MDD node.
  * @param child MDD child node.
  * @returns true if the link was added, false if the nodes were not present.
  */
 bool addLink(MDDNode mddNode, MDDNode child);

 /**
  * @brief Remove a link between two nodes.
  * @param mddNode MDD node.
  * @param child MDD child node.
  * @returns true if the link was removed, false if the nodes were not present.
  */
 bool removeLink(MDDNode mddNode, MDDNode child);

 /**
  * @brief Get the children of the given node.
  * @param mddNode MDD node.
  * @returns Vector of children of the given node.
  */
 std::vector<MDDNode> getChildren(MDDNode mddNode) const;

 /**
  * @brief Count how many nodes are present on that level.
  * @param level MDD level, equals to depth of the path.
  * @returns Number of nodes on given level of MDD.
  */
 size_t count(size_t level) const;

 /**
  * @brief Return true if the node is present in the MDD.
  * @param mddNode MDD node.
  * @returns true if the node is present.
  */
 bool contains(MDDNode mddNode) const { return mdd.find(mddNode) != mdd.end(); }

 /**
  * @brief Get maximum depth of the MDD, this equals to the length of the possible path.
  * @returns Maximum depth of the MDD
  */
 size_t getLength() const { return mdd.rbegin()->first.getLevel() + 1; }

 /**
  * @brief Prune the given node from the MDD. Searches also for orphan children or childless parents and remove them
  * @param mddNode Node to be pruned.
  */
 void prune(MDDNode mddNode);

 /**
  * @brief Prune the MDD from orphan nodes.
  */
 void prune();

 /**
  * @brief Get the first node of the MDD.
  * @returns the first node of the MDD.
  */
 MDDNode getBegin() const { return mdd.begin()->first; }

 /**
  * @brief Get the last node of the MDD.
  * @returns the last node of the MDD.
  */
 MDDNode getEnd() const { return mdd.rbegin()->first; }

 /**
  * @brief Pad the MDD to a fixed depth by adding the last node until length = maxLength.
  * @param maxLength Desired length of the future MDD.
  */
 void pad(size_t maxLength);

 void create_dot_file(std::string filename = "mdd.dot") const;

 friend std::ostream & operator<<(std::ostream & stream, const MDD & M);
};

#endif  // MAOF_MDD_HPP
