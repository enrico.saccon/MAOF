//
// Created by Paolo Bevilacqua on 05/09/17.
//

#ifndef MAOF_TIMEPERF_HPP
#define MAOF_TIMEPERF_HPP

#include <chrono>

class TimePerf
{
public:
  TimePerf() = default;
  TimePerf(const TimePerf& tp) = default;
  virtual ~TimePerf() = default;

  void start() { startTime = std::chrono::high_resolution_clock::now(); }

  template <typename TimeGran = std::milli>
  double getTime()
  {
    auto endTime = std::chrono::high_resolution_clock::now();
    auto diff = endTime - startTime;
    return std::chrono::duration<double, TimeGran>(diff).count();
  }

private:
  std::chrono::time_point<std::chrono::high_resolution_clock> startTime;
};

#ifdef MAOF_PROFILE

// System includes
#include <map>
#include <iostream>

// Library includes
#include <Utils.hpp>

/**
 * This function should be used at the end of a function or a piece of code you want to profile
 * @param profiler_name The name of the profiler
 */
void profile(const std::string & profiler_name);

/**
 * @brief This function should be used at the beginning of either the profiled function or the
 * profiled piece of code to restart the profiler.
 * @param profiler_name The name of the profiler
 */
void restartProfiling(const std::string & profiler_name);

#else // MAOF_PROFILE

#include <iostream>

inline void profile(const std::string & profiler_name) {}
inline void restartProfiling(const std::string & profiler_name) {}

#endif // MAOF_PROFILE
#endif  //MAOF_TIMEPERF_HPP
