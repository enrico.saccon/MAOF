# Testing Scripts

This folder contains Python scripts that are used to either generate tests or to test the executables and parse the 
output. 

The tests should then be copied to the folder `jsonTesting` which will be automatically copied after successful 
compilation by Cmake.

There are two sub-folders:

- `generate`: it contains scripts to _generate_ the tests. This contains 
  - `shortestPath`: it uses the NetworkX Python module to generate random graphs and compute the shortest path between
  points. Then these tests can be used to test the correctness of SAPF solvers.
  - `warehouse`: it is a hand-written (sorry but there wasn't another way) series of scripts to construct the different
  sectors of the warehouse. In the `maps` folder there are the scripts that will produce the sectors of the warehouse 
  (the name of the script gives the sector, e.g., script `WH2_2.py` will produce the sector WH2_2). These are all
  undirected graphs. `oneWayMaps` (directed connected graphs) are not finished yet, so do not use.

- `test`: it contains scripts to _test_ and _parse_ the output of the solvers. Please refer to the withing `README.md`.

## Requirements.txt

This folder contains a `requirements.txt` file that contains all the modules that should be installed to make all the 
scripts inside all the subfolders work. If you are interested in using only one of the subfolders, then separate
`requirements.txt` files are present within. 
