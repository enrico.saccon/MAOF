# Testing

This folder is specifically used to store testing scripts, that is scripts that either run the tests or that run and
parse the tests. 

`testRandom.py`: _NOT IMPLEMENTED YET_

## Parser

This script (contained in the `parser` folder and called by the `tester.py` script) is used to run and parse the tests
as defined by YAML file that it takes in input. Such input file has the following fields:

- `exec`: a dictionary:
  - `path`: the path to the executable to test;
  - `args`: the arguments that should be passed. This string should contain at some points `"%%"` as this will later be
    replaced with the test path.
- `outputFolder`: the folder where the outputs of the executable are going to be stored. The parser will create a
  subfolder named with the date and time of the test.
- `tests`: a dictionary:
  - `testPath`: set only if there is only one tests that needs to be done.
  - `testsFolder`: a folder containing the JSON for the tests scenarios.
  - `testRe`: a regex that is used to check if a file in the `testsFolder` dir is actually a test that has to be 
    considered or not. The parser also considers the sub-folders and navigate them if they are a match for the regex.
- `repeat`: how many times the tests should be repeated. _NOT IMPLEMENTED YET_
- `timeout`: a list of timeouts (in seconds) that are used to stop a test.
- `variables`: a dictionary of statistical variables that needs to be evaluated. The statistical variables can be:
  - _Normal_: they store a value by matching a regex. In this class, the statistical variable `success` is __MANDATORY__
    as it is used to check if the test was successful or not and nothing else will work without this. All of these
    variables have two fields:
    - `type`: it can be:
      - `bool`: only stores `True` or `False`
      - `num`: increments a floating variables when a regex is matched. If no group is specified inside the regex, then
        the value is increased only by one.
      - `stat`: increments a floating variables and a counter. It also keeps track of the minimum and maximum values
        that matched the regex, and finally it will return the average value dividing the sum by the counter. 
    - `r`: the regex that needs to be matched.
  - _Cross_: they store a dictionary where each entry is another _normal_ variable. This allows to store values given
    some other piece of information, e.g., we want to know the average computational time per number of nodes in the
    graph. These variables have three fields:
    - `r1`: the regex to match the data we want to store (e.g., the average times);
    - `r2`: the regex to match the fields of the dictionary (e.g., the number of nodes).
    - `type`: it can be:
      - `crossNum`: for each output, if `r1` is matched, then for each occurrence of `r2` increment an integer variable;
      - `crossStat`: for each output, if `r1` is matched, then search for `r2` and add the corresponding values. This 
        will use a `stat` variable to compute average, minimum and maximum.
  - _File_: it is better to leave the input file `parse.yaml` as clean as possible, hence one should add the variables 
    that they want to consider for a specific algorithm inside the `yaml` folder. For example, the folder contains the 
    `ICR.yaml` file which contains variables especially thought for the ICR algorithm. To tell the parser to get also
    the variables in said file, one should simply add the field `type: yaml` and specify the path inside the `path` key.

Check `parse.yaml` and the `yaml` folder for examples. 

By using the following parser, one can simply create and keep track of all the statistical variables that needs to be
taken into consideration. 

This script uses the `subprocess` module to run the executable, so make sure that what you are running can be trusted. 
Moreover, it uses the `timeout` command to check if the process has ended within the given timeout. On UNIX, this
command should already be present, on Mac it can be installed via Brew with `brew install coreutils` and then creating
an alias in your ~/bashrc or whatever you are using as `alias timeout = gtimeout`.

## NOTICE

Please use the macro `PARSER` in the code and compile with `CMAKE_BUILD_TYPE=MAOF_PARSER` to maintain the code clean and
ensure that the regexes are correct. 

## Future work

- Instead of using `timeout`, use [runlim](http://fmv.jku.at/runlim/) which also allows to consider the amount of memory
  used by the process. 
- Latex support: print tables;
- Graphical support: allow statistical variables to plot graphs. 
- Increment the dimensions for _cross_ variables.

