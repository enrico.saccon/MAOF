import yaml

from parser import Parser
from argparse import ArgumentParser


def singleParser():
    """
    Function to test a single executable on multiple tests, for example, to test only CBS.
    :return: None
    """
    parser = ArgumentParser(
        prog="Tester",
        description="This script allows the programmer to easily test and parse an executable on multiple scenarios."
    )
    parser.add_argument("-i", "--input", metavar="input", type=str, help="The path to the YAML input file")
    args = parser.parse_args()

    filename = ""
    if args.input:
        filename = args.input

    timeouts = []
    solver = []

    with open(filename, "r") as file:
        if filename.endswith(".yaml") or filename.endswith(".yml"):
            yamlFile = yaml.safe_load(file)
            print(yamlFile["exec"], type(yamlFile["exec"]))
            print(yamlFile["exec"]["args"], type(yamlFile["exec"]["args"]))
            solvers = yamlFile["exec"]["args"]["solvers"]
            timeouts = yamlFile["timeout"]
        else:
            raise Exception("Input file {} not supported".format(filename))

    for timeout in timeouts:
        for solver in solvers:
            print("EXECUTING {} WITH TIMEOUT {} ".format(solver, timeout))
            testParser = Parser(filename, solver, timeout)
            testParser.exec()
            print(testParser)
            print()



if __name__ == "__main__":
    singleParser()
