import os, subprocess
import re
import yaml
from pathlib import Path
from time import localtime, strftime


class Parser:
    from ._createStats import createStatVars

    def __init__(self, filename, _solver : dict, _timeout = 1):
        """
        :param filename:
        :param _solver: A dictionary where the key is the option of the executable and the values is the argument to be passed
        :param _timeout:
        """
        self.count = 0
        self.execPath = ""
        self.execArgs = ""
        self.outputFolder = ""
        self.testsFolder = ""
        self.testsRe = ""
        self.testPath = ""
        self.repeat = 0
        self.solver = _solver
        self.timeout = _timeout
        self.parserStruct = {}
        self.failed = []
        self.solved = []

        if filename.endswith(".yaml") or filename.endwith(".yml"):
            self.__from_yaml(filename)
        else:
            raise Exception ("Input file format not supported")

    def __from_yaml(self, filename):
        data = None
        with open(filename, "r") as yamlFile:
            data = yaml.safe_load(yamlFile)

        # Set executable path
        if data["exec"]["path"] == "":
            raise Exception("Executable must be specified")
        else:
            self.execPath = data["exec"]["path"]
            self.execArgs = data["exec"]["args"]["inputFile"]
            for key, val in self.solver.items():
                self.execArgs += " " + key + " " + val

        # Create output directory
        if not os.path.isdir(data["outputFolder"]):
            os.mkdir(data["outputFolder"])
        stime = strftime("%Y-%m-%d_%H.%M.%S", localtime())
        self.outputFolder = os.path.join(data["outputFolder"], stime)
        # Add info to outputFolder name in a specific order
        if "-M" in self.solver.keys(): self.outputFolder += "_" + self.solver["-M"]
        if "-s" in self.solver.keys(): self.outputFolder += "_" + self.solver["-s"]
        if "-S" in self.solver.keys(): self.outputFolder += "_" + self.solver["-S"]
        if "-c" in self.solver.keys(): self.outputFolder += "_" + self.solver["-c"]
        if "-H" in self.solver.keys(): self.outputFolder += "_" + self.solver["-H"]
        self.outputFolder += "_" + str(self.timeout)

        if not os.path.isdir(self.outputFolder):
            os.mkdir(self.outputFolder)

        # Set tests variables
        if data["tests"]["testPath"] != "" and data["tests"]["testsFolder"] != "":
            raise Exception("Cannot have both testPath and testsFolder set")
        else:
            if data["tests"]["testPath"] != "":
                self.testPath = data["tests"]["testPath"]
            else:
                self.testsFolder = data["tests"]["testsFolder"]
                self.testsRe = data["tests"]["testRe"]

        # Set repeatability
        self.repeat = data["repeat"]

        self.createStatVars(data["variables"])

    def exec(self):
        if self.testPath != "":
            self.__execTest()
        else:
            self.__execTests()

        self.__saveLog()

    def __execTest(self):
        self.count += 1
        args = self.execArgs.replace("%%", self.testPath)
        print("executing", ["timeout", str(self.timeout), self.execPath]+args.split(" "))
        result = subprocess.run(["timeout", str(self.timeout), self.execPath]+args.split(" "), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out = result.stdout.decode('utf-8')
        err = result.stderr.decode('utf-8')
        if self.parserStruct["success"].parse(out):
            self.solved.append(self.testPath)
            for key in self.parserStruct:
                if key == "success":
                    continue
                self.parserStruct[key].parse(out)
        else:
            self.failed.append(self.testPath)

        with open(os.path.join(self.outputFolder, Path(self.testPath).stem), "w+") as output:
            output.write(str([self.execPath]+args.split(" ")))
            output.write("\n")
            output.write("----------------------------------------------------------------------------------\n")
            output.write(out)
            output.write("\n")
            output.write(err)
            output.write("\n")

    def __execTests(self):
        ls = os.listdir(self.testsFolder)
        for file in ls:
            if re.search(self.testsRe, file):
                if os.path.isdir(os.path.join(self.testsFolder, file)):
                    oldTest = self.testsFolder
                    oldOutput = self.outputFolder
                    os.mkdir(os.path.join(self.outputFolder, file))
                    self.outputFolder = os.path.join(self.outputFolder, file)
                    self.testsFolder = os.path.join(self.testsFolder, file)
                    print("new folder:", self.testsFolder)
                    self.__execTests()
                    self.testsFolder = oldTest
                    self.outputFolder = oldOutput
                else:
                    if file.endswith(".json"):
                        self.testPath = os.path.join(self.testsFolder, file)
                        self.__execTest()

    def __str__(self):
        statVars = ""
        for key in self.parserStruct:
            statVars+="{}: {}\n".format(key, self.parserStruct[key])

        solvFailTable = ""
        tot = self.solved + self.failed
        for t in sorted(tot):
            if t in self.solved:
                solvFailTable += (t+" V\n")
            else:
                solvFailTable += (t+" X\n")

        return "Successful: {}/{}\nrun <{} {}>\n{}\nOutput dir to {}\nRepeat: {}\nTimeouts: {}\nStatistical variables:\n{}\nSolved: {}\nFailed: {}\nSolved/Failed Table:\n{}".format(
            len(self.solved), self.count,
            self.execPath, self.execArgs,
            "Test: {}".format(self.testPath) if self.testsFolder == "" else "Tests in {} re: <{}>".format(self.testsFolder, self.testsRe),
            self.outputFolder,
            self.repeat,
            self.timeout,
            statVars,
            sorted(self.solved),
            sorted(self.failed),
            solvFailTable)

    def __saveLog(self):
        with open(os.path.join(self.outputFolder, "log.log"), "w+") as output:
            output.write(str(self))