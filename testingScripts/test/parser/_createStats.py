from .parserClasses.Bool import Bool
from .parserClasses.Stat import Stat
from .parserClasses.Num import Num
from .parserClasses.Cross import CrossStat, CrossNum

import yaml

# Create statistical variables
def createStatVars(self, vars):
    for key in vars:
        if vars[key]["type"] == "stat":
            self.parserStruct[key] = Stat(vars[key]["r"])
        elif vars[key]["type"] == "num":
            self.parserStruct[key] = Num(vars[key]["r"])
        elif vars[key]["type"] == "bool":
            self.parserStruct[key] = Bool(vars[key]["r"])
        elif vars[key]["type"] == "crossStat":
            self.parserStruct[key] = CrossStat(vars[key]["r1"], vars[key]["r2"])
        elif vars[key]["type"] == "crossNum":
            self.parserStruct[key] = CrossNum(vars[key]["r1"], vars[key]["r2"])
        elif vars[key]["type"] == "yaml":
            filename = vars[key]["path"]
            file = open(filename)
            if not file:
                raise Exception("Cannot find sub yaml file {}".format(filename))
            newVars = yaml.safe_load(file)
            self.createStatVars(newVars)
            file.close()
