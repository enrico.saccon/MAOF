import re

class Bool:
    def __init__(self, r):
        self.v = False
        self.r = r

    def parse(self, string):
        ret = re.search(self.r, string)
        if ret:
            self.v = True
        else:
            self.v = False
        return self.v

    def __str__(self):
        return str(self.v)

    def main(self):
        return self.v