import re

class Stat:
    def __init__(self, r):
        self.min = float("inf")
        self.max = 0
        self.avg = 0
        self.cnt = 0
        self.r = r
        self.values = []

    def parse(self, string):
        if self.r == "":
            return False
        ret = re.search(self.r, string)
        if ret:
            if len(ret.groupdict()) == 0:
                val = 1
                self.avg += 1
                self.values.append(1)
            else:
                val = float(ret["val"])
                self.avg += val
                self.cnt += 1
                self.values.append(val)
            self.max = max(val, self.max)
            self.min = min(val, self.min)
            return True
        else:
            return False

    def mean(self):
        return self.avg/self.cnt if self.cnt>0 else 0

    def main(self):
        return self.mean()

    def __str__(self):
        return "min {:.2f} avg {:.2f} max {:.2f} counter {:d} values: {:s}".format(self.min, self.mean(), self.max, self.cnt, str(self.values))
