import re
from Float import Float

class CrossF:
    def __init__(self, r1, r2):
        self.values = dict()
        self.r1 = r1
        self.r2 = r2

    def parse(self, string):
        m2 = re.search(self.r2, string)
        if m2:
            if m2["val"] in self.values.keys():
                self.values[m2["val"]].parse(string)
            else:
                self.values[m2["val"]] = Float(self.r1)
                self.values[m2["val"]].parse(string)
            return True
        else:
            return False

    def __str__(self):
        string = "{\n"
        for key in self.values:
            string += "  {}, {}\n".format(key, str(self.values[key]))
        string += "}"
        return string
