import re

class Num:
    def __init__(self, r):
        self.v = 0.0
        self.r = r

    def parse(self, string):
        ret = re.search(self.r, string)
        if ret:
            if len(ret.groupdict()) == 0:
                self.v += 1
            else:
                self.v += float(ret["val"])
            return True
        return False

    def __str__(self):
        return str(self.v)

    def main(self):
        return self.v