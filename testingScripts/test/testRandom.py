"""
This file contains functions to create random graphs for testing.
The function `generate_random_easy` prints a connectivity matrix to stdout and shows the graph.
The function `generate_random` writes the data to a .json file and saves the graph to a PDF file.
"""
import os
import random
import subprocess
import sys
from pathlib import Path

import numpy as np
import networkx as nx
import json

import shutil

max_duration = 1  # seconds


def create_connectivity(n_nodes, p):
    connect = np.ones((n_nodes, n_nodes), dtype=int)
    for i in range(n_nodes):
        for j in range(n_nodes):
            if i != j and np.random.uniform() > p:
                connect[i][j] = 0
        while np.matrix.sum(np.asmatrix(connect), axis=1)[i] == 0:
            for j in range(n_nodes):
                if i != j and np.random.uniform() > p:
                    connect[i][j] = 0

    return connect


def generate_random_easy(n_nodes, p=0.25, delimiter=("[", "]"), space=","):
    """
    Function to randomly generate a test.
    :param n_nodes: the number of nodes in the scenario
    :param p: the threshold probability of an edge between two nodes existing
    :param delimiter: when printing, which parenthesis to use
    :param space: when printing, which separator to use
    """
    conn = create_connectivity(n_nodes, p)

    for i in range(n_nodes):
        print(delimiter[0], end="")
        for j in range(n_nodes):
            print(int(conn[i][j]), end=(space if j < n_nodes - 1 else ""))
        print(delimiter[1])

    # This is to avoid showing the self-loops
    for i in range(n_nodes):
        conn[i][i] = 0
    graph = nx.from_numpy_matrix(conn, create_using=nx.DiGraph())

    return graph


def generate_random(n_nodes, n_agents, n_goals, p, filename):
    data = dict()
    data["n_agents"] = n_agents
    data["n_nodes"] = n_nodes
    data["nodes"] = [[i, 0, 0] for i in range(1, n_nodes + 1)]
    tmp_init = random.sample(data["nodes"], n_agents)
    data["initPos"] = [{"ID": (a + 1), "node": tmp_init[a]} for a in range(n_agents)]
    if n_agents >= 70 * n_nodes / 100:
        data["endPos"] = data["initPos"]
    else:
        tmp_end = [n for n in data["nodes"] if n not in tmp_init]
        data["endPos"] = [{"ID": (a + 1), "node": tmp_end[a]} for a in range(n_agents)]
    if not (len(data["initPos"]) == len(data["endPos"]) and len(data["initPos"]) == n_agents):
        raise Exception("Wrong length")

    data["goalPos"] = [{"ID": a + 1, "nodes": []} for a in range(n_agents)]
    nonEndPos = [x for x in data["nodes"] if x not in [y["node"] for y in data["endPos"]]]
    for a in range(n_agents):
        data["goalPos"][a]["nodes"] = [random.choice(nonEndPos)]
        for i in range(1, n_goals):
            tmp = random.choice(nonEndPos)
            while tmp == data["goalPos"][a]["nodes"][-1]:
                tmp = random.choice(nonEndPos)
            data["goalPos"][a]["nodes"].append(tmp)

    # Make sure that no goalPos is also an endPos
    for a in range(n_agents):
        for n in data["goalPos"][a]["nodes"]:
            if n in [y["node"] for y in data["endPos"]]:
                raise Exception("goalPos is already an endPos")

    for goal in data["goalPos"]:
        for n in goal["nodes"]:
            if n in data["endPos"]:
                assert ("Wrong value in goalPos")

    found_connected = False
    while not found_connected:
        connect = create_connectivity(n_nodes, p)
        data["connect"] = connect.tolist()

        # This is to avoid showing the self-loops
        for i in range(n_nodes):
            connect[i][i] = 0
        graph = nx.from_numpy_matrix(connect, create_using=nx.DiGraph())
        if not nx.is_strongly_connected(graph):
            continue
        else:
            found_connected = True

        graph.clear()

    with open(filename + ".json", "w+") as file:
        json.dump(data, file, indent=2)


def generate_all():
    """
    Generate all the random tests once and for all. It simply calls generate_and_test without any executable.
    """
    generate_and_test("")


def test_case(exec, case, exec_csv):
    """
    Runs a shell passing the program and the test case. Moreover, it uses the system call timeout (gtimeout on macOS) to
    avoid running too long. The value is set with the global constant max_duration.
    :param exec: the path to the executable
    :param case: the path to the test case to be run
    :param exec_csv: the CSV file onto which to save data
    """
    output = subprocess.run(["timeout", "--kill={}".format(max_duration + 5), str(max_duration), exec, case],
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    case_id = Path(case).stem
    exec_name = exec.split("/")[-1]
    if not os.path.isdir(os.path.join("output", exec_name)):
        os.mkdir(os.path.join("output", exec_name))

    # print(output.stdout.decode("UTF-8"))
    with open(os.path.join("output", exec_name, case_id + ".txt"), "w+") as f:
        f.write(output.stdout.decode("UTF-8"))


def test_all(exec, dir_cases, exec_csv):
    """
    This function is used to test the program over a set of tests already defined.
    :param exec: the path to the executable
    :param dir_cases: the directory containing the tests
    :param exec_csv: the CSV file onto which to save data
    :return:
    """
    files = [f for f in os.listdir(dir_cases) if ".json" in f]
    files.sort(key=lambda x: int(x.split("_")[1].split(".")[0]))
    for file in files:
        print("\rRunning {}/{}".format(files.index(file) + 1, len(files)), end="")
        test_case(exec, os.path.join(dir_cases, file), exec_csv)


def generate_and_test(exec_path, exec_csv):
    """
    This function is used to generate and run the tests at the same time. It should be avoided when comparing different
    executables.
    :param exec_path: the path to the executable
    :param exec_csv: the CSV file onto which to save data
    """
    with open("tests/index.csv", "w+") as file:
        file.write("counter,a_occ,n_nodes,n_agents,n_g,p\n")

        agents_occ = [5, 10, 20, 50, 75]  # Percents of agents on nodes
        nodes = [10, 20, 50, 100]  # Number of nodes in the scenario
        n_goals = [1, 2, 5, 10, 20, 50]  # Number of goals in the scenario
        p_conn = [0.1, 0.25, 0.5]

        random.seed(1234)
        np.random.seed(1234)

        counter = 0
        for n_g in n_goals:
            for n_nodes in nodes:
                for a_occ in agents_occ:
                    n_agents = max(2, int(a_occ / 100.0 * n_nodes))
                    if n_agents >= n_nodes:
                        raise Exception("Too many agents for nodes {}>={}".format(n_agents, n_nodes))

                    for p in p_conn:
                        file.write("{},{},{},{},{}\n".format(counter, n_nodes, n_agents, n_g, p))
                        print(n_nodes, n_agents, n_g, p)
                        generate_random(n_nodes, n_agents, n_g, p, "tests/test_{}".format(counter))

                        if exec_path and exec_csv:
                            test_case(exec_path, "tests/test_{}.json".format(counter), exec_csv)

                        counter += 1

                sys.exit(1)


def main():
    # read_neigh_to_connect(sys.argv[1])
    # generate_mag_all(sys.argv[1])

    global timeout
    main_dir = os.path.join(os.getcwd())
    testing_dir = os.path.join(os.getcwd(), "testing")
    for tt in [1, 10, 60]:
        timeout = tt
        timeout_dir = os.path.join(testing_dir, str(timeout)+"s")
        if not os.path.isdir(timeout_dir):
            os.mkdir(timeout_dir)

        for rep in range(3):
            rep_dir = os.path.join(timeout_dir, str(rep))
            if not os.path.isdir(rep_dir):
                os.mkdir(rep_dir)

            execs = ["CBS_ST_MKS", "CBS_ST_SIC", "CBS_TDSP_MKS", "CBS_TDSP_SIC", "CP_MKS", "CP_SIC"]
            for testing in execs:
                print(testing)
                exec = "./exec/{}".format(testing)
                exec_csv = "../output/{}.csv".format(testing)
                test_all(exec, "tests", exec_csv)

            if not os.path.isdir(os.path.join("testing", str(timeout)+"s", str(rep), "output")):
                os.mkdir(os.path.join("testing", str(timeout)+"s", str(rep), "output"))
            if not os.path.isdir(os.path.join("testing", str(timeout) + "s", str(rep), "mag-output")):
                os.mkdir(os.path.join("testing", str(timeout) + "s", str(rep), "mag-output"))

            shutil.copytree("output", os.path.join("testing", str(timeout)+"s", str(rep), "output"))
            shutil.copytree("mag-output", os.path.join("testing", str(timeout) + "s", str(rep), "mag-output"))

    # generate_and_test(exec, exec_csv)


if __name__ == "__main__":
    main()
