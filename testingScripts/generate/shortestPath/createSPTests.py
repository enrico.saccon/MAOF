#!/usr/local/env python3
import os.path, sys
import random
import json
import numpy as np
import networkx as nx
import argparse

SEED = 13
TESTS_PER_PROBLEM = 10
N_GOALS = 10
N_NODES = [20, 50, 100, 200, 500, 1000]
PS = [0.15, 0.2, 0.35, 0.5, 0.6]
OUT_DIR = "SPTests"

def createProblem(n, p):
    '''
    :brief: Function to create a TSP problem with multiple intermediate goals.
    :param n: The number of nodes in the graph
    :param p: The probability of an edge existing between two nodes
    :return:
    '''
    G=nx.erdos_renyi_graph(n, p, directed=False, seed=SEED)
    i = 0
    while (nx.is_empty(G) or not(nx.is_connected(G))):
        G=nx.erdos_renyi_graph(n, p, directed=False, seed=SEED)
        i += 1

    # Add self-loops
    for n in G.nodes:
        G.add_edge(n, n)

    # List of goals, plus source (initial node) and target (last node)
    goals = [random.choices(list(G.nodes), k=N_GOALS+2) for i in range(TESTS_PER_PROBLEM)]

    return (G, goals)


def solveProblem(G, goals, i, n, filename):
    '''
    :brief: Function to solve a TSP problem with multiple intermediate goals.
    :details: The function solves the problem by finding the shortest path between each pair of consecutive goals, and
     then concatenating the results. If no path exists between two consecutive goals, the function returns an empty
     list. Also saves the solution in a JSON file.
    :param G: The graph representing the problem
    :param goals: The list of goals
    :param i: The index of the problem to solve
    :param n: The number of nodes in the graph
    :param filename: The name of the file in which to save the solution
    :return: None
    '''
    source = goals[i][0]
    target = goals[i][-1]
    sol = []
    try:
        for g in range(N_GOALS+1):
            if goals[i][g] == goals[i][g+1]:
                sol.append(goals[i][g])
            else:
                sol += nx.shortest_paths.shortest_path(G, source=goals[i][g], target=goals[i][g+1])[:-1]
        sol.append(target)
        print(type(sol), sol)
    except Exception as E:
        if type(E) == nx.exception.NetworkXNoPath:
            print("No path between {} and {}".format(source, target))
            sol = []
        else:
            print("Other exception "+str(E))
            sol = []

    with open(filename, "w+") as f:
        np.set_printoptions(threshold=sys.maxsize)
        dic = {
            "n" : n,
            "source" : source,
            "target" : target,
            "goals" : goals[i][1:-1],
            "sol" : sol,
            "connectivity" : nx.to_numpy_array(G).astype(int).tolist()
        }
        json.dump(dic, f)


def main():
    count = 0
    for n in N_NODES:
        for p in PS:
            (G, goals) = createProblem(n, p)
            for i in range(TESTS_PER_PROBLEM):
                print("[{}, {} {}->{} through {}]".format(n, p, goals[i][0], goals[i][-1], goals[i][1:-1]), end=" ")
                filename = os.path.join(OUT_DIR, str(count)+".json")
                solveProblem(G, goals, i, n, filename)
                count += 1
                # break
            # break
        # break


def argsParse():
    global N_NODES
    global PS
    global SEED
    global TESTS_PER_PROBLEM
    global N_GOALS
    global OUT_DIR

    parser = argparse.ArgumentParser(description='Generate shortest path tests')
    parser.add_argument('-n', '--nnodes', type=str, dest='n_nodes', help='A coma-separated list of integer values representing the number of nodes for which tests should be created.')
    parser.add_argument('-p', '--probs', type=str, dest='ps', help='A coma-separated list of integer values representing the probabilities of edge creation.')
    parser.add_argument('-s', '--seed', type=int, dest='seed', default=SEED, help='The seed to use for the random number generator.')
    parser.add_argument('-t', '--tests', type=int, dest='tests', default=TESTS_PER_PROBLEM, help='The number of tests to create for each problem.')
    parser.add_argument('-g', '--goals', type=int, dest='goals', default=N_GOALS, help='The number of goals to create for each test.')
    parser.add_argument('-o', '--out', type=str, dest='out', default=OUT_DIR, help='The directory in which to save the tests.')

    args = parser.parse_args()
    if args:
        if args.n_nodes and args.n_nodes != "":
            N_NODES = [int(x) for x in args.n_nodes.split(",")]
        if args.ps and args.ps != "":
            PS = [float(x) for x in args.ps.split(",")]
        SEED = args.seed
        TESTS_PER_PROBLEM = args.tests
        N_GOALS = args.goals
        OUT_DIR = args.out

    if not os.path.isdir(OUT_DIR):
        os.mkdir(OUT_DIR)

    print("Running problem with the following parameters:\n"
          "\tN_NODES = {}\n"
          "\tPS = {}\n"
          "\tSEED = {}\n"
          "\tTESTS_PER_PROBLEM = {}\n"
          "\tN_GOALS = {}\n"
          "\tOUT_DIR = {}".format(N_NODES, PS, SEED, TESTS_PER_PROBLEM, N_GOALS, OUT_DIR))


if __name__ == "__main__":
    argsParse()
    random.seed(SEED)
    main()