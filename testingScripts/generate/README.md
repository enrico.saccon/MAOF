# Generate Maps

This folder contains scripts to generate maps. The modules needed can be found in `requirements.txt`. 

- `shortestPath`: it uses the `NetworkX` module to create random graphs. Then, it chooses random initial and final nodes 
  and compute the shortest path between them using [Dijkstra](https://networkx.org/documentation/stable/reference/algorithms/generated/networkx.algorithms.shortest_paths.generic.shortest_path.html#networkx.algorithms.shortest_paths.generic.shortest_path).
  It returns a JSON file containing:
  - `n`: the number of nodes;
  - `source`: the node which is the initial position;
  - `target`: the node which should be reached;
  - `sol`: a list containing the nodes that are traversed, empty if no solution was found;
  - `connectivity`: an adjacency matrix.
  The files are saved in the local directory `SPTests`, but should be then moved to `jsonTesting` in the main folder.
- `warehouse`: it creates the maps based on the warehouse. 
  - `maps`: it contains the scripts to generate the JSON files describing the sectors of the map. Due to the structure
    of the warehouse, the scripts had to be hand-made, so they cannot be easily automated. For example, to generate the
    sector `WH1` use the command:
    ```commandline
    python3 WH1.py 
    ```
    
    With the command:
    ```commandline
    python3 WH.py
    ```
    All the warehouses are generated.

    And that should create the corresponding file, both as a JSON and as a visual graph in PDF and PNG. _Notice_ that at
    the beginning of each script, a variable with the path to where the file should be saved is set. That path __MUST__
    exist before the script is run as it does not check whether it exists or not, nor it creates it for you. The JSON
    file contains:
    - `nNodes`: the number of nodes in the sector;
    - `nodes`: a list of nodes. Each node is a list of 4 elements:
      - The value of the node;
      - The x coordinate of the node;
      - The y coordinate of the node;
      - The ID of the node, that is, the position of the node inside the `nodes` list. 
    - `connect`: the adjency matrix of the problem. This is __NOT__ a unitary map.
    
    The maps created are _undirected_ graphs.
    
    Notice the class `Node` inside `Node.py`, which allows to easily change the position of the fields of the node.

  - `oneWayMaps`: _NOT FINISHED YET_. It will contain scripts to obtain one way maps, i.e., _directed_ graphs.
  - `generateWH.py`: a script to generate test cases on the warehouse maps. More information is available by calling
    the script with `--help` or in the withing documentation.