#!/usr/bin/env/ python3

import json
import networkx as nx
import matplotlib.pyplot as plt
import os
from Node import Node

def func(G, nodes, labeldict, i, j):
    string = "{:d}{:02d}".format(i, j)
    n = len(nodes)
    nodes.append(Node(int(string), n, i, j))
    print(nodes[-1])
    G.add_node(n, pos = (-i,j))
    labeldict[n] = string

def addConnection(G, connect, nid, toid):
    connect[nid][toId]=1
    connect[toId][nid]=1
    G.add_edge(nid, toId)
    G.add_edge(toId, nid)

filename = "output/WH12/wh12"

xMin = 1
xMax = 8
yMin = 31
yMax = 59

G = nx.Graph()
# create nodes for mag1
nNodes = 0
nodes = []
labeldict = {}
for i in range(xMin,xMax+1):
    for j in range(yMin,yMax+1):
        func(G, nodes, labeldict, i, j)

mag1Nodes = len(nodes)

# create intermediate nodes
for j in [31, 40, 47]:
    func(G, nodes, labeldict, 9, j)
for j in [39, 46]:
    func(G, nodes, labeldict, 10, j)
for j in [40, 47]:
    func(G, nodes, labeldict, 11, j)
for j in [41, 48]:
    func(G, nodes, labeldict, 12, j)
for j in [28, 32, 39]:
    func(G, nodes, labeldict, 13, j)
for j in [29, 31, 40]:
    func(G, nodes, labeldict, 14, j)
for j in [30, 41]:
    func(G, nodes, labeldict, 15, j)
for j in [20, 28]:
    func(G, nodes, labeldict, 16, j)

# create nodes for mag2
xMin = 17
xMax = 23
yMin = 1
yMax = 22
for i in range(xMin,xMax+1):
    for j in range(yMin,yMax+1):
        if i == 23 and j == 22:
            continue
        func(G, nodes, labeldict, i, j)

    if i == 17:
        func(G, nodes, labeldict, i, 29)
    if i == 18:
        func(G, nodes, labeldict, i, 30)

nNodes = len(nodes)

connect = [[0 for x in range(nNodes)] for x in range(nNodes)]

xMin = 1
xMax = 8
yMin = 31
yMax = 59

# create connections for mag1
dY = yMax - yMin + 1
for i in range(xMin,xMax+1):
    idx = i-xMin
    for j in range(yMin,yMax+1):
        jdx = j-yMin
        nid = idx*dY + jdx
        connect[nid][nid] = 1
        # G.add_edge(nid, nid)
        if jdx>0: 
            connect[nid][nid-1] = 1
            G.add_edge(nid, nid-1)
        if jdx<dY - 1:
            connect[nid][nid+1] = 1
            G.add_edge(nid, nid+1)
        if i<xMax and j == yMax:
            nextId = (idx+1)*dY + dY - 1
            connect[nextId][nid] = 1
            connect[nid][nextId] = 1
            G.add_edge(nextId, nid)
            G.add_edge(nid, nextId)
        if i < xMax and (j==yMin or j==yMin+1):
            nextId = (idx+1)*dY + jdx
            connect[nid][nextId] = 1
            connect[nextId][nid] = 1
            G.add_edge(nextId, nid)
            G.add_edge(nid, nextId)


# create intermediate connections..................
# 931
nid = next(n for n in nodes if n[1]==9 and n[2]==31)[-1]
toId = next(n for n in nodes if n[1]==8 and n[2]==31)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==8 and n[2]==32)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==13 and n[2]==32)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==14 and n[2]==31)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==15 and n[2]==30)[-1]
addConnection(G, connect, nid, toId)

#940
nid = next(n for n in nodes if n[1]==9 and n[2]==40)[-1]
toId = next(n for n in nodes if n[1]==8 and n[2]==40)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==8 and n[2]==41)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==10 and n[2]==39)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==11 and n[2]==40)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==12 and n[2]==41)[-1]
addConnection(G, connect, nid, toId)

#947
nid = next(n for n in nodes if n[1]==9 and n[2]==47)[-1]
toId = next(n for n in nodes if n[1]==8 and n[2]==46)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==8 and n[2]==47)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==8 and n[2]==48)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==10 and n[2]==46)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==11 and n[2]==47)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==12 and n[2]==48)[-1]
addConnection(G, connect, nid, toId)

#1039
nid = next(n for n in nodes if n[1]==10 and n[2]==39)[-1]
toId = next(n for n in nodes if n[1]==9 and n[2]==40)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==10 and n[2]==46)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==13 and n[2]==39)[-1]
addConnection(G, connect, nid, toId)

#1046
nid = next(n for n in nodes if n[1]==10 and n[2]==46)[-1]
toId = next(n for n in nodes if n[1]==9 and n[2]==47)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==10 and n[2]==39)[-1]
addConnection(G, connect, nid, toId)

#1140
nid = next(n for n in nodes if n[1]==11 and n[2]==40)[-1]
toId = next(n for n in nodes if n[1]==9 and n[2]==40)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==11 and n[2]==47)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==14 and n[2]==40)[-1]
addConnection(G, connect, nid, toId)

#1147
nid = next(n for n in nodes if n[1]==11 and n[2]==47)[-1]
toId = next(n for n in nodes if n[1]==9 and n[2]==47)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==11 and n[2]==40)[-1]
addConnection(G, connect, nid, toId)

#1241
nid = next(n for n in nodes if n[1]==12 and n[2]==41)[-1]
toId = next(n for n in nodes if n[1]==9 and n[2]==40)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==12 and n[2]==48)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==15 and n[2]==41)[-1]
addConnection(G, connect, nid, toId)

#1248
nid = next(n for n in nodes if n[1]==12 and n[2]==48)[-1]
toId = next(n for n in nodes if n[1]==9 and n[2]==47)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==12 and n[2]==41)[-1]
addConnection(G, connect, nid, toId)

#1328
nid = next(n for n in nodes if n[1]==13 and n[2]==28)[-1]
toId = next(n for n in nodes if n[1]==13 and n[2]==32)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==16 and n[2]==28)[-1]
addConnection(G, connect, nid, toId)

#1332
nid = next(n for n in nodes if n[1]==13 and n[2]==32)[-1]
toId = next(n for n in nodes if n[1]==13 and n[2]==28)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==13 and n[2]==39)[-1]
addConnection(G, connect, nid, toId)

#1339
nid = next(n for n in nodes if n[1]==13 and n[2]==39)[-1]
toId = next(n for n in nodes if n[1]==13 and n[2]==32)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==11 and n[2]==40)[-1]
addConnection(G, connect, nid, toId)

#1429
nid = next(n for n in nodes if n[1]==14 and n[2]==29)[-1]
toId = next(n for n in nodes if n[1]==14 and n[2]==31)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==17 and n[2]==29)[-1]
addConnection(G, connect, nid, toId)

#1431
nid = next(n for n in nodes if n[1]==14 and n[2]==31)[-1]
toId = next(n for n in nodes if n[1]==14 and n[2]==29)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==14 and n[2]==40)[-1]
addConnection(G, connect, nid, toId)

#1440
nid = next(n for n in nodes if n[1]==14 and n[2]==40)[-1]
toId = next(n for n in nodes if n[1]==14 and n[2]==31)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==12 and n[2]==41)[-1]
addConnection(G, connect, nid, toId)

#1530
nid = next(n for n in nodes if n[1]==15 and n[2]==30)[-1]
toId = next(n for n in nodes if n[1]==15 and n[2]==41)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==18 and n[2]==30)[-1]
addConnection(G, connect, nid, toId)

#1541
nid = next(n for n in nodes if n[1]==15 and n[2]==41)[-1]
toId = next(n for n in nodes if n[1]==15 and n[2]==30)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==12 and n[2]==41)[-1]
addConnection(G, connect, nid, toId)

#1620
nid = next(n for n in nodes if n[1]==16 and n[2]==20)[-1]
toId = next(n for n in nodes if n[1]==16 and n[2]==28)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==17 and n[2]==20)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==17 and n[2]==21)[-1]
addConnection(G, connect, nid, toId)

#1628
nid = next(n for n in nodes if n[1]==16 and n[2]==28)[-1]
toId = next(n for n in nodes if n[1]==16 and n[2]==20)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==13 and n[2]==28)[-1]
addConnection(G, connect, nid, toId)

#1729
nid = next(n for n in nodes if n[1]==17 and n[2]==29)[-1]
toId = next(n for n in nodes if n[1]==17 and n[2]==22)[-1]
addConnection(G, connect, nid, toId)
print(nid, toId, connect[nid][toId])
toId = next(n for n in nodes if n[1]==14 and n[2]==29)[-1]
addConnection(G, connect, nid, toId)

#1830
nid = next(n for n in nodes if n[1]==18 and n[2]==30)[-1]
toId = next(n for n in nodes if n[1]==18 and n[2]==22)[-1]
addConnection(G, connect, nid, toId)
toId = next(n for n in nodes if n[1]==15 and n[2]==30)[-1]
addConnection(G, connect, nid, toId)


# create connections mag2
xMin = 17
xMax = 23
yMin = 1
yMax = 22
dY = yMax - yMin + 1
for i in range(xMin,xMax+1):
    idx = i-xMin
    for j in range(yMin,yMax+1):
        if i == 23 and j == 22: 
            continue
        jdx = j-yMin
        nid = next(n for n in nodes if n[1]==i and n[2]==j)[-1]
        connect[nid][nid] = 1
        # G.add_weighted_edges_from([(nid, nid, 1)])
        if jdx>0: 
            connect[nid][nid-1] = 1
            G.add_weighted_edges_from([(nid, nid-1, 1)])
        if jdx<dY - 1 and not(i == 23 and j == 21):
            connect[nid][nid+1] = 1
            G.add_weighted_edges_from([(nid, nid+1, 1)])
        if i<xMax-1 and j == yMax:
            nextId = next(n for n in nodes if n[1]==i+1 and n[2]==j)[-1]
            connect[nextId][nid] = 1
            connect[nid][nextId] = 1
            G.add_weighted_edges_from([(nextId, nid, 1)])
            G.add_weighted_edges_from([(nid, nextId, 1)])
        if i<xMax and j == yMin:
            nextId = next(n for n in nodes if n[1]==i+1 and n[2]==j)[-1]
            connect[nextId][nid] = 1
            connect[nid][nextId] = 1
            G.add_weighted_edges_from([(nextId, nid, 1)])
            G.add_weighted_edges_from([(nid, nextId, 1)])
        if i<xMax and j == 13:
            nextId = next(n for n in nodes if n[1]==i+1 and n[2]==j)[-1]
            connect[nextId][nid] = 1
            connect[nid][nextId] = 1
            G.add_weighted_edges_from([(nextId, nid, 1)])
            G.add_weighted_edges_from([(nid, nextId, 1)])
        
pos = nx.get_node_attributes(G, 'pos')
plt.figure(figsize=(4096/128, 2160/128))
nx.draw(G, pos, labels = labeldict, with_labels=True, node_size = 170, font_size = 5)
plt.gca().invert_xaxis();
plt.gca().invert_yaxis();
plt.savefig(filename+".pdf", dpi = 1600, orientation = "landscape", transparent = True)
plt.savefig(filename+".png", dpi = 1600, orientation = "landscape", transparent = True)

with open("temp.json", "w+") as output:
    json.dump({
        "nNodes" : nNodes,
        "nodes" : [n.toList() for n in nodes],
        "connect" : connect 
    }, output, indent = 2)

with open("temp.json", "r") as temp: 
    with open(filename+".json", "w+") as output:
        for line in temp:
            if any(x in line for x in ["{" , "}", "nNodes", "nodes", "connect", "]"]):
                output.write(line)
            else:
                line = line.split("\n")[0]
                output.write(line)

os.remove("temp.json")