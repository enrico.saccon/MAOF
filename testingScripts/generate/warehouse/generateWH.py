#!/usr/bin/env python3

"""
This file contains functions to create tests from the warehouse.
"""

import os
import random
import json
import argparse

input = "maps/output"
agents = [2,4,8]
goals = [0,1,5,10]
MAPF = "CBS"
SAPF = "TDSP"
costFunction = "SIC"
heuristic = ""

def generate_wh_test(nodes, nAgents, n_goals, connect, filename) -> None:
    """
    This function creates a random test for a given sector of the warehouse with the given number of nodes, agents and
    number of goals.
    In the JSON files, the following data are saved:
    - The MAPFSolver to be used, as specified in the global variables;
    - The SAPFSolver to be used, as specified in the global variables;
    - The cost function to be used, as specified in the global variables;
    - The heuristic for the SAPFSolver to be used (if any), as specified in the global variables;
    - The number of agent;
    - The number of nodes;
    - The adjacency matrix;
    - The nodes in the scenario;
    - The agents: a dictionary made of the ID of the agent, the initial and final position and a list of goals (if any).
    Some assumptinos on the tests that are enforeced are:
    - A goal cannot be a final position;
    - If the number of agents w.r.t. to the number of nodes (i.e., the density) is less than 50% then the final
      positions are for sure different from the initial position, otherwise the final positions are the initial
      positions.
    :param nodes: A list of nodes composing the scenario.
    :param nAgents: The number of agents in the test.
    :param n_goals: The number of goals for each agent.
    :param connect: The adjacency matrix.
    :param filename: The path to which the JSON should be saved.
    :return: None
    """
    nNodes = len(nodes)
    data = dict()
    data["MAPF"] = MAPF
    data["SAPF"] = SAPF
    data["costFunction"] = costFunction
    data["heuristic"] = heuristic

    data["nAgents"] = nAgents
    data["nNodes"] = nNodes
    data["connect"] = connect
    data["nodes"] = nodes

    if nAgents > nNodes:
        return

    initPos = random.sample(data["nodes"], nAgents)
    if nAgents > 50 * nNodes / 100:
        endPos = initPos
    else:
        endPos = random.sample([n for n in data["nodes"] if n not in initPos], nAgents)
    if not (len(initPos) == len(endPos) and len(initPos) == nAgents):
        raise Exception("Wrong length initPos: {} endPos: {} nAgents {}".format(len(initPos, len(endPos), nAgents)))

    goalPos = [[] for i in range(0, nAgents)]
    nonEndPos = [x for x in data["nodes"] if x not in endPos]
    for a in range(0, nAgents):
        if n_goals > 0:
            goalPos[a].append(random.choice(nonEndPos))
        for i in range(1, n_goals):
            tmp = random.choice(nonEndPos)
            while tmp == goalPos[a][-1]:
                tmp = random.choice(nonEndPos)
            goalPos[a].append(tmp)

    # Make sure that no goalPos is also an endPos
    for a in range(nAgents):
        for n in goalPos[a]:
            if n in endPos:
                raise Exception("goalPos is already an endPos")

    data["agents"] = [{} for i in range(0, nAgents)]
    for a in range(0, nAgents):
        data["agents"][a] ={
            "ID" : a,
            "initPos" : initPos[a],
            "endPos" : endPos[a],
            "goalPos" : goalPos[a]
        }

    with open("temp.json", "w+") as output:
        json.dump(data, output, indent = 2)

    with open("temp.json", "r") as temp:
        with open(filename, "w+") as output:
            for line in temp:
                if any(x in line for x in ["{" , "}", "nNodes", "nodes", "connect", "]", "MAPF", "SAPF", "costFunction", "heuristic", "nAgents", "ID", "goalPos"]):
                    output.write(line)
                else:
                    line = line.split("\n")[0]
                    output.write(line)

    os.remove("temp.json")


def generate_wh(wh_data_file, wh_dir) -> None:
    """
    Takes in input the path to the file containing the information about the sector and produces a test. Moreover,
    it creates the folder "tests" inside the directory of the warehouse (i.e., for ./maps/output/WH1/wh1.json it will
    create ./maps/output/WH1/tests if not present). It also creates a CSV file containing for each test the number of
    agents it contains and the number of goals.
    :param wh_data_file: The path to the JSON file containing the nodes and the adjacency matrix for the scenario.
    :param wh_dir: The path to the directory containing the JSON file.
    :return: None
    """
    testsDir = os.path.join(wh_dir, "tests")
    if not os.path.isdir(testsDir):
        os.mkdir(testsDir)

    jsonFile = None
    with open(wh_data_file, "r") as file:
        jsonFile = json.load(file)

    connect = jsonFile["connect"]
    nodes = jsonFile["nodes"]

    counter = 0
    with open(os.path.join(testsDir, "index.csv"), "w+") as f:
        f.write("id,n_agent,n_goals\n")
        for n_agent in agents:
            for n_goal in goals:
                mag_file = os.path.join(testsDir, "{}.json".format(counter))
                generate_wh_test(nodes, n_agent, n_goal, connect, mag_file)
                counter += 1
                f.write("{},{},{}\n".format(counter, n_agent, n_goal))


def generate_wh_all(wh_dir) -> None:
    """
    Takes as input the directory in which the following folders are contained and then creates the tests for each part:
    ["WH12", "WH1", "WH2", "WH2_1", "WH2_2", "WH2_1_1", "WH2_1_2", "WH2_2_1", "WH2_2_2"]
    :param wh_dir: The directory containing the needed data
    :return: None
    """
    for dir_name in ["WH12", "WH1", "WH2", "WH2_1", "WH2_2", "WH2_1_1", "WH2_1_2", "WH2_2_1", "WH2_2_2"]:
        print(dir_name)
        jsonDir = os.path.join(wh_dir, dir_name)
        jsonFile = os.path.join(jsonDir, dir_name.lower()+".json")
        if os.path.isfile(jsonFile):
            generate_wh(jsonFile, jsonDir)


def parseArguments() -> None:
    """
    Function to parse arguments, refer to --help.
    :return: None
    """
    global MAPF
    global SAPF
    global agents
    global goals
    global heuristic
    global costFunction
    global input

    parser = argparse.ArgumentParser(
        prog = "generateWH",
        description = "Script to produce tests for the warehouse scenarios"
    )
    parser.add_argument('-M', "--MAPF", metavar="MAPF", type=str, help="Specify the MAPF solver to be used.")
    parser.add_argument('-S', "--SAPF", metavar="SAPF", type=str, help="Specify the SAPF solver to be used.")
    parser.add_argument('-A', "--agents", metavar="agents", type=str, help="A comma-separated list containing the number of agents for "
                                                         "which the tests should be produced. For example, -A \"2,5,10\""
                                                         " will produce tests with #agents 2, 5 and 10.")
    parser.add_argument('-G', "--goals", metavar="goals", type=str, help="A comma-separated list containing the number of goals per agent"
                                                        " for which the tests should be produced. For example, -G "
                                                        "\"1,5,10\" will produce tests with 1, 5 and 10 goals per "
                                                        "agent.")
    parser.add_argument('-C', "--costF", metavar="costF", type=str, help="The name of the cost function to be used.")
    parser.add_argument('-H', "--heur", metavar="heuristic", type=str, help="The name of the heuristic to use, if any.")
    parser.add_argument('-i', "--input", metavar="path", type=str, help="The path to the directory containing the "
                                                                        "scenarios dirs")

    args = parser.parse_args()
    if args.MAPF:
        MAPF = args.MAPF
    if args.SAPF:
        SAPF = args.SAPF
    if args.agents:
        agents = []
        for a in args.agents.split(","):
            agents.append(int(a))
    if args.goals:
        goals = []
        for g in args.goals.split(","):
            goals.append(int(g))
    if args.costF:
        costFunction = args.costF
    if args.heur:
        heuristic = args.heur
    if args.input:
        input = args.input

    print("MAPF: ", str(MAPF))
    print("SAPF: ", str(SAPF))
    print("agents: ", str(agents))
    print("goals: ", str(goals))
    print("costFunction: ", str(costFunction))
    print("heuristic: ", str(heuristic))


def main():
    random.seed(1) # So that every time I call this script the tests will be the same.
    parseArguments()
    generate_wh_all(input)

if __name__ == "__main__":
    main()
