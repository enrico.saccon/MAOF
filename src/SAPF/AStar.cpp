/*!
 * @author Diego Planchenstainer, Giovanni Lorenzini and Enrico Saccon <enrico.saccon@unitn.it>
 * @file AStar.cpp
 * @brief File that contains functions to solve the SAPF problem using A*.
 * @details If more performance are needed, one could use a std::vector instead of an unordered_map
 * for `cameFrom`.
 */
#include <SAPF/AStar/AStar.hpp>

static bool AStarMes = false;

/*--------------------------------------------------------------------------------------------------------------------*/

/**
* @brief Reconstructs the path from the start to the goal.
* @param cameFrom The map that contains the information about the previous nodes.
* @param current The current node.
* @return The path from the start to the goal.
*/
std::vector<GraphNode> reconstructPath(
  std::unordered_map<size_t, size_t> & cameFrom, GraphNode currentNode, const std::vector<GraphNode>& nodes)
{
  std::vector<GraphNode> path;
  size_t current = currentNode.getId();
  auto currentIter = cameFrom.find(current);
  size_t counter = 0;

  while (counter <= nodes.size()*2) {
    counter ++;
    path.push_back(nodes[current]);
    if (cameFrom.find(current) == cameFrom.end()) { break; }
    current = cameFrom.at(current);
  }
  std::reverse(path.begin(), path.end());
  return path;
}

/*--------------------------------------------------------------------------------------------------------------------*/

/**
* @Brief Function to compute the shortest path between two points on a graph using AStar
* @param nodes A vector containing the nodes of the graph
* @param initPos The initial vertex
* @param endPos The final vertex
* @param connect A connectivity matrix made SPTests of Connection types
* @param heuristic The heuristic function to use
* @param a_id The id of the Agent which is being considered
* @return The shortest path going from `initPos` to `endPos`
*/
static std::vector<GraphNode> AStar(
 std::vector<GraphNode> & nodes, GraphNode initPos, GraphNode endPos, Graph * connect,
 double (*heuristic)(GraphNode, GraphNode), size_t a_id = 0)
{
  FUNCTION_NAME("Using A*", AStarMes)

  // If the initial and final positions are the same, return the initial position
  if (initPos == endPos) {
    std::vector<GraphNode> sol = {initPos, endPos};
    return sol;
  }

  // The set of nodes already evaluated
  std::vector<bool> closedSet (nodes.size(), false);

  // The set of discovered nodes that may need to be (re-)expanded
  PriorityQueue<size_t, double> openSet;
  // Initially, only the start node is known
  openSet.put(initPos.getId(), heuristic(initPos, endPos));

  // For node n, gScore[n] is the cost of the cheapest path from start to n currently known
  std::vector<double> gScore (nodes.size(), -1.0);
  // For the start node gScore is zero
  gScore[initPos.getId()] = 0.0;

  // For node n, cameFrom[n] is the node immediately preceding it on the cheapest path from start to n currently known
  std::unordered_map<size_t, size_t> cameFrom;

  while (!openSet.empty()) {
    // Get the node in openSet having the lowest fScore value and remove it from openSet
    GraphNode currentNode = nodes[openSet.get()];

    // If the current node is the goal, then we have found the solution
    if (currentNode == endPos) {
      // Reconstruct the path
      return reconstructPath(cameFrom, currentNode, nodes);
    }

    // Add the neighbor node to the closed set
    closedSet[currentNode.getId()] = true;

    // Get the neighbors of the current node
    std::vector<GraphNode> neighbors = connect->getNeighbors(currentNode);
    for (GraphNode & neighborNode : neighbors) {
      // Ignore the neighbor which is already evaluated
      if (closedSet[neighborNode.getId()]) {
        continue;
      }

      // The distance from start to the neighbor through current
      // TODO this is not correct
//       double gScoreProposal = gScore.at(currentNode) + euclidean(currentNode, neighborNode);
     double gScoreProposal = gScore.at(currentNode.getId()) + connect->getCost(currentNode, neighborNode);

      // If the path is better than the previous one, update the path
      if (gScore.at(neighborNode.getId()) == -1.0 || gScoreProposal < gScore.at(neighborNode.getId())) {
        cameFrom[neighborNode.getId()] = currentNode.getId();
        gScore[neighborNode.getId()] = gScoreProposal;

        // Add the neighbor node to the open set
        openSet.put(neighborNode.getId(), gScoreProposal + heuristic(neighborNode, endPos));
      }
    }
  }

  // Open set is empty but goal was never reached
  return {};
}

/*--------------------------------------------------------------------------------------------------------------------*/

/**
* @brief A function that returns the shortest path for an agent going from its initial position to
* its final position and through the different intermediate goal positions
* @details We assume the order of the goals is already optimal, hence the problem reduced to
* calling AStar P2P for the adjacent couples of nodes in `[initPos, goalPos, endPos]`.
*
* @param nodes The list of nodes in the graph
* @param initPos The initial position
* @param endPos The arrival position
* @param goalPos A list containing the intermediate position to meet
* @param connect A connectivity matrix made SPTests of Connection classes
* @param heuristic The heuristic function to use
* @param a_id The id of the agent that is being considered
* @throws std::runtime_error if it could not find a point-to-point path
* @return The shortest path going from `initPos` to `endPos` meeting the different goal positions
*/
static std::vector<GraphNode> AStar(
 std::vector<GraphNode> & nodes, GraphNode initPos, GraphNode endPos,
 std::vector<GraphNode> & goalPos, Graph * connect, double (*heuristic)(GraphNode, GraphNode),
 size_t a_id = 0)
{
 FUNCTION_NAME("Using A*", AStarMes)

 if (goalPos.empty()) {
   return AStar(nodes, initPos, endPos, connect, heuristic, a_id);
 }

 std::vector<GraphNode> finalPath;
 GraphNode s = initPos;
 GraphNode f = goalPos[0];

 // Compute path from initPos to first goalPos
 std::vector<GraphNode> tmp = AStar(nodes, s, f, connect, heuristic, a_id);

 if (tmp.empty()) {
   ERROR("Error, A* could not find a path between @ and @", s, f);
 }
 finalPath.insert(finalPath.end(), tmp.begin(), tmp.end());

 // Compute paths for intermediate goals
 for (size_t i = 1; i < goalPos.size(); i++) {
   s = f;
   f = goalPos[i];

   tmp = AStar(nodes, s, f, connect, heuristic);

   if (tmp.empty()) {
     ERROR("Error, A* could not find a path between @ and @", s, f);
   }

   finalPath.insert(finalPath.end(), tmp.begin() + 1, tmp.end());
 }

 // Compute path from final goalPos to endPos
 s = f;
 f = endPos;

 tmp = AStar(nodes, s, f, connect, heuristic);

 if (tmp.empty()) {
   std::cerr << "Error, A* could not find a path between " << s << " and " << f << std::endl;
   return {};
 }
 finalPath.insert(finalPath.end(), tmp.begin() + 1, tmp.end());

 return finalPath;
}

/*--------------------------------------------------------------------------------------------------------------------*/

std::vector<GraphNode> AStarSolver::solve(
 GraphNode initPos, GraphNode endPos, std::vector<GraphNode> goals, size_t depTime, int a_id)
{
  std::vector<GraphNode> nodes = this->getGraph()->getNodes();
  auto connect = this->getGraph();
  std::vector<GraphNode> sol = AStar(nodes, initPos, endPos, goals, connect, this->heuristic, a_id);
  return sol;
}

/*--------------------------------------------------------------------------------------------------------------------*/

typedef std::pair<size_t, size_t> NidTime;

struct openSetElement{
  size_t nodeId;
  size_t time;
  // For node n, fScore[n] := gScore[n] + heuristic_cost_estimate(n, goal), best guess of the cost to reach the goal
  double fScore;

  openSetElement() : nodeId(0), time(0), fScore(0) {}

  openSetElement(size_t _nodeId, size_t _time, double _fScore) :
    nodeId(_nodeId), time(_time), fScore(_fScore)
  {}

  openSetElement(const openSetElement & e) {
    this->nodeId = e.nodeId;
    this->time = e.time;
    this->fScore = e.fScore;
  }

  bool operator< (const openSetElement& other) const {
    return this->fScore < other.fScore;
  }

  bool operator> (const openSetElement& other) const {
    return this->fScore > other.fScore;
  }

  bool operator==(const openSetElement& other) const {
    return (nodeId == other.nodeId && time == other.time);
  }

  friend std::ostream& operator<< (std::ostream & out, const openSetElement & e) {
    out << tprintf("Node: @ Time: @ fScore: @", e.nodeId+1, e.time, e.fScore);
    return out;
  }
};

namespace std{
template <>
struct hash<openSetElement> {
  size_t operator()(const openSetElement& e) const {
    return std::hash<std::size_t>{}(e.nodeId) ^ (std::hash<std::size_t>{}(e.time));
  }
};
}

// I need this function to correctly order the nodes in the priority queue
bool operator< (const std::shared_ptr<openSetElement>& lhs, const std::shared_ptr<openSetElement>& rhs){
  return lhs->fScore < rhs->fScore;
}

/**
* @brief Reconstructs the path from the start to the goal.
* @param cameFrom The map that contains the information about the previous nodes.
* @param current The current node.
* @return The path from the start to the goal.
*/
std::vector<GraphNode> reconstructPath(
  std::unordered_map<std::shared_ptr<openSetElement>, std::shared_ptr<openSetElement>> & cameFrom,
  std::shared_ptr<openSetElement> currentNode,
  const std::vector<GraphNode>& nodes)
{
  std::vector<GraphNode> path = {nodes[currentNode->nodeId]};
  size_t counter = 0;

  std::shared_ptr<openSetElement> current = cameFrom.at(currentNode);
  while (counter <= nodes.size()*2) {
    path.push_back(nodes[current->nodeId]);
    counter++;
    if (cameFrom.find(current) == cameFrom.end()) { break; }
    current = cameFrom.at(current);
  }

  if (counter > nodes.size()*2) {
    WARNING("Error, A* could not backtrack the path");
  }

  std::reverse(path.begin(), path.end());
  return path;
}

double computeGscore (GraphNode neighbor, Graph* connect,
                     const std::vector<std::vector<GraphNode>>& paths){
  if (paths.empty()){
    return 1.0;
  }
  double ret = 0.5;
  // Check if the node is inside one of the paths.
  for (auto& path : paths){
    if (std::find(path.begin(), path.end(), neighbor) != path.end()){
      ret += 0.5;
      break;
    }
  }

  return ret;
//  return 1.0;
}


/**
* @Brief Function to compute the shortest path between two points on a graph using AStar
* @param nodes A vector containing the nodes of the graph
* @param initPos The initial vertex
* @param endPos The final vertex
* @param connect A connectivity matrix made SPTests of Connection types
* @param heuristic The heuristic function to use
* @param a_id The id of the Agent which is being considered
* @return The shortest path going from `initPos` to `endPos`
*/
static std::vector<GraphNode> AStar(
  std::vector<GraphNode> & nodes, GraphNode initPos, GraphNode endPos, Graph * connect,
  double (*heuristic)(GraphNode, GraphNode), std::vector<Constraint> & cons, size_t depTime = 0,
  size_t a_id = 0, std::vector<std::vector<GraphNode>> paths = {}, GraphNode final = GraphNode())
{
//  INFO(tprintf("depTime @", depTime));
  FUNCTION_NAME("Using A*", AStarMes)

  // If the initial and final positions are the same, return the initial position
  if (initPos == endPos) {
    std::vector<GraphNode> sol = {initPos, endPos};
    return sol;
  }

  // A vector containing all the possible elements
  std::vector<openSetElement> elements = {
      openSetElement(initPos.getId(), depTime, std::numeric_limits<double>::infinity())
    };

  // The priority queue of nodes to be evaluated
  std::priority_queue<std::shared_ptr<openSetElement>, std::vector<std::shared_ptr<openSetElement>>, std::greater<std::shared_ptr<openSetElement>>> openSet;
  openSet.push(std::make_shared<openSetElement>(elements.front()));
  // For node n, cameFrom[n] is the node immediately preceding it on the cheapest path from start to n currently known.
  std::unordered_map<std::shared_ptr<openSetElement>, std::shared_ptr<openSetElement>> cameFrom;

  // For node n, gScore[n] is the cost of the cheapest path from start to n currently known.
  std::unordered_map<openSetElement, double> gScore;
  gScore[elements.front()] = 0;

  // The loop ends either when the set is empty or when the final position is reached
  while (!openSet.empty()) {
    std::shared_ptr<openSetElement> curr = openSet.top();
    openSet.pop();

//    INFO_COL(tprintf("Expanding node @", curr.get()[0]), Color::GREEN);

    // If the node is the final position, then we are done.
    if (nodes[curr->nodeId] == endPos) {
      if (endPos == final) {
        // Check that all constraints have been satisfied
        size_t maxTime = curr->time;
        if (!cons.empty()) {
          maxTime = std::max_element(cons.begin(), cons.end(), [](Constraint c1, Constraint c2) {
                      return c1.getTimestamp() < c2.getTimestamp();
                    })->getTimestamp();
        }

        if (curr->time >= maxTime) {
          return reconstructPath(cameFrom, curr, nodes);
        }
      }
      else {
        return reconstructPath(cameFrom, curr, nodes);
      }
    }

    // Consider the neighbors of the current node
    std::vector<GraphNode> neighbors = connect->getNeighbors(curr->nodeId);
    for (size_t i = 0; i < neighbors.size(); i++) {
      // Check that the neighbor does not violate a constraint
      auto constIter =
        std::find_if(cons.begin(), cons.end(), [&neighbors, i, a_id, curr](Constraint c) {
          return c.getAId() == a_id && c.getTimestamp() == (curr->time + 1) &&
                 (c.getN1() == neighbors[i] || c.getN2() == neighbors[i]);
        });
      // If the node is in the constraint, then skip it.
      if (constIter != cons.end()) {
        continue;
      }

      // Computes the possible gScores for the neighbors.
      double proposedGscore = gScore[curr.get()[0]] + computeGscore(neighbors[i], connect, paths);
      double proposedFscore = proposedGscore + heuristic(nodes[curr->nodeId], neighbors[i]);

//      INFO_COL(tprintf("Considering neighbor @ with gScore @ fScore @", neighbors[i].getV(), proposedGscore, proposedFscore), Color::YELLOW);

      openSetElement neighNode(neighbors[i].getId(), curr->time + 1, proposedFscore);

      // If the node is not in the vector, then add it to the vector and the open set.
      if (gScore.find(neighNode) == gScore.end()) {
        elements.push_back(neighNode);
        std::shared_ptr<openSetElement> newElem = std::make_shared<openSetElement>(elements.back());
        cameFrom[newElem] = curr;
        openSet.push(newElem);
        gScore[neighNode] = proposedGscore;
      }
      // Otherwise check if the score is better than before.
      else if (proposedGscore < gScore.at(neighNode)) {
        gScore[neighNode] = proposedGscore;
        auto elemIter = std::find(elements.begin(), elements.end(), neighNode);
        std::shared_ptr<openSetElement> newElem = std::make_shared<openSetElement>(*elemIter);
        cameFrom[newElem] = curr;
        openSet.push(newElem);
        gScore[neighNode] = proposedGscore;
      }
      else{
//        INFO_COL(tprintf("Neighbor @ is not better than before", neighbors[i]), Color::YELLOW);
      }
    }
  }

  // Open set is empty but goal was never reached
  return {};
}

/*--------------------------------------------------------------------------------------------------------------------*/

/**
* @brief A function that returns the shortest path for an agent going from its initial position to
* its final position and through the different intermediate goal positions
* @details We assume the order of the goals is already optimal, hence the problem reduced to
* calling AStar P2P for the adjacent couples of nodes in `[initPos, goalPos, endPos]`.
*
* @param nodes The list of nodes in the graph
* @param initPos The initial position
* @param endPos The arrival position
* @param goalPos A list containing the intermediate position to meet
* @param connect A connectivity matrix made SPTests of Connection classes
* @param heuristic The heuristic function to use
* @param a_id The id of the agent that is being considered
* @return The shortest path going from `initPos` to `endPos` meeting the different goal positions
*/
static std::vector<GraphNode> AStar(
  std::vector<GraphNode> & nodes, GraphNode initPos, GraphNode endPos,
  std::vector<GraphNode> & goalPos, Graph * connect, double (*heuristic)(GraphNode, GraphNode),
  std::vector<Constraint> & constraints, size_t depTime = 0, size_t a_id = 0,
  std::vector<std::vector<GraphNode>> paths = {})
{
  FUNCTION_NAME("Using A*", AStarMes)

  std::vector<GraphNode> poses = {};
  if (goalPos.empty()) {
    return AStar(nodes, initPos, endPos, connect, heuristic, constraints, depTime, a_id, paths, endPos);
  }
  else {
    poses.push_back(initPos);
    poses.insert(poses.end(), goalPos.begin(), goalPos.end());
    poses.push_back(endPos);
  }

  std::vector<GraphNode> finalPath = {initPos};

  for (size_t i = 0; i<poses.size()-1; i++){
    std::vector<GraphNode> tmp = AStar(nodes, poses[i], poses[i+1], connect, heuristic, constraints, depTime, a_id, paths, endPos);
//    INFO_COL(tprintf("Found g2g solution @\n", tmp), Color::PURPLE);
    if (tmp.empty()) {
      WARNING(tprintf("Error, A* could not find a path between @ and @", poses[i], poses[i + 1]));
      return {};
    }
    finalPath.insert(finalPath.end(), tmp.begin() + 1, tmp.end());
    depTime = finalPath.size() - 1;
  }

  return finalPath;
}

/*--------------------------------------------------------------------------------------------------------------------*/

std::vector<GraphNode> AStarSolver::solveCBS(
 GraphNode initPos, GraphNode endPos, std::vector<GraphNode> goals,
 std::vector<Constraint> & constraints, size_t depTime, int a_id, std::vector<std::vector<GraphNode>> paths)
{
//  INFO(tprintf("Solving with A* for agent @ with constraints: @", a_id, constraints));
  std::vector<GraphNode> nodes = this->getGraph()->getNodes();
  std::vector<GraphNode> path = AStar(nodes, initPos, endPos, goals, this->getGraph(), this->heuristic, constraints, depTime, a_id, paths);

  // Check that the path returned is not empty
  if (path.empty()){
    WARNING(tprintf("A* could not find a path for agent @", a_id));
    return {};
  }

  // Check that the solution returnes it faesible given the constraints
  for (const auto & cons : constraints){
    if (cons.getAId() == a_id && (path[cons.getTimestamp()] == cons.getN1() || path[cons.getTimestamp()] == cons.getN2())){
      WARNING(tprintf("A* returned the wrong path for the constraints @ \n@", path, constraints));
      return {};
    }
  }
//  INFO(tprintf("[@] Found solution: @", a_id, path));
  return path;
}

