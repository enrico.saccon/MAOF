/**
 * @author Enrico Saccon <enrico.saccon@unitn.it>
 * @file ST.cpp
 * @brief File containing the functions to compute the shortest path using spanning tree.
 */

// TODO This file has not been checked yet

#include "SAPF/ST/ST.hpp"

static bool STMes = false; /// Variable used to ensure that the function name is not printed multiple times

struct NodeDist {
  GraphNode* first;
  size_t second;

  NodeDist() : first(nullptr), second(0) {}
  explicit NodeDist(std::pair<GraphNode*, size_t> _p) : first(_p.first), second(_p.second) {}

  [[nodiscard]] GraphNode* getNode() const { return first; }
  [[nodiscard]] size_t getDist() const { return second; }
};

std::vector<std::vector<GraphNode> > spanningTree(std::vector<GraphNode>& nodes, GraphNode initPos, GraphNode endPos,
                                             const Graph * connect, size_t initDist)
{
  FUNCTION_NAME("Using spanningTree", STMes);

  GraphNode * node = &(nodes[initPos.getId()]);
  GraphNode * prev = nullptr;
  size_t dist = initDist;

  std::vector<std::vector<NodeDist>> paths (1);
  std::vector<NodeDist> open = { NodeDist(std::make_pair(node, dist)) };

  while(!open.empty()){
//    std::cout << "OPEN: [";
//    for (auto a : open){
//      std::cout << a.getNode()->getV() << " ";
//    }
//    std::cout << "] paths: [";
//    for (auto path : paths){
//      std::cout << std::endl << "\t[";
//      for (auto a : path) {
//        std::cout << a.getNode()->getV() << " ";
//      }
//      std::cout << "]";
//    }
//    std::cout << "]" << std::endl << std::endl;

    node = open.back().getNode();
    dist = open.back().getDist();
    open.pop_back();

    // The path followed led to a dead-end.
    if (!paths.back().empty() && paths.back().back().getDist() > dist){
      paths.back().erase(
        std::find_if(paths.back().begin(), paths.back().end(),
                     [dist](const NodeDist& n) { return n.getDist() == dist; }),
        paths.back().end());

      prev = paths.back().back().getNode();
      dist = paths.back().back().getDist();
    }

    // The inserted node creates a cycle. Then it checks the last node of OPEN, which actually
    // represents alternatives to the node that created the cycle. The algorithm removes all the
    // nodes that have a distance higher or equal to the one of the last node, then it restarts the
    // loop, which will then consider the new node from OPEN.
    // TODO This condition is actually pretty time-consuming, I could use counters instead.
    if(std::find_if(
          paths.back().begin(),
          paths.back().end(),
          [node](const NodeDist& n) { return n.getNode()[0] == node[0]; }) != paths.back().end())
    {
      if(!open.empty())
      { //That was the last node
        dist = open.back().getDist();
        paths.back().erase(
          std::find_if(
            paths.back().begin(),
            paths.back().end(),
            [dist](const NodeDist& n) { return n.getDist() == dist; }),
          paths.back().end());
        prev = paths.back().back().getNode();
      }
      continue;
    }

    // Insert the node in the solution
    paths.back().push_back(NodeDist(std::make_pair(node, dist)));

    // Node inserted is the target, so create a new vector, which should inherit all the nodes up to
    // the one which created a ramification.
    if(*node == endPos){
//      std::cout << "This path reached the destination" << std::endl;
      if (!open.empty()) {
        paths.push_back(std::vector<NodeDist>());

        for (auto n : paths[paths.size()-2]){
          if (n.getDist() == open.back().getDist()){
            break;
          }
          paths.back().push_back(n);
        }
        dist = open.back().second;
        prev = paths.back().back().getNode();
      }
      continue;
    }

    // Add neighboring nodes that are different from the node from which we are coming from.
    for(size_t n = 0; n<nodes.size(); n++)
    {
      if((prev == nullptr || nodes[n] != *prev) && connect->getCost(node->getId(), n, dist))
      {
        open.push_back(NodeDist(std::make_pair(&nodes[n], dist+1)));
      }
    }

    prev = node;
  }

  //Check that the last added path actually reaches the destination, and if it does not, remove it
  if (!paths.empty() && paths.back().back().getNode()[0] != endPos){
    paths.pop_back();
  }

  // Remove the distances from the solution
  std::vector<std::vector<GraphNode>> sol (paths.size());
  for (size_t i=0; i<paths.size(); i++){
    for (NodeDist n : paths[i]){
      sol[i].push_back(*(n.getNode()));
    }
  }

  return sol;
}

static void appendPaths(std::vector<std::vector<GraphNode> >& finalPaths, const std::vector<std::vector<GraphNode> >& tmp)
{
  size_t initialSize = finalPaths.size();
  finalPaths.resize(initialSize == 0 ? tmp.size() : initialSize*tmp.size());
  //First copy the vectors
  for(size_t i=1; i<tmp.size(); i++) {
    for(size_t j=0; j<initialSize; j++){
      finalPaths[i*initialSize + j].insert(finalPaths[i*initialSize + j].begin(),
                                             finalPaths[j].begin(),
                                             finalPaths[j].end());
    }
  }

  //Then add the new paths
  for(size_t i=0; i<tmp.size(); i++){
    for(size_t j=0; j<initialSize; j++) {
      finalPaths[i * initialSize + j].insert(finalPaths[i * initialSize + j].end(),
                                             tmp[i].begin() + 1,
                                             tmp[i].end());
    }
  }
}

std::vector<std::vector<GraphNode> >spanningTree(std::vector<GraphNode>& nodes, GraphNode initPos, GraphNode endPos, std::vector<GraphNode>& goalPos,
                                             const Graph * connect, size_t initDist)
{
  FUNCTION_NAME("Using spanningTree", STMes);

  if(goalPos.empty()){
    return spanningTree(nodes, initPos, endPos, connect, initDist);
  }

  std::vector<std::vector<GraphNode> > finalPaths;
  GraphNode s = initPos;
  GraphNode f = goalPos[0];

  // Compute path from initPos to first goalPos
  std::vector<std::vector<GraphNode> > tmp = spanningTree(nodes, s, f, connect, initDist);

  if (tmp.empty()){
    std::cerr << "Error, could not find a path between " << s << " and " << f << std::endl;
    return {};
  }

  finalPaths = tmp;

  // Compute paths for intermediate goals
  for(size_t i=1; i<goalPos.size(); i++) {
    s = f;
    f = goalPos[i];

    tmp = spanningTree(nodes, s, f, connect);

    if (tmp.empty()){
      std::cerr << "Error, could not find a path between " << s << " and " << f << std::endl;
      return {};
    }

    appendPaths(finalPaths, tmp);
  }

  // Compute path from final goalPos to endPos
  s = f;
  f = endPos;

  tmp = spanningTree(nodes, s, f, connect);

  if (tmp.empty()){
    std::cerr << "Error, could not find a path between " << s << " and " << f << std::endl;
    return {};
  }
  appendPaths(finalPaths, tmp);

  return finalPaths;
}

static std::vector<GraphNode> findSolution(std::vector<std::vector<GraphNode> >& paths, const std::vector<Constraint>& constraints)
{
  sort(paths.begin(), paths.end(),
       [](const std::vector<GraphNode> & a, const std::vector<GraphNode> & b) -> bool
       {
         return a.size() < b.size();
       });

  for (std::vector<GraphNode> path : paths) {
    bool conflict = false;
    for (Constraint con: constraints) {
      if (con.checkConflict(path) != Conflict::NONE){ conflict = true; break; }
    }
    if (!conflict) { return path; }
  }
  return {};
}

//TODO this could be improved by removing the unnecessary loops over longer paths when a feasible shorter one has been found
static std::vector<GraphNode> fixSolution(std::vector<std::vector<GraphNode> >& paths, std::vector<Constraint>& constraints)
{
  sort(constraints.begin(), constraints.end(),
       [](const Constraint & a, const Constraint & b) -> bool
       {
         return a.getTimestamp() < b.getTimestamp();
       });

  size_t max = constraints.empty() ? 0 : constraints.back().getTimestamp();

  std::vector<GraphNode> sol;

  for (auto &path : paths){
    // First resize the path to the maximum timestamp inside the constraints by adding the last node multiple times.
    if (path.size() < max){
      size_t prevSize = path.size();
      GraphNode prevNode = path.back();

      for (size_t pos = prevSize; prevSize < max; prevSize++){
        path.insert(path.begin()+pos, prevNode);
      }
    }

    size_t safe_time = 1;
    for(auto con : constraints){
      switch(con.checkConflict(path)) {
        case Conflict::VERTEX : {
          path.insert(path.begin() + safe_time, *(path.begin() + safe_time - 1));
          break;
        }
        case Conflict::SWAP : {
          path.insert(path.begin() + safe_time, *(path.begin() + safe_time - 1));
          break;
        }
        case Conflict::NONE :
        default : {}
      }
      safe_time = con.getTimestamp();
    }
  }

  return findSolution(paths, constraints);
}

std::vector<GraphNode> spanningTree(std::vector<GraphNode> nodes, GraphNode initPos, GraphNode endPos,
                               std::vector<GraphNode>& goalPos, const Graph* connect,
                               std::vector<Constraint>& constraints)
{
  FUNCTION_NAME("Using spanningTree", STMes);
  std::vector<std::vector<GraphNode> > paths = spanningTree(nodes, initPos, endPos, goalPos, connect);

  //  std::vector<GraphNode> sol = findSolution(paths, constraints);
  std::vector<GraphNode> sol = fixSolution(paths, constraints);

  return sol;
}