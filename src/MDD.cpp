#include <MDD.hpp>
#include <GraphNode.hpp>

bool MDDNode::operator==(const MDDNode & other) const
{
  return level == other.getLevel() && label == other.getLabel() && this->node == other.getNode();
}

bool MDDNode::operator<(const MDDNode & other) const
{
  if (this->level != other.getLevel()){
    return this->level < other.getLevel();
  }

  if (this->node != other.getNode()) {
    return this->node < other.getNode();
  }

  if (this->label != other.getLabel()) {
    return this->label < other.getLabel();
  }
  return false;
}

bool MDD::addNode(MDDNode mddNode)
{
  if (mdd.find(mddNode) != mdd.end()) {
    return false;
  } else {
    mdd[mddNode] = std::make_pair(Parents(), Children());
    return true;
  }
}

bool MDD::addLink(MDDNode mddNode, MDDNode child)
{
  if (mdd.find(mddNode) != mdd.end() && mdd.find(child) != mdd.end()) {
    mdd[mddNode].second.insert(child);
    mdd[child].first.insert(mddNode);
    return true;
  } else {
    return false;
  }
}

bool MDD::removeLink(MDDNode mddNode, MDDNode child)
{
  if (mdd.find(mddNode) != mdd.end() && mdd.find(child) != mdd.end()) {
    mdd[mddNode].second.erase(child);
    mdd[child].first.erase(mddNode);
    this->prune();
    return true;
  } else {
    return false;
  }
}

std::vector<MDDNode> MDD::getChildren(MDDNode mddNode) const
{
  std::vector<MDDNode> children;

  for (auto & child : mdd.at(mddNode).second) {
    children.push_back(child);
  }

  return children;
}

size_t MDD::count(size_t level) const
{
  size_t count = 0;

  for (auto & node : mdd) {
    if (node.first.getLevel() == level) {
      count++;
    }
  }

  return count;
}

void MDD::prune(MDDNode mddNode)
{
  if (mdd.find(mddNode) != mdd.end()) {
    // Delete mddNode from its children
    for (auto & child : mdd[mddNode].second) {
      mdd[child].first.erase(mddNode);
    }

    // Delete mddNode from its parents
    for (auto & parent : mdd[mddNode].first) {
      mdd[parent].second.erase(mddNode);
    }

    // Delete mddNode
    mdd.erase(mddNode);
  }

  this->prune();
}

void MDD::prune()
{
  // Remove nodes that have no parents
  for (auto it = ++mdd.cbegin(); it != mdd.cend(); ++it) {
    if (it->second.first.empty()) {
      for (auto & child : it->second.second) {
        mdd[child].first.erase(it->first);
      }

      mdd.erase(it);
      it = ++mdd.cbegin();  // QUEST Da vedere
    }
  }

  // Remove nodes that have no children
  for (auto it = ++mdd.crbegin(); it != mdd.crend(); ++it) {
    if (it->second.second.empty()) {
      for (auto & parent : it->second.first) {
        mdd[parent].second.erase(it->first);
      }

      mdd.erase(std::next(it).base());
      it = ++mdd.crbegin();  // QUEST Da vedere
    }
  }
}

void MDD::pad(size_t maxLength)
{
  while (this->getLength() < maxLength) {
    MDDNode mddNode = this->getEnd();
    auto newMddNode = MDDNode(mddNode.getLevel() + 1, mddNode.getNode(), mddNode.getLabel());
    this->addNode(newMddNode);
    this->addLink(mddNode, newMddNode);
  }
}

std::ostream & operator<<(std::ostream & stream, const MDD & M)
{
  for (auto & node : M.mdd) {
    stream << "Node: " << node.first.getNode() << " Level: " << node.first.getLevel() << "\t";

    stream << "Parents: ";
    for (auto & parent : node.second.first) {
      stream << parent.getNode() << " ";
    }

    stream << "\t";

    stream << "Children: ";
    for (auto & child : node.second.second) {
      stream << child.getNode() << " ";
    }

    stream << std::endl;
  }

  stream << std::endl;
  return stream;
}

void MDD::create_dot_file(std::string filename) const {
  std::ofstream dot_file(filename);
  dot_file << "digraph G {" << std::endl << "# NODES" << std::endl;

  for (const auto& node : this->mdd) {
    std::string label = tprintf("@_@", node.first.getNode().getV(), node.first.getLevel());
    dot_file << tprintf("\tn@ [label=\"@\"];\n", label, label);
  }

  dot_file << "#EDGES" << std::endl;

  for (const auto& node : this->mdd) {
    for (auto & child : node.second.second) {
      std::string label1 = tprintf("n@_@", node.first.getNode().getV(), node.first.getLevel());
      std::string label2 = tprintf("n@_@", child.getNode().getV(), child.getLevel());
      dot_file << tprintf("\t@ -> @;\n", label1, label2);
    }
  }

  dot_file << "}" << std::endl;
  dot_file.close();
}
