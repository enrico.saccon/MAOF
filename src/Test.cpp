/**
 * @author Enrico Saccon <enrico.saccon@unitn.it>
 */

#include <Test.hpp>

template<typename T>
T try_catch_json(const nlohmann::json& json, const std::string& key, bool raise = false)
{
  T value = T();
  try {
    value = json.at(key).get<T>();
  } catch (const std::exception& e) {
    if (raise) {
      ERROR(tprintf("Could not find key \"@\" in json file", key));
    }
    else {
      WARNING(tprintf("Could not find key \"@\" in json file", key));
    }
  }
  return value;
}

MAPFSolver * readJSON(
  const std::string& fileName, std::string mapfSolver, std::string costFunction,
  std::string sapfSolver, std::string heuristic, std::string subSolver)
{
  std::ifstream file(fileName);
  if (!file.is_open()) {
    ERROR("Could not find file @", fileName);
  }
  nlohmann::json json = nlohmann::json::parse(file);

  // Parse JSON
  if (mapfSolver.empty())   { mapfSolver   = try_catch_json<std::string>(json, "MAPF"); }
  if (sapfSolver.empty())   { sapfSolver   = try_catch_json<std::string>(json, "SAPF"); }
  if (costFunction.empty()) { costFunction = try_catch_json<std::string>(json, "costFunction"); }
  if (heuristic.empty())    { heuristic    = try_catch_json<std::string>(json, "heuristic"); }
  if (subSolver.empty())    { subSolver    = try_catch_json<std::string>(json, "subSolver"); }

  size_t NAgents = try_catch_json<int>(json, "nAgents");

  std::vector<GraphNode> nodes;
  // If the vector from the json containing the nodes information has length:
  // - 1 then it's only the value associated with the node
  // - 2 then it's the value first and the id as second. Note that the id is the second element of
  //     the vector from the json, but is the first argument passed to the constructor.
  // - 4 then it's the value first, the id second, and the x,y coordinates as third and fourth
  //     values respectively.
  bool sort = false;

  for (auto i : json["nodes"]) {
    std::vector<int> tmp = i.get<std::vector<int>>();
    switch(tmp.size()){
      case 1:
      {
        nodes.push_back(GraphNode(nodes.size(), tmp[0]));
        break;
      }
      case 2:
      {
        nodes.push_back(GraphNode(tmp[1], tmp[0]));
        sort = true;
        break;
      }
      case 4:
      {
        nodes.push_back(GraphNode(tmp[3], tmp[0], tmp[1], tmp[2]));
        sort = true;
        break;
      }
      default: ERROR(tprintf("Vector of size @ is not valid", tmp.size()));
    }
  }

  if (sort)
  {
    std::sort(nodes.begin(), nodes.end(), [nodes](const GraphNode& a, const GraphNode& b) {
      if (a.getId() > nodes.size()-1) { ERROR(tprintf("Cannot have a node with an ID @ higher than the number of nodes.", a.getId()) ); }
      if (b.getId() > nodes.size()-1) { ERROR(tprintf("Cannot have a node with an ID @ higher than the number of nodes.", b.getId()) ); }

      return a.getId() < b.getId();
    });

    if (nodes[0].getId()!=0) {
      ERROR(tprintf("The first node must have ID 0, but it has ID @", nodes[0].getId()) );
    }
  }

  std::vector<std::vector<int>> connect (nodes.size());
  size_t counter = 0;
  for (auto i : json["connect"]) {
    connect[counter] = i.get<std::vector<int>>();
    for (size_t j = 0; j<connect[counter].size(); j++){
      connect[counter][j] = connect[counter][j]>0 ? 1 : 0;
    }
    counter++;
  }
  std::shared_ptr<Graph> graph = std::make_shared<ConMatrix>(ConMatrix(nodes, connect));

  std::vector<Agent> agents (NAgents);

  for (size_t i = 0; i<NAgents; i++)
  {
    size_t id = try_catch_json<size_t>(json["agents"][i], "ID");
    if (id > NAgents - 1){
      ERROR(tprintf("Cannot have an agent with an ID @ higher than the number of agents.", id));
    }
    std::vector<int> tmp = try_catch_json<std::vector<int>>(json["agents"][i], "initPos");
    GraphNode initPos = *std::find_if(nodes.begin(), nodes.end(), [tmp](const GraphNode & a){ return tmp[0] == a.getV(); });
    tmp = try_catch_json<std::vector<int>>(json["agents"][i], "endPos");
    GraphNode endPos = *std::find_if(nodes.begin(), nodes.end(), [tmp](const GraphNode & a){ return tmp[0] == a.getV(); });

    std::vector<GraphNode> goals;
    try{
      for(auto j : json["agents"][i]["goalPos"]){
        tmp = j.get<std::vector<int>>();
        if (!tmp.empty()) {
          goals.push_back(*std::find_if(
            nodes.begin(), nodes.end(), [tmp](const GraphNode & a) { return tmp[0] == a.getV(); }));
        }
      }
    } catch (const std::exception& E) {
      std::cerr << "No goals found for agent" << i << "\n" << E.what() << std::endl;
      goals = {};
    }

    int priority = try_catch_json<int>(json["agents"][i], "priority");
    if (priority == int()) {
      priority = id;
    }

    std::string name = try_catch_json<std::string>(json["agents"][i], "name");

    if (sapfSolver == "") {
      sapfSolver = try_catch_json<std::string>(json["agents"][i], "solver");
    }

    SAPFSolver * solver = chooseSAPFSolver(sapfSolver, graph, getHeuristic(heuristic));

    agents[id] = Agent(id, priority, initPos, endPos, goals, std::make_unique<SAPFSolver*>(solver), name);
  }

  COST_FUNCTION costF = chooseCFType(costFunction);
  MAPFSolver * solver = chooseMAPFSolver(mapfSolver, agents, costF, subSolver);

  file.close();

  return solver;
}

bool solutionIsEmpty (const std::vector<std::vector<Node>>& sol){
  if(sol.empty()){
    return true;
  }
  for (auto& v: sol){
    if (v.empty()){
      return true;
    }
  }
  return false;
}

void runTest(
  std::string json_file, std::string mapfSolver, std::string costFunction, std::string sapfSolver,
  std::string heuristic, std::string subSolver)
{
  MAPFSolver * solver = readJSON(json_file, mapfSolver, costFunction, sapfSolver, heuristic, subSolver);

  // Read solution from JSON if any
  std::ifstream file(json_file);
  nlohmann::json json = nlohmann::json::parse(file);

  std::vector<std::vector<std::vector<size_t>>> jsonSolution;
  try {
    jsonSolution = try_catch_json<std::vector<std::vector<std::vector<size_t>>>>(json, "solution");
  } catch (const std::exception& E) {
    WARNING("No solution found in JSON file @", E.what());
  }

  std::vector<std::vector<GraphNode>> solution;
  for (auto& path : jsonSolution) {
    std::vector<GraphNode> tmp;
    for (auto& node : path){
      switch(node.size()){
        case 1:
        {
          tmp.push_back(GraphNode(tmp.size(), node[0]));
          break;
        }
        case 2:
        {
          tmp.push_back(GraphNode(node[1], node[0]));
          break;
        }
        case 4:
        {
          tmp.push_back(GraphNode(node[1], node[0], node[2], node[3]));
          break;
        }
        default: ERROR(tprintf("Vector of size @ is not valid", tmp.size()));
      }
    }
    solution.push_back(tmp);
  }

  PARSER(solver->out());
  TimePerf tp;
  tp.start();
  std::vector<std::vector<Node>> compSol = {};
  try {
    compSol = solver->solve();
  }
  catch(std::exception & e){
    ERROR("Could not find a solution @", e.what());
  }
  auto finalTime = tp.getTime();

  if (!solution.empty()){
    PARSER("Deafult solution:\n@", printTablePaths(solution));
  }

  if(validMultiPaths(compSol, solution, solver->getAgents())) {
    PARSER("Solution found in: @\n@\n", finalTime, printTablePaths(compSol));
  }
  else {
    PARSER("Could not find a solution");
  }
}

void runTests(
  std::string directory, std::string mapfSolver, std::string costFunction, std::string sapfSolver,
  std::string heuristic, std::string subSolver)
{
  std::cout << "dir: " << directory << std::endl;
  for (const auto & file : fs::directory_iterator(directory)) {
    if (std::string(file.path()).find("json") != std::string::npos) {
      std::cout << file.path() << std::endl;
      runTest(file.path(), mapfSolver, costFunction, sapfSolver, heuristic);
    }
  }
}

std::shared_ptr<Graph> readSAPFJSON(std::vector<GraphNode>& sol, GraphNode& initPos, GraphNode& endPos,
                                    std::vector<GraphNode>& goals, const std::string& fileName)
{
  std::vector<Node> nodes = {};
  std::vector<std::vector<int>> connect = {};

  std::ifstream file(fileName);
  nlohmann::json json = nlohmann::json::parse(file);

  for (size_t n = 0; n < try_catch_json<size_t>(json, "n"); n++) {
    nodes.push_back(Node(n + 1));
  }
  initPos = *std::find(nodes.begin(), nodes.end(), Node(try_catch_json<int>(json, "source") + 1));
  endPos = *std::find(nodes.begin(), nodes.end(), Node(try_catch_json<int>(json, "target") + 1));

  for (auto node : try_catch_json<std::vector<int>>(json, "goals")) {
    goals.push_back(*std::find(nodes.begin(), nodes.end(), Node(node + 1)));
  }

  for (auto node : try_catch_json<std::vector<int>>(json, "sol")) {
    sol.push_back(*std::find(nodes.begin(), nodes.end(), Node(node + 1)));
  }

  connect = try_catch_json<std::vector<std::vector<int>>>(json, "connectivity");

  std::shared_ptr<Graph> _graph = std::make_shared<ConMatrix>(ConMatrix(nodes, connect));

  return _graph;
}

bool equalPaths(const std::vector<GraphNode> & path1, const std::vector<GraphNode> & path2)
{
  if (path1.size() != path2.size()) {
    return false;
  }

  for (size_t i = 0; i < path1.size(); i++) {
    if (path1[i] != path2[i]) {
      return false;
    }
  }

  return true;
}

bool bothValid(const std::vector<GraphNode> & path1, const std::vector<GraphNode> & path2, const std::vector<std::vector<int>>& connect)
{
  for (size_t i=0; i<path1.size()-1; i++)
  {
    if (connect[path1[i].getId()][path1[i+1].getId()] == 0)
    {
      return false;
    }
  }

  for (size_t i=0; i<path2.size()-1; i++)
  {
    if (connect[path2[i].getId()][path2[i+1].getId()] == 0)
    {
      return false;
    }
  }

  return true;
}

bool bothValid(const std::vector<GraphNode> & path1, const std::vector<GraphNode> & path2, const std::vector<std::vector<Connection>>& connect)
{
  for (size_t i=0; i<path1.size()-1; i++)
  {
    if (connect[path1[i].getId()][path1[i+1].getId()].cost() == 0)
    {
      std::cout << tprintf("path1, no edge between @ and @", path1[i], path1[i+1]);
      return false;
    }
  }

  for (size_t i=0; i<path2.size()-1; i++)
  {
    if (connect[path2[i].getId()][path2[i+1].getId()].cost() == 0)
    {
      std::cout << tprintf("path2, no edge between @ and @", path1[i], path1[i+1]);
      return false;
    }
  }

  return true;
}

bool equalMultiPaths(const std::vector<std::vector<GraphNode>> & paths1, const std::vector<std::vector<GraphNode>> & paths2)
{
  if (paths1.size() != paths2.size()) { return false; }

  for (size_t i = 0; i<paths1.size(); i++) {
    if (paths1[i].size() != paths2[i].size()) {
      return false;
    }

    for (size_t j = 0; j < paths1[i].size(); j++) {
      if (paths1[i][j] != paths2[i][j]) {
        return false;
      }
    }
  }

  return true;
}

int validatePath(std::vector<GraphNode> path, Agent& agent){
  // Check initial and final nodes
  if (path.front() != agent.getInitPos() || path.back() != agent.getEndPos()) {
    std::cout << tprintf("Initial or final node is not correct") << std::endl;
    return false;
  }

  // Check all the goals are met
  for (auto goal : agent.getGoals()) {
    if (std::find(path.begin(), path.end(), goal) == path.end()) {
      std::cout << tprintf("Goal @ not met", goal) << std::endl;
      return false;
    }
  }

  // Check that moving from one node to another is valid
  for(size_t i = 0; i < path.size() - 1; i++){
    if (agent.getSolver()->getGraph()->getCost(path[i], path[i+1]) == 0) {
      std::cout << tprintf("At time @, movement from @ to @ is not valid", i, path[i], path[i+1]) << std::endl;
      return false;
    }
  }

  return true;
}

bool validMultiPaths(const std::vector<std::vector<GraphNode>> & paths1, const std::vector<std::vector<GraphNode>> & paths2,
                     std::vector<Agent> agents)
{
  // First check that the computed solution is valid and does not have conflicts
  if (solutionIsEmpty(paths1)) {
    std::cout << "Solution is empty" << std::endl;
    return false;
  }
  if (!findConflict(paths1).isNone()) {
    std::cout << "There are conflicts in the solution" << std::endl;
    return false;
  }

  // If the computed solution and the given solution are equal then the paths are valid
  if (equalMultiPaths(paths1, paths2)) { return true; }
  else{
    // If the agents were not passed, then the evaluation cannot continue
    if (agents.empty()) {
      std::cout << "Solutions not equal and agents not passed" << std::endl;
      return false;
    }
    for (size_t i = 0; i<paths1.size(); i++) {
      if (!validatePath(paths1[i], agents[i])) {
        std::cout << "Path " << i << " is not valid" << std::endl;
        std::cout << printTablePaths({paths1[i]}) << std::endl;
        return false;
      }
    }
  }
  return true;
}
