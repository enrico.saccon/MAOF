/** 
 * @author Enrico Saccon <enrico.saccon@unitn.it>.
 * @file Timeperf.cpp
 * @brief 
 */

#include <Timeperf.hpp>

#ifdef MAOF_PROFILE

static std::map<std::string, TimePerf> timers;
static std::map<std::string, size_t> rounds;
static std::map<std::string, double> total;

/**
 * @brief Auxiliary function to create a new timer.
 * @param profiler_name The name of the profiler
 */
static void createProfiler(const std::string& profiler_name){
  timers[profiler_name] = TimePerf();
  timers[profiler_name].start();
  rounds[profiler_name] = 1;
  total[profiler_name] = 0.0;
}

/**
 * This function should be used at the end of a function or a piece of code you want to profile
 * @param profiler_name The name of the profiler
 */
void profile(const std::string & profiler_name){
  if (timers.find(profiler_name) == timers.end()){
    createProfiler(profiler_name);
  }
  else {
    double t = timers[profiler_name].getTime();
    std::cout << tprintf("[@] @ @ms", rounds[profiler_name], profiler_name, t) << std::endl;
    rounds[profiler_name]++;
    timers[profiler_name].start();
    total[profiler_name] += t;
  }
}

/**
 * @brief This function should be used at the beginning of either the profiled function or the
 * profiled piece of code to restart the profiler.
 * @param profiler_name The name of the profiler
 */
void restartProfiling(const std::string & profiler_name){
  if (timers.find(profiler_name) == timers.end()){
    createProfiler(profiler_name);
  }
  timers[profiler_name].start();
}

#endif //MAOF_PROFILE