/*!
* @author Enrico Saccon
* @file Utils.cpp
* @brief File that contains utility functions.
*/

#include <Utils.hpp>

std::string multiPathToString(const std::vector<std::vector<GraphNode>> & sol, std::string separator){
  std::string ret = "{\n";
  for (auto path : sol){
    ret += ("\t"+pathToString(path, separator));
  }
  ret+="\n}";

  return ret;
}

std::string pathToString(const std::vector<GraphNode> & path, std::string separator)
{
  std::stringstream ss;
  for (const GraphNode & node : path) {
    ss << node << separator;
  }
  ss << std::endl;
  return ss.str();
}



