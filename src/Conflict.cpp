/** 
 * @author Enrico Saccon <enrico.saccon@unitn.it>.
 * @file Conflict.cpp
 * @brief 
 */

// Library includes
#include <Conflict.hpp>

Conflict findConflict(const std::vector<Agent> & agents)
{
  std::vector<std::vector<GraphNode>> paths;
  for (const auto & agent : agents){
    paths.push_back(agent.getPath());
  }

  return findConflict(paths);
}

/**
 * @brief Compares the paths in pairs to check if any conflict arises.
 * @details Be warned that the id of the agents is taken from the position of the path in the vector
 * so be careful in ordering them corresponding to the ids of the agents.
 * @param paths A vector containing the paths to be checked for conflicts
 * @return A vector of Constraint
 */
Conflict findConflict(const std::vector<std::vector<GraphNode>> & paths)
{
  Conflict conflict;

  for(size_t path1=0; path1 < paths.size() - 1 && conflict.isNone(); path1++){
    for(size_t path2= path1 + 1; path2 < paths.size() && conflict.isNone(); path2++){
      size_t t1 = 0, t2 = 0;
      bool last = false;
      do{
        // Vertex conflict
        if(paths[path1][t1] == paths[path2][t2]){
          conflict = Conflict(Conflict::VERTEX, {path1, path2}, {paths[path1][t1]}, (t1 > t2 ? t1 : t2));
        }
        // Edge conflict
        else if(t1>0 && t2>0 && paths[path1][t1] == paths[path2][t2 - 1]
                 && paths[path1][t1 - 1] == paths[path2][t2]){
          conflict = Conflict(Conflict::SWAP, {path1, path2}, {paths[path1][t1-1], paths[path1][t1]}, (t1 > t2 ? t1 : t2));
        }

        // Update times to consider shorter/longer paths
        t1 = (t1 < paths[path1].size() - 1 ? (t1 + 1) : paths[path1].size() - 1);
        t2 = (t2 < paths[path2].size() - 1 ? (t2 + 1) : paths[path2].size() - 1);
        if (last){
          break;
        }
        // When this happens I still need to do one last loop
        if (t1 == paths[path1].size() - 1 && t2 == paths[path2].size() - 1){
          last = true;
        }
      } while (conflict.isNone());
    }
  }

  return conflict;
}


