#include <MAPF/iMAPF.hpp>

MAPFSolver * chooseMAPFSolver (MAPF_TYPE mapfSolver, std::vector<Agent> & agents, COST_FUNCTION costF, MAPF_TYPE subSolver){
  if (mapfSolver == MAPF_TYPE::CBS)
  {
    return new CBSSolver(agents, costF);
  }
#ifdef CPLEX_FOUND
  else if (mapfSolver == MAPF_TYPE::CP)
  {
    return new CPSolver(agents, costF);
  }
#endif
  else if (mapfSolver == MAPF_TYPE::ICR)
  {
    return new ICRSolver(agents, subSolver, costF);
  }
  else if (mapfSolver == MAPF_TYPE::PP)
  {
    return new PPSolver(agents);
  }
  else if (mapfSolver == MAPF_TYPE::ICTS)
  {
    return new ICTSSolver(agents);
  }
  else
  {
    ERROR("MAPF solver not valid");
  }
}
