//
// Created by Enrico Saccon on 28/07/22.
//

#include <MAPF/CostFunction.hpp>

size_t CF_SIC(const std::vector<std::vector<Node> > & solution)
{
  size_t cost = 0;
  for (auto v : solution) {
    cost += v.size();
  }
  return cost;
}

size_t CF_MKS(const std::vector<std::vector<Node> > & solution)
{
  return std::max_element(
           solution.begin(), solution.end(),
           [](const std::vector<Node> & a, const std::vector<Node> & b) {
             return a.size() < b.size();
           })
    ->size();
}