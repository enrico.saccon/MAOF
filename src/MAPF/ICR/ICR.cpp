/**
* @author Enrico Saccon <enrico.saccon@unitn.it>.
* @file ICR.cpp
* @brief
*/

#include "MAPF/ICR/ICR.hpp"
#include <unistd.h>

std::pair<std::shared_ptr<Graph>, std::map<int, int>> ICRSolver::extractGraph(
  Agent & a1, Agent & a2, size_t t, size_t hops)
{
  INFO(tprintf("@", a1));
  // Initialize an empty vector of nodes
  std::vector<GraphNode> nodes(
    a1.getSolver()->getGraph()->getNNodes(),
    GraphNode());

  std::vector<Agent *> confAgents = {&a1, &a2};

  for (Agent * a : confAgents) {
    size_t hop_counter = 0;
    GraphNode node = a->getPath().back();
    if (t < a->getPath().size()) {
      node = a->getPath()[t];
    }

    std::queue<std::pair<GraphNode, size_t>> toVisit;
    toVisit.push(std::make_pair(node, 0));
    size_t max_height = 0, last_height = 0;

    // Breadth-first visit of the graph from the node until the specified number of hops has been
    // reached
    while (hop_counter < hops && !toVisit.empty()) {
      node = toVisit.front().first;
      size_t height = toVisit.front().second;
      max_height = height > max_height ? height : max_height;

      toVisit.pop();
      INFO(tprintf("Examining @/@ hop @", node, toVisit.size(), hop_counter));
      // If the node has not been visited yet
      if (nodes[node.getId()].empty()) {
        nodes[node.getId()] = node;
        // Add the neighbors of the current node to toVisit
        for (GraphNode neigh : a->getSolver()->getGraph()->getNeighbors(node.getId())) {
          INFO(tprintf(
            "node @ neigh @ stored in nodes as @ toVisit @ nodes @", node, neigh,
            nodes[neigh.getId()], toVisit.size(), nodes));
          if (nodes[neigh.getId()].empty()) {
            INFO(tprintf("Adding @ to toVisit, noded[neigh] = @", neigh, nodes[neigh.getId()]));
            toVisit.push(std::make_pair(neigh, height + 1));
//            INFO(tprintf("nodes[neigh] = @", nodes[neigh.getId()]));
          }
        }
        // If the node has more than 2 neighbors, than increase the hop counter
        // TODO if the first node of the conflict actually has multiple neightbors, than this may not give the correct result
        if (a->getSolver()->getGraph()->getNNeighbors(node.getId()) > 2 && height > last_height) {
          last_height = height;
          hop_counter++;
          // Add the neighbors of the cross
          for (GraphNode neigh : a->getSolver()->getGraph()->getNeighbors(node.getId())) {
            if (nodes[neigh.getId()].empty()) {
              toVisit.push(std::make_pair(neigh, height));
            }
          }
        }
      }
    }
    // Add the remaining nodes in toVisit that have the same height
    INFO(tprintf("max_height @ ", max_height));
    INFO("Remaining nodes to add: ");
    while (!toVisit.empty()) {
      node = toVisit.front().first;
      INFO(tprintf("(@,@)", toVisit.front().first, toVisit.front().second));
      toVisit.pop();
      nodes[node.getId()] = node;
    }
  }

  // Keep only the nodes that should be considered and add them to a map
  std::map<int, int> nodeNodeMap;  // This may be changed to unordered_map to improve performance
  std::vector<GraphNode> final;

  for (GraphNode g : nodes) {
    if (!g.empty()) {
//      INFO(tprintf("@ -> @", g, final.size()));
      nodeNodeMap[final.size()+1] = g.getId();
      nodeNodeMap[-1 * (g.getId()+1)] = final.size();
      g.setId(final.size());
      final.push_back(g);
    }
  }
  // If the subgraph is the whole graph, then there is no need to do anything
  if (final.size() != this->agents[0].getSolver()->getGraph()->getNNodes()) {
    std::vector<std::vector<Connection>> matrix(final.size());
    for (int i = 0; i < final.size(); i++) {
      matrix[i].resize(final.size());
      for (int j = 0; j < final.size(); j++) {
        size_t oldCost = this->agents[0].getSolver()->getGraph()->getCost(
              getOldId(nodeNodeMap, i), getOldId(nodeNodeMap, j));
        if (oldCost) {
          matrix[i][j] = Connection(Connection::TYPE::ONE, oldCost);
        } else {
          matrix[i][j] = Connection(Connection::TYPE::ZERO, 0);
        }
      }
    }
    std::shared_ptr<Graph> newGraph;
    switch (this->agents[0].getSolver()->getGraph()->getType()) {
      case Graph::TYPE::CON_MATRIX:
        newGraph = std::make_shared<ConMatrix>(ConMatrix(final, matrix));
        break;
      case Graph::TYPE::INVALID:
      default:
        WARNING("Invalid type for graph");
    }

    INFO(tprintf("New graph: \n@", newGraph.get()->toString()));

    return std::make_pair(newGraph, nodeNodeMap);
  } else {
    return std::make_pair(this->agents[0].getSolver()->getShrGraph(), std::map<int, int>());
  }
}

// TODO test that if there are multiple goals that need to be taken inside the subgraph, they are all
// considered.

std::vector<Agent> ICRSolver::populateNewAgents(
  std::shared_ptr<Graph> & newGraph, const std::map<int, int> & nodeNodeMap,
  const std::map<size_t, size_t> & conTimestamps, std::vector<std::pair<size_t, size_t>>& timestamps,
  std::map<size_t, size_t>& agentAgentMap)
{
  std::vector<size_t> keyId = {};   // The ids of the agents in the conflict
  std::vector<size_t> valTime = {}; // The times at which the agent had a conflict
  for (auto p : conTimestamps) {
    keyId.push_back(p.first);
    valTime.push_back(p.second);
  }

  // The agents that are part of the sub-problem
  std::vector<Agent> ret = {};

  // I can assume that the actual time of the collision is the maximum value insider valTime, as
  // all the other smaller values are the ones for which the agent have shorter paths.
  size_t maxTime = std::max_element(conTimestamps.begin(), conTimestamps.end(),
                                    [](const auto& p1, const auto& p2) {
                                      return p1.second < p2.second;}
                                    )->second;

  for (Agent a : this->agents) {
    // Copy the agent
    Agent agent = a;
    size_t time = maxTime;

    // If the agent is in the conflict, then the time should be the time of the conflict. Otherwise,
    // it should be set to either the maximum time in valTime, or the size of the path - 1.
    auto agentTime = conTimestamps.find(agent.getID());
    if (agentTime != conTimestamps.end()) {
      time = agentTime->second;
    }

    if (time > agent.getPath().size()-1) {
      time = agent.getPath().size()-1;
    }

    INFO(tprintf("Populating agent @ time @ maxTime @ length @", agent.getID(), time, maxTime, agent.getPath().size()));

    // Check that the agent is involved with the subgraph
    bool consider = false;
    for (const auto & n : agent.getPath()){
      if (findNewNode(nodeNodeMap, n.getId()) != nodeNodeMap.end()){
        consider = true;
        break;
      }
    }
    if (!consider) {
      INFO(tprintf("Dropping agent @ since it is not involved with subgraph\nnodes: <@>\npath:  <@>", agent.getID(), newGraph->getNodes(), agent.getPath()));
      continue;
    }

    // CHANGE INITIAL POINT
    // Starting from the time of conflict, move left to find the last point that is inside the
    // subgraph. If the node to the left of the one with conflict is not inside the subgraph, then
    // the agent should probably not be considered since it is already out of it.
    // TODO I should actually start checking posInit on an interval on the right since this would probably reduce future collisions.
    int posInit = (int)time;
    if (findNewNode(nodeNodeMap, agent.getPath()[posInit].getId()) == nodeNodeMap.end()){
      INFO(tprintf("Dropping agent @ because it exited the subgraph before conflict", agent.getID()));
      continue;
    }
    else {
      posInit--;
      for (; posInit >= 0 &&
             findNewNode(nodeNodeMap, agent.getPath()[posInit].getId()) != nodeNodeMap.end();
           posInit--)
      {}
      posInit++;
    }
//    INFO(tprintf("posInit: @ @", posInit, agent.getPath().at(posInit)));

    // CHANGE FINAL POINT
    // Starting from the next node of the conflict, move right to find the last point that is inside
    // the subgraph. If the node immediately to the right is out of the subgraph, then the agent
    // should probably not be considered.
    int posEnd = (int)time;
    if (posEnd < agent.getPath().size() &&
        findNewNode(nodeNodeMap, agent.getPath()[posEnd].getId()) == nodeNodeMap.end()){
      INFO(tprintf("Dropping agent @ because it exited the subgraph immediately", agent.getID()));
      continue;
    }
    else {
      posEnd ++;
      for (; posEnd < agent.getPath().size() &&
             findNewNode(nodeNodeMap, agent.getPath()[posEnd].getId()) != nodeNodeMap.end();
           posEnd++)
      {
        INFO(tprintf("Checking node @ @", agent.getPath().at(posEnd), getNewId(nodeNodeMap, agent.getPath()[posEnd].getId())));
      }
      posEnd--;
    }
//    INFO(tprintf("posEnd: @ @", posEnd, agent.getPath().at(posEnd)));

    // CHANGE GOAL POINTS
    std::vector<GraphNode> newGoals = {};
    std::vector<GraphNode> oldGoals = agent.getGoals();
    for (size_t pos = posInit; pos < posEnd; pos++){
//      INFO(agent.getPath().at(pos));
      if (std::find_if(
            oldGoals.begin(),
            oldGoals.end(),
            [pos,agent](const GraphNode& node) {
              return agent.getPath().at(pos) == node;
            }) != oldGoals.end()) {
        newGoals.push_back(newGraph->getNode(getNewId(nodeNodeMap, agent.getPath().at(pos).getId())));
      }
    }

    // MAKE CHANGES
    agent.setInitPos(newGraph->getNode(getNewId(nodeNodeMap, agent.getPath()[posInit].getId())));
    agent.setEndPos(newGraph->getNode(getNewId(nodeNodeMap, agent.getPath()[posEnd].getId())));
    if (newGoals.size() > 0) {
      // If the first goal is the start position then remove it
      if (newGoals.front() == agent.getInitPos()) {
        newGoals.erase(newGoals.begin());
      }
      // If the last goal is the final position then remove it
      if (newGoals.back() == agent.getEndPos()) {
        newGoals.erase(newGoals.end());
      }
    }
    agent.setGoals(newGoals);
    INFO(tprintf("old agent @ @->@ goals @\nnew @ @->@ goals @", a.getID(), a.getInitPos(), a.getEndPos(), a.getGoals(), agent.getID(), agent.getInitPos(), agent.getEndPos(), agent.getGoals()));
    // The graph can be changed later: using share_ptr.swap changes the content of the smart pointer

    timestamps.push_back(std::make_pair(posInit, posEnd));
    agent.setID(ret.size());
    ret.push_back(agent);
    agentAgentMap[agent.getID()] = a.getID();
  }
  return ret;
}

std::pair<std::vector<std::vector<GraphNode>>, std::vector<size_t>> ICRSolver::solveLocal(
  std::shared_ptr<Graph> & newGraph, std::map<int, int> & nodeNodeMap, const std::vector<Constraint> cons)
{
  std::map<size_t, size_t> conTimestamps;
  std::map<size_t, size_t> agentAgentMap;
  for (const auto& con : cons){
    conTimestamps[con.getAId()] = con.getTimestamp();
  }

  std::vector<std::pair<size_t, size_t>> timestamps = {};
  std::vector<Agent> newAgents = populateNewAgents(newGraph, nodeNodeMap, conTimestamps, timestamps, agentAgentMap);
  INFO(tprintf("agentAgentMap: @", agentAgentMap));

  // TODO check if agents have the same initial or final position, as that is not possible... It actually is, if the positions are taken in different time steps... But then this would create a conflict before the considered conflict
  bool wrong = false;
  for (size_t i=0; i<newAgents.size() && !wrong; i++){
    for (size_t j=i+1; j<newAgents.size() && !wrong; j++){
      if (newAgents[i].getInitPos() == newAgents[j].getInitPos()){
        if (timestamps[i].first == timestamps[j].first) {
          WARNING(tprintf("Agent @ and @ have the same initial positions and starting times @ @, continuing", newAgents[i], newAgents[j], timestamps[i].first, timestamps[j].first));
          wrong = true;
        }
        else {
          WARNING(tprintf("Agent @ and @ have the same initial positions, but different times @ @, NOT dropping", newAgents[i], newAgents[j], timestamps[i].first, timestamps[j].first));
          wrong = true;
        }
      }
      // TODO this should not even be considered as the subsolver should provide a solution with different arriving times on the node.
      if (newAgents[i].getEndPos() == newAgents[j].getEndPos()){
        wrong = true;
        WARNING(tprintf("Agent @ and @ have the same final positions finishing at the same time, continuing", newAgents[i], newAgents[j]));
      }
    }
  }
  if (wrong) { return {}; }

  std::vector<size_t> times (timestamps.size());
  size_t minTime = std::min_element(timestamps.begin(), timestamps.end())->first;
  for (size_t i = 0; i<times.size(); i++){ times[i] = timestamps[i].first - minTime; }
  INFO(tprintf("TIMES: @", times));

  // Change graph for the agents in conflict
  std::shared_ptr<Graph> oldGraph = this->agents[0].getSolver()->getShrGraph();
  for(Agent& a : newAgents) {
    a.getSolver()->setShrGraph(newGraph);
    if (oldGraph.get() == a.getSolver()->getGraph()) {
      WARNING(tprintf("First graph swap not working properly for agent @", a));
    }
  }
  std::vector<std::vector<GraphNode>> localPaths = {};

  MAPFSolver * solv = chooseMAPFSolver(this->subSolverType, newAgents, this->getCostFunction());
  INFO(tprintf("Solving with sub solver\n@", solv->out()));
  INFO(tprintf("Graph\n@", solv->getAgent(0)->getSolver()->getGraph()->toString()));
  localPaths = solv->solveICR(times);

  for (Agent& agent : this->agents){
    agent.getSolver()->setGraph(oldGraph.get());
    if (agent.getSolver()->getGraph() != oldGraph.get()){
      WARNING(tprintf("Second graph swap not working for agent @", agent));
    }
  }

  if (localPaths.empty()){
    return std::pair<std::vector<std::vector<GraphNode>>, std::vector<size_t>>({}, {});
  }
  INFO("Found local solution with @ hops", this->n_hops);

  // Change the nodes from subgraph to old graph. Only the ID should be different since we are
  // creating copies in the extractGraph function.
  for (std::vector<GraphNode> & path : localPaths){
    for (GraphNode & n : path){
      n.setId(getOldId(nodeNodeMap, n.getId()));
    }
  }
  INFO(tprintf("Local solved paths:"));
  for(auto & path : localPaths){
    INFO(tprintf("@", path));
  }

  // Merge paths
  std::vector<std::vector<GraphNode>> newPaths (this->agents.size());
  std::vector<size_t> a_ids = {};
  for(size_t newId = 0; newId < newAgents.size(); newId++){
    size_t a_id = agentAgentMap.at(newId);
    INFO(tprintf("Merging solution for new @->@ old", newId, a_id));
    // Add first part of the paths
    std::vector<GraphNode> oldPath = this->agents[a_id].getPath();
    INFO(tprintf("[@->@] Conflict (@,@) oldPath @", newId, a_id, timestamps[newId].first, timestamps[newId].second, oldPath));
    std::vector<GraphNode> newPath (oldPath.begin(), oldPath.begin() + timestamps[newId].first);
    INFO(tprintf("[@->@] First part of path: @", newId, a_id, newPath));
    // Merge paths. The first node of the local path is actually the last one of the first part of
    // the old path.
    INFO(tprintf("[@=@->@] (@,@) Local paths @", timestamps[newId].first, times[newId], timestamps[newId].second, newId, a_id, localPaths));
    newPath.insert(newPath.end(), std::next(localPaths[newId].begin(), times[newId]), localPaths[newId].end());
    INFO(tprintf("[@->@] After merging: @", newId, a_id, newPath));
    // Insert final part of the original path
    if (timestamps[newId].second < oldPath.size() - 1) {
      newPath.insert(newPath.end(),
                     std::next(oldPath.begin(), timestamps[newId].second + 1), oldPath.end());
    }
    INFO(tprintf("[@->@] After adding final part: @", newId, a_id, newPath));

    newPaths[a_id] = newPath;
    a_ids.push_back(a_id);
  }

  INFO(tprintf("Printing new paths:\n@", printTablePaths(newPaths)));
  return std::make_pair(newPaths, a_ids);
}

static void pad(std::vector<std::vector<GraphNode>> & paths){
  size_t longest = std::max_element(paths.begin(), paths.end(), [](auto & path1, auto & path2){ return path1.size() < path2.size(); })->size();
  for (auto & path : paths){
    for (size_t i = path.size(); i<longest; i++){
      path.push_back(path.back());
    }
  }
}

std::vector<std::vector<GraphNode>> ICRSolver::solve()
{
  // We first need to find the best paths as if the agents are alone in the environment.
  std::vector<std::vector<GraphNode>> paths = {};
  size_t totalHops = 0, minHops = INF<size_t>(), maxHops = 0, nConstraints = 0;
  for (Agent & a : this->agents) {
    std::vector<GraphNode> path = a.solve();
    paths.push_back(path);
  }
  pad(paths);
  INFO("Initial paths: \n@", printTablePaths(paths));
  std::vector<Constraint> constraints = getConstraintsFromConflicts(findConflict(agents));
  while (!constraints.empty()) {
    INFO("Conflicts @ @", constraints[0], constraints[1]);
    bool found = false;
    for (size_t hop = 1; hop <= this->n_hops && !found; hop++) {
      INFO("Trying @ hops", hop);

      // Extract new subgraph with map for nodes
      std::pair<std::shared_ptr<Graph>, std::map<int, int>> p = extractGraph(
        this->agents[constraints[0].getAId()], this->agents[constraints[1].getAId()],
        constraints[0].getTimestamp(), hop);
      std::shared_ptr<Graph> newGraph = p.first;
      std::map<int, int> nodeNodeMap = p.second;

      if (nodeNodeMap.empty()) {
        PARSER("The whole graph is considered, solving the problem with @",
          mapfName(this->subSolverType));
        MAPFSolver * solv = chooseMAPFSolver(this->subSolverType, this->agents, this->cf);
        return solv->solve();
      }

      // Compute new paths in subgraph
      std::pair<std::vector<std::vector<GraphNode>>, std::vector<size_t>> newPaths =
        solveLocal(newGraph, nodeNodeMap, constraints);

      // If the new paths are valid, then break local search
      if (!(newPaths.first.empty() || newPaths.second.empty())) {
        found = true;
        for (size_t id : newPaths.second) {
          INFO(tprintf("Updating path for agent @", id));
          this->agents[id].setPath(newPaths.first[id]);
          paths[id] = newPaths.first[id];
        }
        INFO(tprintf("Updated paths:\n@", printTablePaths(paths)))
        totalHops += hop;
        minHops = std::min(minHops, hop);
        maxHops = std::max(maxHops, hop);
        nConstraints += 1;
      } else {
        // I need to reset the last obtained paths
        for (size_t a_id = 0; a_id < this->agents.size(); a_id++) {
          this->agents[a_id].setPath(paths[a_id]);
        }
      }
    }

    // Check if there is any other conflict
    constraints = getConstraintsFromConflicts(findConflict(agents));
  }

  paths.clear();
  for (const Agent& a : this->agents){
    paths.push_back(a.getPath());
  }

  PARSER("Total hops: @", totalHops);
  PARSER("Min hops: @", minHops != INF<size_t>() ? minHops : 0);
  PARSER("Max hops: @", maxHops);
  PARSER("Avg hops: @", (double)(1.0*totalHops/(1.0*nConstraints)));
  PARSER("Number of conflicts: @", nConstraints);

  return paths;
}
