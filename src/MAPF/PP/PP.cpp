/*!
 * @author Enrico Saccon <enrico.saccon@unitn.it>
 * @file PP.cpp
 * @brief Contains the functions for the priority planner.
 */

#include "MAPF/PP/PP.hpp"

void PPSolver::sortAgents(bool (*func)(const Agent & a1, const Agent a2))
{
  std::sort(this->agents.begin(), this->agents.end(), [func](const Agent & a1, const Agent & a2) {
    if (func != nullptr) {
      return func(a1, a2);
    } else {
      return a1.getPriority() < a2.getPriority();
    }
  });
}

/*!
 * @brief Consider the path of the previous agent and change the graph accordingly prohibiting any
 * other agent from being on those nodes at the same time.
 * @param agent The agent which is changing the graph.
 * @param lastSol The last computed path
 */
void changeConnect(Agent& agent, const std::vector<GraphNode> & lastSol)
{
  for (size_t i = 1; i < lastSol.size(); ++i) {
    GraphNode nodeA = lastSol[i];

    std::shared_ptr<Graph> graph(agent.getSolver()->getGraph());

    std::vector<GraphNode> placeholders;

    // When passing -1 as the agent to the constraint, then the constraint is valid for all the agents
    graph.get()->addConstraints({Constraint(nodeA, i, -1)}, &placeholders);
  }
}

/*!
 * @brief Iterate through the agents (already in order) and compute their paths.
 * @return A vector of vector containing the final solution.
 */
std::vector<std::vector<GraphNode>> PPSolver::solve()
{
  std::vector<std::vector<GraphNode>> sol = {};

  for (Agent & a : this->agents) {
    std::vector<GraphNode> path = a.solve();
    sol.push_back(path);

    changeConnect(a, sol.back());
  }

  return sol;
}