#include <MAPF/ICTS/ICTS.hpp>

/*--------------------------------------------------------------------------------------------------------------------*/

bool ICTSSolver::checkPathsConflicts(std::vector<GraphNode> & path1, std::vector<GraphNode> & path2)
{
  if (path1.size() && path2.size()) {
    size_t i = 0;

    while (i < path1.size() && i < path2.size()) {
      // Vertex conflict
      if (path1[i] == path2[i]) {
        return true;
      }

      // Swap conflict
      if (i > 0) {
        if (path1[i] == path2[i - 1] && path1[i - 1] == path2[i]) {
          return true;
        }
      }

      i++;
    }
  }
  return false;
}

/*--------------------------------------------------------------------------------------------------------------------*/

std::vector<std::vector<GraphNode>> ICTSSolver::solve()
{
  std::vector<std::vector<GraphNode>> ret = {};
  restartProfiling("ICTS::Solve "+ std::to_string(this->agents[0].getSolver()->getGraph()->getNNodes()));
  if (!ID) {
    // Load all agents in a vector
    std::vector<Agent *> agents(this->getAgents().size());

    for (size_t a_id = 0; a_id < this->getAgents().size(); a_id++) {
      agents[a_id] = this->getAgent(a_id);
    }

    ret = conflictingAgentsSolve(agents);
  } else {
    // Compute solution considering only single agents
    std::vector<std::vector<GraphNode>> solution;

    for (size_t a_id = 0; a_id < this->getAgents().size(); a_id++) {
      Agent * agent = this->getAgent(a_id);
      std::vector<GraphNode> path = agent->solve();

      if (path.empty()) {
        throw std::runtime_error(
          "lowLevelAgents: Could not compute a feasible path for agent " + std::to_string(a_id));
      }

      solution.push_back(path);
    }

    // Initialize groups of agents that are in conflict
    std::vector<std::vector<size_t>> conflicting_groups;
    for (size_t i = 0; i < solution.size(); i++) {
      conflicting_groups.push_back({i});
    }

    bool conflicts;
    do {
      size_t maxSolutionLength = 0;

      // Find the longest solution
      for (size_t i = 0; i < solution.size(); i++) {
        if (solution[i].size() > maxSolutionLength) {
          maxSolutionLength = solution[i].size();
        }
      }

      // Extends all solutions to the same length
      for (size_t i = 0; i < solution.size(); i++) {
        while (solution[i].size() < maxSolutionLength) {
          solution[i].push_back(solution[i].back());
        }
      }

      conflicts = false;

      // Check for conflicts between solutions of agents
      for (size_t i = 0; i < solution.size(); i++) {
        for (size_t j = i + 1; j < solution.size(); j++) {
          // If there is a conflict, merge the groups of agents
          if (checkPathsConflicts(solution[i], solution[j])) {
            // Find group of agent i
            size_t group_i;
            for (size_t k = 0; k < conflicting_groups.size(); k++) {
              for (size_t l = 0; l < conflicting_groups[k].size(); l++) {
                if (conflicting_groups[k][l] == i) {
                  group_i = k;
                  break;
                }
              }
            }

            // Find group of agent j
            size_t group_j;
            for (size_t k = 0; k < conflicting_groups.size(); k++) {
              for (size_t l = 0; l < conflicting_groups[k].size(); l++) {
                if (conflicting_groups[k][l] == j) {
                  group_j = k;
                  break;
                }
              }
            }

            // Merge groups of agents i and j
            if (group_i != group_j) {
              conflicting_groups[group_i].insert(
                conflicting_groups[group_i].end(), conflicting_groups[group_j].begin(),
                conflicting_groups[group_j].end());
              conflicting_groups.erase(conflicting_groups.begin() + group_j);
            }

            conflicts = true;
          }
        }
      }

      // If there are no conflicts, return the solution
      if (!conflicts) {
        ret = solution;
      }

      auto f = [&](size_t i) {
        if (conflicting_groups[i].size() == 1) {
          return true;
        }

        // Conflicting agents
        std::vector<Agent *> conflicting_agents;
        for (size_t j = 0; j < conflicting_groups[i].size(); j++) {
          conflicting_agents.push_back(this->getAgent(conflicting_groups[i][j]));
        }

        // Compute a solution for the group of agents
        std::vector<std::vector<GraphNode>> paths = conflictingAgentsSolve(conflicting_agents);

        if (paths.empty()) {
          return false;
        }

        // Update the general solution using the solution for the group of agents
        for (size_t j = 0; j < conflicting_groups[i].size(); j++) {
          solution[conflicting_groups[i][j]] = paths[j];
        }

        return true;
      };

      std::vector<std::future<bool>> threads;

      // Solve conflicts between groups of agents
      for (size_t i = 0; i < conflicting_groups.size(); i++) {
        threads.push_back(std::async(f, i));
      }

      for (size_t i = 0; i < conflicting_groups.size(); i++) {
        if (!threads[i].get()) {
          ret = {};
        }
      }
    } while (conflicts);
  }

  profile("ICTS::Solve "+ std::to_string(this->agents[0].getSolver()->getGraph()->getNNodes()));
  return ret;
}

/*--------------------------------------------------------------------------------------------------------------------*/

std::vector<std::vector<GraphNode>> ICTSSolver::conflictingAgentsSolve(
  std::vector<Agent *> & agents)
{
  // Cache for MDDs
  std::unordered_map<AgentCostPair, MDD> mddsCache;

  // Cache for single agent heuristics
  std::unordered_map<NodeAgentLabel, size_t> heuristicCache;

  // Init root of ICT
  ICTNode root(agents.size(), 0);

  // Compute the initial cost of every agent using a single agent search
  for (size_t i = 0; i < agents.size(); i++) {
    root[i] = agents[i]->solve().size();
  }

  // Queue for BFS to store the frontier
  std::queue<ICTNode> openSet;
  openSet.push(root);

  // The set of generated nodes
  std::set<ICTNode> generated;

  size_t expNodes = 0;
  size_t goalTestedNodes = 0;

  // Run BFS through ICTNodes
  while (!openSet.empty()) {
    // Visit current node
    ICTNode current = openSet.front();
    openSet.pop();
    expNodes += 1;

    // Build MDD for every agent
    std::vector<MDD> mdds = buildMDDs(agents, current, mddsCache, heuristicCache);

    // Use the pointers to MDDs so that the pruning can work
    std::vector<MDD *> mddsPtrs;
    bool goToNextNode = false;
    for (auto & mdd : mdds) {
      mddsPtrs.push_back(&mdd);
      if (mdd.empty()){
        goToNextNode = true;
        break;
      }
    }

    // Pruning
    if (!goToNextNode && !prune(mddsPtrs)) {
      // Run goalTest aka low level search, return a solution if it exists
      std::vector<std::vector<GraphNode>> solution = goalTest(mddsPtrs);
      goalTestedNodes += 1;

      if (!solution.empty()) {
        std::cout << "Explored nodes: " << expNodes << std::endl;
        std::cout << "Pruned nodes: " << expNodes - goalTestedNodes << std::endl;
        std::cout << "Goal tested nodes: " << goalTestedNodes << std::endl;
        return solution;
      }
    }

    // Otherwise, add incremented ICTNodes to the openSet
    for (size_t i = 0; i < current.size(); i++) {
      ICTNode newNode = current;
      newNode[i] += 1;

      // Add the neighbor to the queue if it is not generated yet
      if (!generated.count(newNode)) {
        generated.emplace(newNode);
        openSet.push(newNode);
      }
    }
  }

  return {};
}

/*--------------------------------------------------------------------------------------------------------------------*/

bool ICTSSolver::isLegal(
  std::vector<MDDNode> & child, std::vector<MDDNode> & parent)
{
  for (size_t i = 0; i < child.size(); i++) {
    for (size_t j = i + 1; j < child.size(); j++) {
      if (
        // Vertex conflict
        (child[i].getNode() == child[j].getNode()) ||
        // Swap conflict
        ((parent[i].getNode() == child[j].getNode()) &&
         (parent[j].getNode() == child[i].getNode()))) {
        return false;
      }
    }
  }
  return true;
}

/*--------------------------------------------------------------------------------------------------------------------*/

bool ICTSSolver::prune(std::vector<MDD *> & mdds)
{
  if (mdds.size() > 2) {
    for (size_t i = 0; i < mdds.size(); i++) {
      for (size_t j = i + 1; j < mdds.size(); j++) {
        // Pair of MDDs to consider
        std::vector<MDD *> pair;
        pair.push_back(mdds[i]);
        pair.push_back(mdds[j]);

        // Do a goal test with pruning enabled
        auto solution = goalTest(pair, true);

        // If it cannot find a solution, the ICT node can be skipped
        if (solution.empty()) {
          return true;
        }
      }
    }
  }

  return false;
}

/*--------------------------------------------------------------------------------------------------------------------*/

std::vector<std::vector<GraphNode>> ICTSSolver::goalTest(
  std::vector<MDD *> & mdds, bool pruning)
{
  // Low level search

  // Multiply mdds between each other (no need to store it)
  // Use DFS to search for goal, return solution if exists

  // If pruning is true, pairwise enhanced pruning is active.
  // This pruning technique prune MDD nodes that conflicts modifying the MDDs themselves.
  // This can speed up the successive goal tests since the MDDs are smaller.

  // Find the longest solution length
  size_t maxLength = std::max_element(mdds.begin(), mdds.end(),
                                      [](const MDD* a, const MDD * b) -> bool
                                      { return a->getLength() < b->getLength(); })[0]->getLength();

  // Padd all the MDD to a fixed length
  for (size_t i = 0; i < mdds.size(); i++) {
    mdds[i]->pad(maxLength);
  }

  // Find startPos as vector of MDDs
  std::vector<MDDNode> startPos;
  for (size_t i = 0; i < mdds.size(); i++) {
    startPos.push_back(mdds[i]->getBegin());
  }

  // Find endPos as vector of MDDs
  std::vector<MDDNode> endPos;
  for (size_t i = 0; i < mdds.size(); i++) {
    endPos.push_back(mdds[i]->getEnd());
  }

  // Keep track of the node before the current node to reconstruct the path
  std::map<std::vector<MDDNode>, std::vector<MDDNode>> cameFrom;

  // Keep track of the nodes that have been visited
  std::set<std::vector<MDDNode>> closedSet;

  // Keep track of the nodes that have to be visited
  std::stack<std::vector<MDDNode>> openSet;
  openSet.push(startPos);

  // Run DFS through MDDNodes
  while (!openSet.empty()) {
    // Find the first valid node in the openSet
    std::vector<MDDNode> node;
    bool check = false;

    do {
      if (openSet.empty()) {
        return {};
      }

      node = openSet.top();
      openSet.pop();

      for (size_t i = 0; i < node.size(); i++) {
        if (mdds[i]->contains(node[i])) {
          check = true;
        } else {
          check = false;
          break;
        }
      }
    } while (!check);

    // If the current node is the goal, then we have found the solution
    if (node == endPos) {
      // If we are pruning, the solution is not needed
      if (pruning) {
        return std::vector<std::vector<GraphNode>>(mdds.size());
      }

      // Extract path from node using cameFrom
      std::vector<std::vector<GraphNode>> path(mdds.size());
      std::vector<MDDNode> current = node;

      try {
        while (true) {
          for (size_t i = 0; i < mdds.size(); i++) {
            path[i].push_back(current[i].getNode());
          }
          current = cameFrom.at(current);
        }
      } catch (std::out_of_range & e) {
        for (size_t i = 0; i < mdds.size(); i++) {
          std::reverse(path[i].begin(), path[i].end());
        }
        return path;
      }
    }

    std::vector<std::vector<MDDNode>> children;
    children.reserve(mdds.size());

    // Get the children of each agent
    for (size_t i = 0; i < mdds.size(); i++) {
      children.push_back(mdds[i]->getChildren(node[i]));
    }

    // Compute the number of possible neighbors
    size_t max = 1;
    for (auto const & child : children) {
      max *= child.size();
    }

    std::vector<std::vector<MDDNode>> neighbors;
    neighbors.reserve(max / children.size());

    // Compute all possible neighbors
    // See https://stackoverflow.com/a/48271759/6413980
    for (size_t i = 0; i < max; i++) {
      auto temp = i;

      std::vector<MDDNode> neighbor;
      neighbor.reserve(children.size());

      for (auto const & child : children) {
        auto index = temp % child.size();
        temp /= child.size();

        neighbor.push_back(child[index]);
      }

      neighbors.push_back(neighbor);
    }

    // For each neighbor
    for (size_t i = 0; i < neighbors.size(); i++) {
      // Skip if already visited
      if (closedSet.count(neighbors[i])) {
        continue;
      }

      // Check if nodes are legal to prune unnecessary conflicts
      if (isLegal(neighbors[i], node)) {
        // Add to openSet
        openSet.push(neighbors[i]);
        cameFrom[neighbors[i]] = node;
        closedSet.emplace(neighbors[i]);
      }
      // Enhanced pairwise pruning
      else if (pruning && neighbors[i].size() == 2) {
        // Prune MDD if vertex conflict
        // Check if vertex conflict
        if (neighbors[i][0] == neighbors[i][1]) {
          // Check if second agent has only one vertex (that is the conflicting one)
          if (mdds[1]->count(neighbors[i][1].getLevel()) == 1) {
            // Delete vertex
            mdds[0]->prune(neighbors[i][0]);
          }
          // Check if first agent has only one vertex (that is the conflicting one)
          if (mdds[0]->count(neighbors[i][0].getLevel()) == 1) {
            // Delete vertex
            mdds[1]->prune(neighbors[i][1]);
          }
        }

        // Prune MDD if edge conflict
        // Check if edge conflict
        if (
          node[0].getNode() == neighbors[i][1].getNode() &&
          node[1].getNode() == neighbors[i][0].getNode()) {
          // Check if second agent has only one vertex for both levels (that is the conflicting edge)
          if (
            mdds[1]->count(node[1].getLevel()) == 1 &&
            mdds[1]->count(neighbors[i][1].getLevel()) == 1) {
            // Delete edge
            mdds[0]->removeLink(node[0], neighbors[i][0]);
          }
          // Check if first agent has only one vertex for both levels (that is the conflicting edge)
          if (
            mdds[0]->count(node[0].getLevel()) == 1 &&
            mdds[0]->count(neighbors[i][0].getLevel()) == 1) {
            // Delete edge
            mdds[1]->removeLink(node[1], neighbors[i][1]);
          }
        }
      }
    }
  }

  return {};
}

/*--------------------------------------------------------------------------------------------------------------------*/

MDD ICTSSolver::buildMDD(
  Agent * agent, size_t costLimit,
  std::unordered_map<NodeAgentLabel, size_t> & heuristicCache)
{
  // The map over which we are searching
  Graph * graph = agent->getSolver()->getGraph();

  // Create initial MDD node
  size_t _level = 0;
  size_t label = 1;
  MDDNode mddNode = MDDNode(_level, agent->getInitPos(), label);

  // Create MDD
  MDD mdd;
  mdd.addNode(mddNode);

  // The goals of the agent
  std::vector<GraphNode> goals;
  goals.push_back(agent->getInitPos());
  for (auto goal : agent->getGoals()) {
    if (goals.back() == goal){
      _level ++;
      label ++;
      MDDNode newNode = MDDNode(_level, goal, label);
      mdd.addNode(newNode);
      mdd.addLink(mddNode, newNode);
      mddNode = newNode;
    }
    goals.push_back(goal);
  }
  goals.push_back(agent->getEndPos());

  // Frontier of the nodes to be explored
  std::queue<MDDNode> openSet;
  openSet.push(mddNode);

  while (!openSet.empty()) {
    // Get the next node to expand, from a queue for breadth-first search
    auto node = openSet.front();
    openSet.pop();

    // Expand node
    for (auto & neighbor : graph->getNeighbors(node.getNode())) {
      MDDNode neighborNode(node.getLevel() + 1, neighbor, node.getLabel());

      // If the neighbour has reached a goal update the label
      if (neighborNode.getLabel() < goals.size() && neighbor == goals[neighborNode.getLabel()]) {
        neighborNode.incrementLabel();
      }

      // Use and access cache of heuristic singleAgentPathLengths trough key nodeAgentLabel
      auto nodeAgentLabel = NodeAgentLabel(neighbor, agent->getID(), neighborNode.getLabel());

      // Load the cached heuristic or compute a new one
      size_t heuristic = 0;
      if (heuristicCache.find(nodeAgentLabel) != heuristicCache.end()) {
        heuristic = heuristicCache.at(nodeAgentLabel);
      } else {
        std::vector<GraphNode> intermediateGoals;
        for (size_t l = neighborNode.getLabel(); l < goals.size() - 1; l++) {
          intermediateGoals.push_back(goals[l]);
        }

        heuristic = agent->getSolver()
                      ->solve(neighbor, agent->getEndPos(), intermediateGoals, agent->getID())
                      .size();
        heuristicCache[nodeAgentLabel] = heuristic;
      }

      // Skip if heuristic is too high
      // Maybe save it in another frontier for when it will be re-expanded with costLimit += 1
      if (neighborNode.getLevel() + heuristic > costLimit) {
        continue;
      }

      if (!mdd.contains(neighborNode)) {
        mdd.addNode(neighborNode);
        openSet.push(neighborNode);
      }

      mdd.addLink(node, neighborNode);
    }
  }

  if(mdd.getLength() == costLimit){
    return mdd;
  }
  else {
    return MDD();
  }
}

/*--------------------------------------------------------------------------------------------------------------------*/

std::vector<MDD> ICTSSolver::buildMDDs(
  std::vector<Agent *> & agents, const ICTNode & costs,
  std::unordered_map<AgentCostPair, MDD> & mddsCache,
  std::unordered_map<NodeAgentLabel, size_t> & heuristicCache)
{
  std::vector<MDD> mdds;
  mdds.reserve(agents.size());

  for (size_t i = 0; i < agents.size(); i++) {
    AgentCostPair agentCostPair = AgentCostPair(agents[i]->getID(), costs[i]);

    // If we have already computed the MDD for this agent and cost, we can just load it
    if (mddsCache.count(agentCostPair)) {
      mdds.push_back(mddsCache[agentCostPair]);
      continue;
    }

    // Otherwise, we have to build the MDD from scratch
    MDD mdd = buildMDD(agents[i], costs[i], heuristicCache);

    mdds.push_back(mdd);

    // Store the mdd in cache
    mddsCache[agentCostPair] = mdd;
  }

  return mdds;
}

/*--------------------------------------------------------------------------------------------------------------------*/

std::vector<std::vector<GraphNode>> ICTSSolver::solveICR(const std::vector<size_t>& times)
{
  if (times.size() != agents.size()) {
    ERROR("The number of elements in time should be the same as the number of agents.");
  }
  for(size_t i=0; i<this->agents.size(); i++){
    // Add as many goals as the times the agent should wait in its start position
    std::vector<GraphNode> _goals (times[i], this->agents[i].getInitPos());
    std::vector<GraphNode> oldGoals = this->agents[i].getGoals();
    _goals.insert(_goals.end(), oldGoals.begin(), oldGoals.end());
    this->agents[i].setGoals(_goals);
  }
  INFO(tprintf("NEW\n@", this->out()));
  return this->solve();
}
