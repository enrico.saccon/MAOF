#include <MAPF/iMAPF.hpp>
#include <GraphNode.hpp>
#include <Test.hpp>
#include <Conflict.hpp>

int main12(int argc, char* argv[])
{
//  std::string filename = "warehouse/oneWayMaps/output/WH2_1_1/tests/100.json";
//  std::string filename = "test.json";
//  std::string filename = "test1.json";
  std::string filename = "test2.json";
//  std::string filename = "test3.json";
//  std::string filename = "test4.json";
//  std::string filename = "test5.json";
  if (argc>1) { filename = std::string(argv[1]); }

//  CBSSolver mapfSolver = readJSON(filename, "CBS", "SIC", "TDSP", "none");
  ICRSolver * mapfSolver = dynamic_cast<ICRSolver*> (readJSON(filename, "ICR", "SIC", "A*", "none", "CBS"));
//  ICTSSolver * mapfSolver = dynamic_cast<ICTSSolver*> (readJSON(filename, "ICTS", "SIC", "A*", "none", "none"));
//  mapfSolver->enableID();
//  CPSolver * mapfSolver = dynamic_cast<CPSolver *> (readJSON(filename, "CP", "SIC", "TDSP", "none", "none"));
//  CBSSolver * mapfSolver = dynamic_cast<CBSSolver*> (readJSON(filename, "CBS", "SIC", "A*", "none", "none"));

  std::cout << mapfSolver[0] << std::endl;

  TimePerf time;
  time.start();
  std::vector<std::vector<GraphNode>> sol = mapfSolver->solve();
  std::cout << time.getTime() << std::endl;

  std::cout << "Solution: " << std::endl;

  std::cout << printTablePaths(sol) << std::endl;

  return 0;
}

#include <ArgParser.hpp>

int main214 (int argc, char* argv[]){
  ArgParser parser("Usage: ./main [options]\n\
Options:\n\
\t-f --filename: the test file, default is an empty string\n\
\t-M --mapfSolver: the MAPF solver name, default is \"CBS\"\n\
\t-S --sapfSolver: the SAPF solver name, default is \"A*\"\n\
\t-c --costFunction: the cost function name, either \"SIC\" or \"MKS\", default is \"SIC\"\n\
\t-H --heuristc: the heuristc name, either \"manhattan\" or \"euclidean\"; default is \"none\" \n\
\t-s --subSolver: the name of the subSolver (a MAPF solver) to be used; default is \"none\"\n\
\t-h --help: prints this message.\n\
    ");

  std::string filename = parser.addArg("f", "string", &filename, "", "filename");
  std::string mapfSolver = parser.addArg("M", "string", &mapfSolver, "ICR", "mapfSolver");
  std::string sapfSolver = parser.addArg("S", "string", &sapfSolver, "A*", "sapfSolver");
  std::string costFunction = parser.addArg("c", "string", &costFunction, "SIC", "costFunction");
  std::string heuristic = parser.addArg("H", "string", &heuristic, "none", "heuristic");
  std::string subSolver = parser.addArg("s", "string", &subSolver, "CP", "subSolver");

  if (heuristic == "") heuristic = "none";
  if (subSolver == "") subSolver = "none";

  parser.parseArgs(argc, argv);

  PARSER("Running \nMAPF @\nSAPF @\nCostFunction @\nHeuristic @\nSubSolver @\n",
         mapfSolver, sapfSolver, costFunction, heuristic, subSolver);

  runTest(filename, mapfSolver, costFunction, sapfSolver, heuristic, subSolver);
  return 0;
}

int main (){
  runTest("../tests/tests/vertexConflict.json", "CBS", "SIC", "A*", "");
  return 0;
}

