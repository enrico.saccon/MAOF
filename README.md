# Multi-Agent Open Framework (MAOF)

The goal of this repository is to be a state-of-the-art framework for testing Multi-Agent Path Finding (MAPF)
algorithms.

The code is structured in such a way so that it can be as interchangeable as possible, while maintaining a formal
structure, which is described by the following diagram:

![Diagram](doc/custom/images/diagram.png "diagram")

Each MAPF solver inherits from the `MAPFSolver` class, so simply using a `MAPFSolver*` in the code allows for
interchanging the solver rapidly.

The same can be said for the Single-Agent Path Finding algorithms: they inherit from `SAPFSolver` which is defined as a
pointer inside the `Agent` class so that it can be changed as one needs. The only aspect that needs modify is if a new 
MAPF solver is introduced. Indeed, if it needed passing some arguments, then one would need to construct a solver for 
that particular instance of MAPF, as it is shown in the above figure for CBS. In this case, we also need to pass the
constraints to the `SAPFSolver` which then needs to have its own method to solve it. 

The graph is abstracted too: the main class `Graph` only shows the functions that are needed to know the cost of the
link between two nodes or the neighbors of a node. How the class is implemented behind, that is up to the coder. The aim
is to present the framework with both a graph modeled with a linked-list and another class modelling a graph simply
using a connectivity matrix. The connections between nodes are modelled with a `Connection` class which can take values:
- `ZERO`: the connection is never available;
- `ONE`: the connection is always available;
- `LIMIT_ONCE`: the connection is _always_ available except for some timestamps;
- `LIMIT_ALWAYS`: the connection is _never_ available except for some timestamps.

To know which timestamps to use, the class stores them inside a vector.

For more information regarding the classes and their methods, please refer to the documentation.

## Write a JSON for testing

The framework can be executed by passing a single JSON file which should contain the following fields:

**Mandatory** 

- `n_agents` : the number of agents as `int`

- `nodes` : a vector of vectors. Each vector contains the information regarding a single node. The length of the vector
tells the algorithm how to create the node:
   - 1 then it's only the value of the node
   - 2 then it's the value first and the id as the second element. Note that the id is the position in the  vector, so 
  they will be ordered following the id values and exceptions may arise.
   - 4 then it's the value first, the id second, and the x,y coordinates as third and fourth values.
   - Any other length is not recognized. 

- `Agents` : a vector of dictionary. Each dictionary has the following fields:
  
  **Mandatory**

  - `ID` : the id of the agent;
  - `initPos` : the initial position of the agent
  - `endPos` : the final position of the agent
  - `goalPos` : a vector containing intermediate positions that the agent should pass through before reaching the final
  one
  
  **Opional**
   
  - `priority` : an integer value stating the priority, used with priority planning `PPSolver`. We are going to use the
  rule that the higher priorities are the one that have a smaller value: agent i with priority 1 has a higher priority
  than agent j with priority 5. 
  - `name` : a string with the name of the agent if you get sentimental
- `connect` : a vector of vectors containing the connectivity matrix

- `MAPF` : a string containing the name of the MAPF solver to be used

- `SAPF` : a string containing the name of the SAPF solver to be used

- `costFunction` : a string containing the name of the cost function to be used

- `heuristic` : a string containing the name of the heuristic to be used in the SAPF solver if it requires any

**Optional**
- `format` : a vector of string containing the map of the nodes.

- `solution` : **OPTIONAL** a vector of vectors containing the solution. If present, the function `runTest()` will take
  this field and check if the solution provided is correct.

Once the JSON is ready, you can pass it's path to the function `runTest()` from `Test.hpp`. 

# IMPORTANT 

The timestamps of the paths start from 0. So the initial node has timestamp 0. 

| time | 0 | 1 | 2 | 3 |
|------|---|---|---|---|
| path | 3 | 8 | 1 | 5 |

